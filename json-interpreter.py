import os
import json

with open('C:\\Users\\timot\\Documents\\cs445project\\WeatherApp\\rScripts\\averages.json',"r") as f:
    data = json.load(f)

length = len(data['states'])
dataDict = {"areas":{}}


for i in range(length):
    tempDict = {}
    county = data['counties'][i] + ", " + data['states'][i]
    
    if data['mean_past'][i] == None or data['mean_current'][i] == None:
        value = 0.0
    else:
        value = float(data['mean_current'][i]) - float(data['mean_past'][i])

    tempDict = {county : {"county" : county, "value" : value}}
    dataDict["areas"].update(tempDict)



with open('C:\\Users\\timot\\Documents\\cs445project\\mapdata.json',"r") as f:
    mapData = json.load(f)



for county in mapData["areas"]:
    
    if not county in dataDict["areas"]:
        tempDict = {county : {"county" : county, "value" : 0}}
        dataDict["areas"].update(tempDict)
    

json_object = json.dumps(dataDict, indent = 4)
w = open('C:\\Users\\timot\\Documents\\cs445project\\WeatherApp\\wwwroot\\results.json','w')
w.write(json_object)