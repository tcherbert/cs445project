import requests
import time
import os
import json

errorLog = open("error-log.txt","w")




def process(latitude,longitude):
    requestString = 'https://geo.fcc.gov/api/census/area?lat=' + latitude + '&lon=' + longitude
    try:
        r = requests.get(requestString)
    except:
        print("No internet?")
        stopHere = input("Press Enter to Continue...")
    
    #Success
    if r.status_code == 200:
        #print(r.text + '\n')

        data = json.loads(r.text)
        #print(data['results'][0]['county_name'])
        county = data['results'][0]['county_name']
        state = data['results'][0]['state_code']
        state.replace("/"," ")
        returnData = [state,county]
        if returnData == None:
            stopping = input("Stopping here for no returnData...")
        return [state,county]
        
    #problem
    elif r.status_code == 429 or r.status_code == 503:
        stopHere = input("Press Enter to Continue...")
        process(latitude,longitude)





path = os.getcwd()
results = path + "/results"
path = path + "/dataset"


#print(path)
counter = 0
for filename in os.listdir(path):
    # print(filename)
    data = open(path + "/" + filename,"r")

    #Clear header line
    line = data.readline()
    splitLine = line.split(",")

    #drop all that don't have the data we need.
    if not '"TAVG"' in splitLine:
        continue
        
    tavgIndex = splitLine.index('"TAVG"')
    dateIndex = splitLine.index('"DATE"')
    
    # print(line)
    print(filename)
    


    counter = 0
    state = ""
    county = ""

    while True: 
        line = data.readline()
        if line == '':
            break
        dataLine = line.split('","')
        latitude = dataLine[2]
        longitude = dataLine[3]
        # print(latitude)
        # print(longitude)


        if counter == 0:
            myData = process(latitude,longitude)
            if myData == None:
                counter = 0
                continue
            else:
                counter += 1
            state = myData[0]
            county = myData[1]

            # print("Issue with file: " + filename)

            try:
                os.mkdir(results + "/" + state)
            except:
                print(state + " folder exists")
            
            try: 
                os.mkdir(results + "/" + state + "/" + county)
            except:
                print(county + " folder exists")

        
        
        # print("tavgIndex: " + str(tavgIndex))
        # print("dateIndex: " + str(dateIndex))
        
        date = dataLine[dateIndex]
        tavg = dataLine[tavgIndex]  
        tavg = tavg.replace(" ", "")

        if not tavg == '':
            # print(date)
            # print(tavg)
            # print()
            # print()
            filePath = results  + "/" + state + "/" + county + "/" + date
            try:
                w = open(filePath,"a")
                w.write(tavg + '\n')
            except:
                errorLog.write("Problem with filename: " + filename + " Unable to open filePath: " + filePath + "\n")




        


#pseudo code....
#iterate over dataset

#get state,county information from FCC Database. 
    #break if 429 for limiting for ipaddress switch...

#create folder structure for state / county
    #store data by years.. making sure to append new data




#"34.23333","-86.16667"

        



#process()