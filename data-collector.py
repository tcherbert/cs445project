import requests
import time
import os
import json

path = os.getcwd()
results = path + "/results"

resultsDict = {}

#print(path)
counter = 0
for state in os.listdir(results):
    # print()
    # print(state)
    resultsDict[state] = {}
    for county in os.listdir(results + "/" + state):
        # print(county)
        
        resultsDict[state].update({county : {}})
        for year in os.listdir(results + "/" + state + "/" + county):
            data = open(results + "/" + state + "/" + county + "/" + year,"r")
            
            total = 0
            counter = 0
            while True:
                line = data.readline()
                if line == "":
                    break
                
                lineData = line.replace("\n","")

                #resultsDict[state][county][year].append(lineData)
                total += float(lineData)
                counter += 1
            total = total / counter
            total = (total * (9/5)) + 32
            total = float(str(round(total, 2)))
            resultsDict[state][county].update({year : total})

json_object = json.dumps(resultsDict, indent = 4) 
# print(json_object)
# print(resultsDict)

jsonFile = open("results.json","w")
jsonFile.write(json_object)