﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using WeatherApp.Models;
using System.Text;
using System.Text.Json;

namespace WeatherApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        // Call this route using localhost:port/api/getdata?pastMin=123&pastMax=234&currentMin=345&currentMax=456
        [HttpGet("api/getdata")]
        public IActionResult GetWeatherData(string pastMin, string pastMax, string currentMin, string currentMax)
        {
            // In case I need to reference this... https://stackoverflow.com/questions/14021055/r-script-form-c-passing-arguments-running-script-retrieving-results
            // Call Rscript
            ProcessStartInfo rInfo = new ProcessStartInfo
            {
                FileName = @"f:\R-4.0.2\bin\Rscript.exe",
                WorkingDirectory = Path.GetDirectoryName(@"f:\R-4.0.2\bin\Rscript.exe"),
                Arguments = @"C:\Users\timot\Documents\cs445project\WeatherApp\rScripts\difference.R " + pastMin + " " + pastMax + " " + currentMin + " " + currentMax,
                RedirectStandardInput = false,
                RedirectStandardOutput = true,
                RedirectStandardError = true,
                UseShellExecute = false,
                CreateNoWindow = true
            };

            byte[] original = System.IO.File.ReadAllBytes(Directory.GetCurrentDirectory() + @"\rScripts\averages.json");

            using (Process rProc = new Process())
            {
                rProc.StartInfo = rInfo;

                rProc.Start();
                Console.WriteLine(rProc.StandardOutput.ReadToEnd());
                Console.WriteLine(rProc.StandardError.ReadToEnd());
                rProc.WaitForExit();

                ProcessStartInfo pyInfo = new ProcessStartInfo
                {
                    FileName = @"C:\Users\timot\AppData\Local\Programs\Python\Python39\python.exe",
                    WorkingDirectory = Path.GetDirectoryName(@"C:\Users\timot\AppData\Local\Programs\Python\Python39\python.exe"),
                    Arguments = @"C:\Users\timot\Documents\cs445project\json-interpreter.py",
                    RedirectStandardInput = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    UseShellExecute = false,
                    CreateNoWindow = true
                };

                using (Process pyProc = new Process())
                {
                    pyProc.StartInfo = pyInfo;

                    pyProc.Start();
                    Console.WriteLine(pyProc.StandardError.ReadToEnd());
                    pyProc.WaitForExit();
                };

                byte[] updated = System.IO.File.ReadAllBytes(Directory.GetCurrentDirectory() + @"\rScripts\averages.json");

                if (FileEquals(original, updated))
                {
                    return NotFound();
                }

                return Ok();
            }
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        static bool FileEquals(byte[] file1, byte[] file2)
        {
            if (file1.Length == file2.Length)
            {
                for (int i = 0; i < file1.Length; i++)
                {
                    if (file1[i] != file2[i])
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }
}
