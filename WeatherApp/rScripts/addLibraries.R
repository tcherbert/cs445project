#method 2
install.packages("rjson", repos="http://cran.us.r-project.org")


install.packages("reshape2", repos="http://cran.us.r-project.org")


#recommended method
install.packages("tidyverse", repos="http://cran.us.r-project.org")


#output as Json file
install.packages("RJSONIO", repos="http://cran.us.r-project.org")