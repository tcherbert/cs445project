﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherApp.Models
{
    public class WeatherDataModel
    {
        internal string State;
        internal string County;
        internal double? PastAvg;
        internal double? CurrentAvg;

        public WeatherDataModel(string state, string county, double? pastAvg, double? currentAvg)
        {
            State = state;
            County = county;
            PastAvg = pastAvg;
            CurrentAvg = currentAvg;
        }
    }
}
