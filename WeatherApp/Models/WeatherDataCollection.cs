﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherApp.Models
{
    public class WeatherDataCollection
    {
        internal List<string> states { get; set; }
        internal List<string> counties { get; set; }
        internal List<double?> mean_past { get; set; }
        internal List<double?> mean_current { get; set; }

        public WeatherDataCollection() { }
    }
}
