let averagesData = {
    "areas": {
        "Aleutians West, AK": {
            "county": "Aleutians West, AK",
            "value": 3.3300000000000054
        },
        "Anchorage, AK": {
            "county": "Anchorage, AK",
            "value": 1.144670000000005
        },
        "Bethel, AK": {
            "county": "Bethel, AK",
            "value": 0.02417000000000158
        },
        "Denali, AK": {
            "county": "Denali, AK",
            "value": 3.3900000000000006
        },
        "Fairbanks North Star, AK": {
            "county": "Fairbanks North Star, AK",
            "value": 0.847999999999999
        },
        "Kenai Peninsula, AK": {
            "county": "Kenai Peninsula, AK",
            "value": -2.1426700000000025
        },
        "Ketchikan Gateway, AK": {
            "county": "Ketchikan Gateway, AK",
            "value": 0.0
        },
        "Matanuska-Susitna, AK": {
            "county": "Matanuska-Susitna, AK",
            "value": -4.704329999999999
        },
        "Northwest Arctic, AK": {
            "county": "Northwest Arctic, AK",
            "value": 0.38333000000000084
        },
        "Prince of Wales-Hyder, AK": {
            "county": "Prince of Wales-Hyder, AK",
            "value": 1.066670000000002
        },
        "Southeast Fairbanks, AK": {
            "county": "Southeast Fairbanks, AK",
            "value": -1.2625000000000028
        },
        "Valdez-Cordova, AK": {
            "county": "Valdez-Cordova, AK",
            "value": 6.035
        },
        "Yukon-Koyukuk, AK": {
            "county": "Yukon-Koyukuk, AK",
            "value": 1.368000000000002
        },
        "Apache, AZ": {
            "county": "Apache, AZ",
            "value": 2.3583300000000023
        },
        "Cochise, AZ": {
            "county": "Cochise, AZ",
            "value": 0.5429999999999993
        },
        "Coconino, AZ": {
            "county": "Coconino, AZ",
            "value": 2.48733
        },
        "Greenlee, AZ": {
            "county": "Greenlee, AZ",
            "value": 1.1625000000000014
        },
        "Mohave, AZ": {
            "county": "Mohave, AZ",
            "value": -3.7476699999999994
        },
        "Pima, AZ": {
            "county": "Pima, AZ",
            "value": 3.806669999999997
        },
        "Yavapai, AZ": {
            "county": "Yavapai, AZ",
            "value": 2.855000000000004
        },
        "Alameda, CA": {
            "county": "Alameda, CA",
            "value": 2.0233300000000014
        },
        "Butte, CA": {
            "county": "Butte, CA",
            "value": 1.0459999999999994
        },
        "El Dorado, CA": {
            "county": "El Dorado, CA",
            "value": 0.5180000000000007
        },
        "Fresno, CA": {
            "county": "Fresno, CA",
            "value": -5.6058300000000045
        },
        "Humboldt, CA": {
            "county": "Humboldt, CA",
            "value": -0.4983299999999957
        },
        "Inyo, CA": {
            "county": "Inyo, CA",
            "value": 0.7259999999999991
        },
        "Kern, CA": {
            "county": "Kern, CA",
            "value": 0.6566600000000022
        },
        "Kings, CA": {
            "county": "Kings, CA",
            "value": -2.473329999999997
        },
        "Lassen, CA": {
            "county": "Lassen, CA",
            "value": -0.254170000000002
        },
        "Los Angeles, CA": {
            "county": "Los Angeles, CA",
            "value": 1.07667
        },
        "Mariposa, CA": {
            "county": "Mariposa, CA",
            "value": -3.414999999999999
        },
        "Monterey, CA": {
            "county": "Monterey, CA",
            "value": 1.1083300000000023
        },
        "Nevada, CA": {
            "county": "Nevada, CA",
            "value": -5.712000000000003
        },
        "Placer, CA": {
            "county": "Placer, CA",
            "value": 4.791669999999996
        },
        "Riverside, CA": {
            "county": "Riverside, CA",
            "value": 0.7700000000000031
        },
        "Sacramento, CA": {
            "county": "Sacramento, CA",
            "value": -1.1713300000000046
        },
        "San Bernardino, CA": {
            "county": "San Bernardino, CA",
            "value": -2.159170000000003
        },
        "San Diego, CA": {
            "county": "San Diego, CA",
            "value": -1.3458299999999994
        },
        "San Francisco, CA": {
            "county": "San Francisco, CA",
            "value": -1.3296700000000001
        },
        "San Luis Obispo, CA": {
            "county": "San Luis Obispo, CA",
            "value": -0.16749999999999687
        },
        "Santa Barbara, CA": {
            "county": "Santa Barbara, CA",
            "value": 0.8590000000000018
        },
        "Shasta, CA": {
            "county": "Shasta, CA",
            "value": -0.9480000000000004
        },
        "Sierra, CA": {
            "county": "Sierra, CA",
            "value": 0.39000000000000057
        },
        "Siskiyou, CA": {
            "county": "Siskiyou, CA",
            "value": 0.7900000000000063
        },
        "Sonoma, CA": {
            "county": "Sonoma, CA",
            "value": -0.7383299999999977
        },
        "Stanislaus, CA": {
            "county": "Stanislaus, CA",
            "value": 0.28000000000000114
        },
        "Tehama, CA": {
            "county": "Tehama, CA",
            "value": 3.4016699999999958
        },
        "Trinity, CA": {
            "county": "Trinity, CA",
            "value": -0.24866999999999706
        },
        "Tulare, CA": {
            "county": "Tulare, CA",
            "value": -0.8200000000000003
        },
        "Yuba, CA": {
            "county": "Yuba, CA",
            "value": 1.6750000000000043
        },
        "Baca, CO": {
            "county": "Baca, CO",
            "value": 0.6516699999999958
        },
        "Delta, CO": {
            "county": "Delta, CO",
            "value": 2.9200000000000017
        },
        "Denver, CO": {
            "county": "Denver, CO",
            "value": 0.7866700000000009
        },
        "Douglas, CO": {
            "county": "Douglas, CO",
            "value": 0.9359999999999999
        },
        "Garfield, CO": {
            "county": "Garfield, CO",
            "value": 1.5250000000000057
        },
        "Hinsdale, CO": {
            "county": "Hinsdale, CO",
            "value": 1.9010000000000034
        },
        "Huerfano, CO": {
            "county": "Huerfano, CO",
            "value": 2.0519999999999996
        },
        "Jackson, CO": {
            "county": "Jackson, CO",
            "value": 0.879170000000002
        },
        "Jefferson, CO": {
            "county": "Jefferson, CO",
            "value": 0.36833000000000027
        },
        "Las Animas, CO": {
            "county": "Las Animas, CO",
            "value": 0.6683299999999974
        },
        "Mesa, CO": {
            "county": "Mesa, CO",
            "value": -0.392000000000003
        },
        "Mineral, CO": {
            "county": "Mineral, CO",
            "value": 0.0
        },
        "Moffat, CO": {
            "county": "Moffat, CO",
            "value": -1.6909999999999954
        },
        "Montrose, CO": {
            "county": "Montrose, CO",
            "value": 1.1839999999999975
        },
        "Otero, CO": {
            "county": "Otero, CO",
            "value": 0.4066699999999983
        },
        "Rio Blanco, CO": {
            "county": "Rio Blanco, CO",
            "value": 0.2966699999999989
        },
        "Routt, CO": {
            "county": "Routt, CO",
            "value": 2.780000000000001
        },
        "Hartford, CT": {
            "county": "Hartford, CT",
            "value": 0.4376700000000042
        },
        "Windham, CT": {
            "county": "Windham, CT",
            "value": 0.08800000000000097
        },
        "Duval, FL": {
            "county": "Duval, FL",
            "value": -0.875
        },
        "Miami-Dade, FL": {
            "county": "Miami-Dade, FL",
            "value": 0.0
        },
        "Chatham, GA": {
            "county": "Chatham, GA",
            "value": -0.11333000000000482
        },
        "Hall, GA": {
            "county": "Hall, GA",
            "value": 0.39800000000000324
        },
        "Richmond, GA": {
            "county": "Richmond, GA",
            "value": 0.6499999999999986
        },
        "Hawaii, HI": {
            "county": "Hawaii, HI",
            "value": -0.5169999999999959
        },
        "Honolulu, HI": {
            "county": "Honolulu, HI",
            "value": 1.8000000000000114
        },
        "Kalawao, HI": {
            "county": "Kalawao, HI",
            "value": 0.0
        },
        "Maui, HI": {
            "county": "Maui, HI",
            "value": 14.0625
        },
        "Benton, IA": {
            "county": "Benton, IA",
            "value": -0.08467000000000269
        },
        "Bremer, IA": {
            "county": "Bremer, IA",
            "value": 0.3416700000000006
        },
        "Buchanan, IA": {
            "county": "Buchanan, IA",
            "value": -3.6980000000000004
        },
        "Cass, IA": {
            "county": "Cass, IA",
            "value": 0.288330000000002
        },
        "Cherokee, IA": {
            "county": "Cherokee, IA",
            "value": 0.1243300000000005
        },
        "Clay, IA": {
            "county": "Clay, IA",
            "value": -0.663330000000002
        },
        "Dubuque, IA": {
            "county": "Dubuque, IA",
            "value": 1.2150000000000034
        },
        "Grundy, IA": {
            "county": "Grundy, IA",
            "value": 1.7933300000000045
        },
        "Hardin, IA": {
            "county": "Hardin, IA",
            "value": 0.34150000000000347
        },
        "Harrison, IA": {
            "county": "Harrison, IA",
            "value": 0.9543299999999988
        },
        "Johnson, IA": {
            "county": "Johnson, IA",
            "value": 0.600830000000002
        },
        "Jones, IA": {
            "county": "Jones, IA",
            "value": -1.533999999999999
        },
        "Linn, IA": {
            "county": "Linn, IA",
            "value": 0.15733000000000175
        },
        "Louisa, IA": {
            "county": "Louisa, IA",
            "value": 0.0
        },
        "Lyon, IA": {
            "county": "Lyon, IA",
            "value": 0.6413299999999964
        },
        "Marshall, IA": {
            "county": "Marshall, IA",
            "value": -0.6083300000000023
        },
        "Mitchell, IA": {
            "county": "Mitchell, IA",
            "value": -1.058330000000005
        },
        "O'Brien, IA": {
            "county": "O'Brien, IA",
            "value": 0.4686699999999959
        },
        "Scott, IA": {
            "county": "Scott, IA",
            "value": -0.008000000000002672
        },
        "Sioux, IA": {
            "county": "Sioux, IA",
            "value": 1.0799999999999983
        },
        "Story, IA": {
            "county": "Story, IA",
            "value": -0.13732999999999862
        },
        "Taylor, IA": {
            "county": "Taylor, IA",
            "value": 0.1525000000000034
        },
        "Warren, IA": {
            "county": "Warren, IA",
            "value": -0.4408300000000054
        },
        "Wayne, IA": {
            "county": "Wayne, IA",
            "value": 0.0
        },
        "Ada, ID": {
            "county": "Ada, ID",
            "value": 0.34433000000000646
        },
        "Bingham, ID": {
            "county": "Bingham, ID",
            "value": 0.8783300000000054
        },
        "Blaine, ID": {
            "county": "Blaine, ID",
            "value": 2.4450000000000003
        },
        "Butte, ID": {
            "county": "Butte, ID",
            "value": -2.1235
        },
        "Cassia, ID": {
            "county": "Cassia, ID",
            "value": -2.9849999999999994
        },
        "Clearwater, ID": {
            "county": "Clearwater, ID",
            "value": 2.1543300000000016
        },
        "Custer, ID": {
            "county": "Custer, ID",
            "value": 2.710329999999999
        },
        "Elmore, ID": {
            "county": "Elmore, ID",
            "value": 1.9903300000000002
        },
        "Idaho, ID": {
            "county": "Idaho, ID",
            "value": -0.2266699999999986
        },
        "Lemhi, ID": {
            "county": "Lemhi, ID",
            "value": 1.3616700000000037
        },
        "Madison, ID": {
            "county": "Madison, ID",
            "value": -0.5450000000000017
        },
        "Owyhee, ID": {
            "county": "Owyhee, ID",
            "value": 1.7433299999999932
        },
        "Power, ID": {
            "county": "Power, ID",
            "value": 0.36165999999999343
        },
        "Shoshone, ID": {
            "county": "Shoshone, ID",
            "value": 2.597670000000001
        },
        "Valley, ID": {
            "county": "Valley, ID",
            "value": -0.441670000000002
        },
        "Washington, ID": {
            "county": "Washington, ID",
            "value": 1.397670000000005
        },
        "Alexander, IL": {
            "county": "Alexander, IL",
            "value": -1.0150000000000006
        },
        "Champaign, IL": {
            "county": "Champaign, IL",
            "value": 0.8449999999999989
        },
        "Coles, IL": {
            "county": "Coles, IL",
            "value": 1.2019999999999982
        },
        "Crawford, IL": {
            "county": "Crawford, IL",
            "value": 0.6375000000000028
        },
        "Edgar, IL": {
            "county": "Edgar, IL",
            "value": 0.5024999999999977
        },
        "Fayette, IL": {
            "county": "Fayette, IL",
            "value": 2.924999999999997
        },
        "Hancock, IL": {
            "county": "Hancock, IL",
            "value": 1.9866699999999966
        },
        "Hardin, IL": {
            "county": "Hardin, IL",
            "value": 0.0
        },
        "Henry, IL": {
            "county": "Henry, IL",
            "value": 0.70167
        },
        "Kane, IL": {
            "county": "Kane, IL",
            "value": 1.1379999999999981
        },
        "Kankakee, IL": {
            "county": "Kankakee, IL",
            "value": 2.2266700000000057
        },
        "Lake, IL": {
            "county": "Lake, IL",
            "value": 0.0
        },
        "Lawrence, IL": {
            "county": "Lawrence, IL",
            "value": 1.8279999999999959
        },
        "Livingston, IL": {
            "county": "Livingston, IL",
            "value": -0.7139999999999986
        },
        "Logan, IL": {
            "county": "Logan, IL",
            "value": 0.0
        },
        "Marion, IL": {
            "county": "Marion, IL",
            "value": -2.2733300000000014
        },
        "McHenry, IL": {
            "county": "McHenry, IL",
            "value": 2.3016699999999943
        },
        "McLean, IL": {
            "county": "McLean, IL",
            "value": -1.6893299999999982
        },
        "Mercer, IL": {
            "county": "Mercer, IL",
            "value": 4.539999999999999
        },
        "Pike, IL": {
            "county": "Pike, IL",
            "value": 3.1983299999999986
        },
        "Pulaski, IL": {
            "county": "Pulaski, IL",
            "value": 0.5150000000000006
        },
        "Putnam, IL": {
            "county": "Putnam, IL",
            "value": 2.1400000000000006
        },
        "Randolph, IL": {
            "county": "Randolph, IL",
            "value": -0.9849999999999994
        },
        "Sangamon, IL": {
            "county": "Sangamon, IL",
            "value": 0.943000000000005
        },
        "Scott, IL": {
            "county": "Scott, IL",
            "value": -0.8674999999999997
        },
        "Vermilion, IL": {
            "county": "Vermilion, IL",
            "value": 0.6075000000000017
        },
        "Wabash, IL": {
            "county": "Wabash, IL",
            "value": -2.9766699999999986
        },
        "Wayne, IL": {
            "county": "Wayne, IL",
            "value": 1.0225000000000009
        },
        "Will, IL": {
            "county": "Will, IL",
            "value": -1.8950000000000031
        },
        "Boone, IN": {
            "county": "Boone, IN",
            "value": 0.14999999999999858
        },
        "Hancock, IN": {
            "county": "Hancock, IN",
            "value": -0.394999999999996
        },
        "Howard, IN": {
            "county": "Howard, IN",
            "value": 0.3106700000000018
        },
        "Madison, IN": {
            "county": "Madison, IN",
            "value": 0.2875000000000014
        },
        "Marion, IN": {
            "county": "Marion, IN",
            "value": 0.3225000000000051
        },
        "Montgomery, IN": {
            "county": "Montgomery, IN",
            "value": 0.16599999999999682
        },
        "Orange, IN": {
            "county": "Orange, IN",
            "value": 0.42199999999999704
        },
        "Randolph, IN": {
            "county": "Randolph, IN",
            "value": 0.4434999999999931
        },
        "Shelby, IN": {
            "county": "Shelby, IN",
            "value": -0.15000000000000568
        },
        "Steuben, IN": {
            "county": "Steuben, IN",
            "value": -0.4694999999999965
        },
        "Tippecanoe, IN": {
            "county": "Tippecanoe, IN",
            "value": -2.1783299999999954
        },
        "Washington, IN": {
            "county": "Washington, IN",
            "value": 0.0
        },
        "Allen, KS": {
            "county": "Allen, KS",
            "value": -0.27417000000000513
        },
        "Chautauqua, KS": {
            "county": "Chautauqua, KS",
            "value": -0.36699999999999733
        },
        "Cloud, KS": {
            "county": "Cloud, KS",
            "value": 0.24132999999999782
        },
        "Douglas, KS": {
            "county": "Douglas, KS",
            "value": -0.07499999999999574
        },
        "Elk, KS": {
            "county": "Elk, KS",
            "value": -0.9991699999999994
        },
        "Finney, KS": {
            "county": "Finney, KS",
            "value": 1.203000000000003
        },
        "Ford, KS": {
            "county": "Ford, KS",
            "value": 0.7850000000000037
        },
        "Gove, KS": {
            "county": "Gove, KS",
            "value": 1.1450000000000031
        },
        "Graham, KS": {
            "county": "Graham, KS",
            "value": 0.14332999999999885
        },
        "Greeley, KS": {
            "county": "Greeley, KS",
            "value": 1.4420000000000002
        },
        "Logan, KS": {
            "county": "Logan, KS",
            "value": 1.3299999999999983
        },
        "Marshall, KS": {
            "county": "Marshall, KS",
            "value": 0.9633299999999991
        },
        "Montgomery, KS": {
            "county": "Montgomery, KS",
            "value": -0.044000000000004036
        },
        "Norton, KS": {
            "county": "Norton, KS",
            "value": 0.5129999999999981
        },
        "Phillips, KS": {
            "county": "Phillips, KS",
            "value": 1.990000000000002
        },
        "Reno, KS": {
            "county": "Reno, KS",
            "value": 0.23167000000000115
        },
        "Republic, KS": {
            "county": "Republic, KS",
            "value": 1.4133299999999949
        },
        "Riley, KS": {
            "county": "Riley, KS",
            "value": -0.14932999999999907
        },
        "Rooks, KS": {
            "county": "Rooks, KS",
            "value": -0.9579999999999984
        },
        "Rush, KS": {
            "county": "Rush, KS",
            "value": 0.027499999999996305
        },
        "Russell, KS": {
            "county": "Russell, KS",
            "value": -0.9460000000000051
        },
        "Scott, KS": {
            "county": "Scott, KS",
            "value": 0.17600000000000193
        },
        "Shawnee, KS": {
            "county": "Shawnee, KS",
            "value": 0.879330000000003
        },
        "Sheridan, KS": {
            "county": "Sheridan, KS",
            "value": 0.769999999999996
        },
        "Sherman, KS": {
            "county": "Sherman, KS",
            "value": 0.6266700000000043
        },
        "Washington, KS": {
            "county": "Washington, KS",
            "value": -1.315329999999996
        },
        "Ballard, KY": {
            "county": "Ballard, KY",
            "value": 0.0
        },
        "Boyd, KY": {
            "county": "Boyd, KY",
            "value": 3.383330000000001
        },
        "Clay, KY": {
            "county": "Clay, KY",
            "value": 0.0
        },
        "Fayette, KY": {
            "county": "Fayette, KY",
            "value": 0.5786700000000025
        },
        "Grayson, KY": {
            "county": "Grayson, KY",
            "value": 1.8220000000000027
        },
        "Harlan, KY": {
            "county": "Harlan, KY",
            "value": 1.658999999999999
        },
        "Henderson, KY": {
            "county": "Henderson, KY",
            "value": 0.0
        },
        "Hopkins, KY": {
            "county": "Hopkins, KY",
            "value": -0.1565000000000012
        },
        "Jefferson, KY": {
            "county": "Jefferson, KY",
            "value": 0.6616700000000009
        },
        "Ohio, KY": {
            "county": "Ohio, KY",
            "value": 0.0
        },
        "Rockcastle, KY": {
            "county": "Rockcastle, KY",
            "value": 0.27649999999999864
        },
        "Warren, KY": {
            "county": "Warren, KY",
            "value": -0.7266699999999986
        },
        "Bienville, LA": {
            "county": "Bienville, LA",
            "value": -0.12400000000000944
        },
        "Lafourche, LA": {
            "county": "Lafourche, LA",
            "value": 0.5840000000000032
        },
        "LaSalle, LA": {
            "county": "LaSalle, LA",
            "value": 0.3299999999999983
        },
        "Madison, LA": {
            "county": "Madison, LA",
            "value": -0.7783299999999969
        },
        "Pointe Coupee, LA": {
            "county": "Pointe Coupee, LA",
            "value": 1.2313299999999998
        },
        "St. John the Baptist, LA": {
            "county": "St. John the Baptist, LA",
            "value": 1.1033299999999997
        },
        "St. Mary, LA": {
            "county": "St. Mary, LA",
            "value": -0.19667000000001167
        },
        "St. Tammany, LA": {
            "county": "St. Tammany, LA",
            "value": 0.7599999999999909
        },
        "Vernon, LA": {
            "county": "Vernon, LA",
            "value": -1.1025000000000063
        },
        "Barnstable, MA": {
            "county": "Barnstable, MA",
            "value": 0.8099999999999952
        },
        "Bristol, MA": {
            "county": "Bristol, MA",
            "value": -0.6950000000000003
        },
        "Nantucket, MA": {
            "county": "Nantucket, MA",
            "value": -0.8049999999999997
        },
        "Norfolk, MA": {
            "county": "Norfolk, MA",
            "value": -1.0840000000000032
        },
        "Plymouth, MA": {
            "county": "Plymouth, MA",
            "value": 0.02300000000000324
        },
        "Worcester, MA": {
            "county": "Worcester, MA",
            "value": 0.467330000000004
        },
        "Baltimore City, MD": {
            "county": "Baltimore City, MD",
            "value": 0.0
        },
        "Calvert, MD": {
            "county": "Calvert, MD",
            "value": 3.0800000000000054
        },
        "Carroll, MD": {
            "county": "Carroll, MD",
            "value": 0.0
        },
        "Kent, MD": {
            "county": "Kent, MD",
            "value": 0.0519999999999996
        },
        "St. Mary's, MD": {
            "county": "St. Mary's, MD",
            "value": -0.9759999999999991
        },
        "Aroostook, ME": {
            "county": "Aroostook, ME",
            "value": 0.3780000000000001
        },
        "Franklin, ME": {
            "county": "Franklin, ME",
            "value": -1.5
        },
        "Somerset, ME": {
            "county": "Somerset, ME",
            "value": -0.79833
        },
        "Washington, ME": {
            "county": "Washington, ME",
            "value": 0.8973299999999966
        },
        "Allegan, MI": {
            "county": "Allegan, MI",
            "value": -0.29333000000000453
        },
        "Alpena, MI": {
            "county": "Alpena, MI",
            "value": 0.18499999999999517
        },
        "Arenac, MI": {
            "county": "Arenac, MI",
            "value": 0.03750000000000142
        },
        "Barry, MI": {
            "county": "Barry, MI",
            "value": 0.048329999999999984
        },
        "Calhoun, MI": {
            "county": "Calhoun, MI",
            "value": 1.8249999999999957
        },
        "Chippewa, MI": {
            "county": "Chippewa, MI",
            "value": -1.0333299999999994
        },
        "Dickinson, MI": {
            "county": "Dickinson, MI",
            "value": 0.9830000000000041
        },
        "Emmet, MI": {
            "county": "Emmet, MI",
            "value": 1.2949999999999946
        },
        "Gladwin, MI": {
            "county": "Gladwin, MI",
            "value": 0.12333000000000283
        },
        "Huron, MI": {
            "county": "Huron, MI",
            "value": -0.4200000000000017
        },
        "Ionia, MI": {
            "county": "Ionia, MI",
            "value": -0.056670000000003995
        },
        "Iosco, MI": {
            "county": "Iosco, MI",
            "value": -0.21466999999999814
        },
        "Iron, MI": {
            "county": "Iron, MI",
            "value": 0.9083299999999994
        },
        "Kalamazoo, MI": {
            "county": "Kalamazoo, MI",
            "value": 0.1466700000000003
        },
        "Lapeer, MI": {
            "county": "Lapeer, MI",
            "value": 1.3975000000000009
        },
        "Macomb, MI": {
            "county": "Macomb, MI",
            "value": -0.380829999999996
        },
        "Marquette, MI": {
            "county": "Marquette, MI",
            "value": -0.11932999999999794
        },
        "Mecosta, MI": {
            "county": "Mecosta, MI",
            "value": -1.480000000000004
        },
        "Missaukee, MI": {
            "county": "Missaukee, MI",
            "value": 2.3659999999999997
        },
        "Monroe, MI": {
            "county": "Monroe, MI",
            "value": 0.0
        },
        "Oakland, MI": {
            "county": "Oakland, MI",
            "value": -0.3633300000000048
        },
        "Otsego, MI": {
            "county": "Otsego, MI",
            "value": -1.181670000000004
        },
        "Roscommon, MI": {
            "county": "Roscommon, MI",
            "value": 0.14699999999999847
        },
        "St. Clair, MI": {
            "county": "St. Clair, MI",
            "value": 0.3575000000000017
        },
        "Wayne, MI": {
            "county": "Wayne, MI",
            "value": 0.0
        },
        "Carlton, MN": {
            "county": "Carlton, MN",
            "value": -0.536999999999999
        },
        "Carver, MN": {
            "county": "Carver, MN",
            "value": -0.191670000000002
        },
        "Cass, MN": {
            "county": "Cass, MN",
            "value": 0.7366699999999966
        },
        "Clay, MN": {
            "county": "Clay, MN",
            "value": 1.4669999999999987
        },
        "Fillmore, MN": {
            "county": "Fillmore, MN",
            "value": 0.288330000000002
        },
        "Hennepin, MN": {
            "county": "Hennepin, MN",
            "value": -0.9849999999999994
        },
        "Itasca, MN": {
            "county": "Itasca, MN",
            "value": 1.7960000000000065
        },
        "Jackson, MN": {
            "county": "Jackson, MN",
            "value": -0.1875
        },
        "Lake of the Woods, MN": {
            "county": "Lake of the Woods, MN",
            "value": -0.6266699999999972
        },
        "Lyon, MN": {
            "county": "Lyon, MN",
            "value": -0.8175000000000026
        },
        "Norman, MN": {
            "county": "Norman, MN",
            "value": 0.19666999999999746
        },
        "Polk, MN": {
            "county": "Polk, MN",
            "value": 1.9420000000000002
        },
        "Ramsey, MN": {
            "county": "Ramsey, MN",
            "value": 0.8359999999999985
        },
        "Roseau, MN": {
            "county": "Roseau, MN",
            "value": 2.8049999999999997
        },
        "Sibley, MN": {
            "county": "Sibley, MN",
            "value": 0.47417000000000087
        },
        "Steele, MN": {
            "county": "Steele, MN",
            "value": 0.5990000000000038
        },
        "Stevens, MN": {
            "county": "Stevens, MN",
            "value": -0.48400000000000176
        },
        "Todd, MN": {
            "county": "Todd, MN",
            "value": -4.079329999999999
        },
        "Waseca, MN": {
            "county": "Waseca, MN",
            "value": -0.8575000000000017
        },
        "Wright, MN": {
            "county": "Wright, MN",
            "value": -0.8599999999999994
        },
        "Yellow Medicine, MN": {
            "county": "Yellow Medicine, MN",
            "value": -1.5733299999999986
        },
        "Adair, MO": {
            "county": "Adair, MO",
            "value": -1.259999999999998
        },
        "Buchanan, MO": {
            "county": "Buchanan, MO",
            "value": -1.0416699999999963
        },
        "Butler, MO": {
            "county": "Butler, MO",
            "value": 0.24249999999999972
        },
        "Cape Girardeau, MO": {
            "county": "Cape Girardeau, MO",
            "value": -1.7675000000000054
        },
        "Dent, MO": {
            "county": "Dent, MO",
            "value": 0.5390000000000015
        },
        "Gasconade, MO": {
            "county": "Gasconade, MO",
            "value": -0.9949999999999974
        },
        "Holt, MO": {
            "county": "Holt, MO",
            "value": -1.4216699999999989
        },
        "Jasper, MO": {
            "county": "Jasper, MO",
            "value": 0.20532999999999646
        },
        "Madison, MO": {
            "county": "Madison, MO",
            "value": -0.3993299999999991
        },
        "Maries, MO": {
            "county": "Maries, MO",
            "value": 0.0
        },
        "Marion, MO": {
            "county": "Marion, MO",
            "value": -0.2680000000000007
        },
        "Moniteau, MO": {
            "county": "Moniteau, MO",
            "value": -0.0625
        },
        "Nodaway, MO": {
            "county": "Nodaway, MO",
            "value": -1.009999999999998
        },
        "Pemiscot, MO": {
            "county": "Pemiscot, MO",
            "value": -0.1280000000000001
        },
        "Pettis, MO": {
            "county": "Pettis, MO",
            "value": 0.41833000000000453
        },
        "Phelps, MO": {
            "county": "Phelps, MO",
            "value": 0.728670000000001
        },
        "Saline, MO": {
            "county": "Saline, MO",
            "value": -1.9699999999999989
        },
        "Shannon, MO": {
            "county": "Shannon, MO",
            "value": -0.7883400000000051
        },
        "St. Charles, MO": {
            "county": "St. Charles, MO",
            "value": -0.5850000000000009
        },
        "Webster, MO": {
            "county": "Webster, MO",
            "value": -0.0940000000000012
        },
        "Carroll, MS": {
            "county": "Carroll, MS",
            "value": -0.45549999999998647
        },
        "Copiah, MS": {
            "county": "Copiah, MS",
            "value": -0.8119999999999976
        },
        "Franklin, MS": {
            "county": "Franklin, MS",
            "value": 0.0
        },
        "Hancock, MS": {
            "county": "Hancock, MS",
            "value": 0.0
        },
        "Holmes, MS": {
            "county": "Holmes, MS",
            "value": 0.0
        },
        "Pearl River, MS": {
            "county": "Pearl River, MS",
            "value": -0.6019999999999897
        },
        "Pontotoc, MS": {
            "county": "Pontotoc, MS",
            "value": -0.7566699999999997
        },
        "Rankin, MS": {
            "county": "Rankin, MS",
            "value": 0.3766700000000043
        },
        "Tallahatchie, MS": {
            "county": "Tallahatchie, MS",
            "value": 0.0
        },
        "Walthall, MS": {
            "county": "Walthall, MS",
            "value": -0.055999999999997385
        },
        "Washington, MS": {
            "county": "Washington, MS",
            "value": 0.0
        },
        "Beaverhead, MT": {
            "county": "Beaverhead, MT",
            "value": 1.6713299999999975
        },
        "Big Horn, MT": {
            "county": "Big Horn, MT",
            "value": -0.2779999999999987
        },
        "Blaine, MT": {
            "county": "Blaine, MT",
            "value": -0.031500000000001194
        },
        "Carter, MT": {
            "county": "Carter, MT",
            "value": 0.11499999999999488
        },
        "Chouteau, MT": {
            "county": "Chouteau, MT",
            "value": -0.6226699999999994
        },
        "Custer, MT": {
            "county": "Custer, MT",
            "value": -1.2043299999999988
        },
        "Daniels, MT": {
            "county": "Daniels, MT",
            "value": 1.3183399999999992
        },
        "Fergus, MT": {
            "county": "Fergus, MT",
            "value": -0.0793300000000059
        },
        "Gallatin, MT": {
            "county": "Gallatin, MT",
            "value": 2.118000000000002
        },
        "Garfield, MT": {
            "county": "Garfield, MT",
            "value": -0.9620000000000033
        },
        "Granite, MT": {
            "county": "Granite, MT",
            "value": 4.396329999999999
        },
        "Jefferson, MT": {
            "county": "Jefferson, MT",
            "value": 2.0166699999999977
        },
        "Lewis and Clark, MT": {
            "county": "Lewis and Clark, MT",
            "value": 0.8086699999999993
        },
        "Liberty, MT": {
            "county": "Liberty, MT",
            "value": -1.0364999999999966
        },
        "Lincoln, MT": {
            "county": "Lincoln, MT",
            "value": -1.2629999999999981
        },
        "Madison, MT": {
            "county": "Madison, MT",
            "value": 0.0
        },
        "Meagher, MT": {
            "county": "Meagher, MT",
            "value": 1.566670000000002
        },
        "Mineral, MT": {
            "county": "Mineral, MT",
            "value": 3.8470000000000013
        },
        "Missoula, MT": {
            "county": "Missoula, MT",
            "value": 3.3583300000000023
        },
        "Musselshell, MT": {
            "county": "Musselshell, MT",
            "value": -2.1200000000000045
        },
        "Park, MT": {
            "county": "Park, MT",
            "value": 1.4976699999999994
        },
        "Petroleum, MT": {
            "county": "Petroleum, MT",
            "value": 0.40749999999999886
        },
        "Phillips, MT": {
            "county": "Phillips, MT",
            "value": -0.5733299999999986
        },
        "Pondera, MT": {
            "county": "Pondera, MT",
            "value": -0.09000000000000341
        },
        "Ravalli, MT": {
            "county": "Ravalli, MT",
            "value": 3.2863299999999995
        },
        "Richland, MT": {
            "county": "Richland, MT",
            "value": -0.6466700000000003
        },
        "Rosebud, MT": {
            "county": "Rosebud, MT",
            "value": 0.9579999999999984
        },
        "Sheridan, MT": {
            "county": "Sheridan, MT",
            "value": 1.6183300000000003
        },
        "Silver Bow, MT": {
            "county": "Silver Bow, MT",
            "value": -0.7090000000000032
        },
        "Stillwater, MT": {
            "county": "Stillwater, MT",
            "value": -0.45000000000000284
        },
        "Valley, MT": {
            "county": "Valley, MT",
            "value": 0.28366999999999365
        },
        "Wheatland, MT": {
            "county": "Wheatland, MT",
            "value": 0.9780000000000015
        },
        "Wibaux, MT": {
            "county": "Wibaux, MT",
            "value": 1.8986700000000027
        },
        "Alexander, NC": {
            "county": "Alexander, NC",
            "value": -2.269999999999996
        },
        "Ashe, NC": {
            "county": "Ashe, NC",
            "value": -0.32500000000000284
        },
        "Caldwell, NC": {
            "county": "Caldwell, NC",
            "value": 1.9774999999999991
        },
        "Carteret, NC": {
            "county": "Carteret, NC",
            "value": 1.0825000000000031
        },
        "Cherokee, NC": {
            "county": "Cherokee, NC",
            "value": 0.0
        },
        "Dare, NC": {
            "county": "Dare, NC",
            "value": 0.6433299999999988
        },
        "Graham, NC": {
            "county": "Graham, NC",
            "value": -1.7141699999999958
        },
        "Hertford, NC": {
            "county": "Hertford, NC",
            "value": -0.2850000000000037
        },
        "Lenoir, NC": {
            "county": "Lenoir, NC",
            "value": -0.12450000000000472
        },
        "Macon, NC": {
            "county": "Macon, NC",
            "value": 1.639329999999994
        },
        "New Hanover, NC": {
            "county": "New Hanover, NC",
            "value": -0.054000000000002046
        },
        "Orange, NC": {
            "county": "Orange, NC",
            "value": 0.12749999999999773
        },
        "Robeson, NC": {
            "county": "Robeson, NC",
            "value": 0.7483300000000028
        },
        "Rowan, NC": {
            "county": "Rowan, NC",
            "value": -0.05917000000000172
        },
        "Scotland, NC": {
            "county": "Scotland, NC",
            "value": 0.8575000000000017
        },
        "Union, NC": {
            "county": "Union, NC",
            "value": 0.347999999999999
        },
        "Wilkes, NC": {
            "county": "Wilkes, NC",
            "value": 0.3230000000000004
        },
        "Cavalier, ND": {
            "county": "Cavalier, ND",
            "value": 1.3999999999999986
        },
        "Dickey, ND": {
            "county": "Dickey, ND",
            "value": 0.0
        },
        "Grant, ND": {
            "county": "Grant, ND",
            "value": 0.4013299999999944
        },
        "McHenry, ND": {
            "county": "McHenry, ND",
            "value": -0.695999999999998
        },
        "McIntosh, ND": {
            "county": "McIntosh, ND",
            "value": 2.460000000000001
        },
        "McLean, ND": {
            "county": "McLean, ND",
            "value": 0.7879999999999967
        },
        "Morton, ND": {
            "county": "Morton, ND",
            "value": -0.125
        },
        "Ramsey, ND": {
            "county": "Ramsey, ND",
            "value": 2.2349999999999994
        },
        "Ransom, ND": {
            "county": "Ransom, ND",
            "value": 3.8999999999999986
        },
        "Renville, ND": {
            "county": "Renville, ND",
            "value": 0.0
        },
        "Richland, ND": {
            "county": "Richland, ND",
            "value": 0.0
        },
        "Sheridan, ND": {
            "county": "Sheridan, ND",
            "value": 0.7980000000000018
        },
        "Stutsman, ND": {
            "county": "Stutsman, ND",
            "value": -0.950330000000001
        },
        "Wells, ND": {
            "county": "Wells, ND",
            "value": 0.0
        },
        "Adams, NE": {
            "county": "Adams, NE",
            "value": 0.17500000000000426
        },
        "Arthur, NE": {
            "county": "Arthur, NE",
            "value": 0.006669999999999732
        },
        "Blaine, NE": {
            "county": "Blaine, NE",
            "value": 0.0
        },
        "Box Butte, NE": {
            "county": "Box Butte, NE",
            "value": -0.7733300000000014
        },
        "Butler, NE": {
            "county": "Butler, NE",
            "value": 2.2239999999999966
        },
        "Clay, NE": {
            "county": "Clay, NE",
            "value": -1.5700000000000003
        },
        "Dodge, NE": {
            "county": "Dodge, NE",
            "value": -1.6383299999999963
        },
        "Dundy, NE": {
            "county": "Dundy, NE",
            "value": 0.42967000000000155
        },
        "Garden, NE": {
            "county": "Garden, NE",
            "value": 2.453330000000001
        },
        "Grant, NE": {
            "county": "Grant, NE",
            "value": 0.0
        },
        "Jefferson, NE": {
            "county": "Jefferson, NE",
            "value": 1.0150000000000006
        },
        "Johnson, NE": {
            "county": "Johnson, NE",
            "value": -0.1280000000000001
        },
        "Knox, NE": {
            "county": "Knox, NE",
            "value": 0.0
        },
        "Lancaster, NE": {
            "county": "Lancaster, NE",
            "value": 0.0
        },
        "Lincoln, NE": {
            "county": "Lincoln, NE",
            "value": -0.21999999999999886
        },
        "Otoe, NE": {
            "county": "Otoe, NE",
            "value": 0.125
        },
        "Scotts Bluff, NE": {
            "county": "Scotts Bluff, NE",
            "value": -0.020000000000003126
        },
        "Thurston, NE": {
            "county": "Thurston, NE",
            "value": -1.1824999999999974
        },
        "Cheshire, NH": {
            "county": "Cheshire, NH",
            "value": 1.164670000000001
        },
        "Coos, NH": {
            "county": "Coos, NH",
            "value": 0.7160000000000011
        },
        "Grafton, NH": {
            "county": "Grafton, NH",
            "value": 2.1624999999999943
        },
        "Bergen, NJ": {
            "county": "Bergen, NJ",
            "value": 0.4966699999999946
        },
        "Camden, NJ": {
            "county": "Camden, NJ",
            "value": -0.5200000000000031
        },
        "Hunterdon, NJ": {
            "county": "Hunterdon, NJ",
            "value": 0.30799999999999983
        },
        "Morris, NJ": {
            "county": "Morris, NJ",
            "value": 0.0
        },
        "Sussex, NJ": {
            "county": "Sussex, NJ",
            "value": 1.9600000000000009
        },
        "Union, NJ": {
            "county": "Union, NJ",
            "value": -0.060000000000002274
        },
        "Bernalillo, NM": {
            "county": "Bernalillo, NM",
            "value": 0.7479999999999976
        },
        "Curry, NM": {
            "county": "Curry, NM",
            "value": -0.23167000000000115
        },
        "Grant, NM": {
            "county": "Grant, NM",
            "value": 0.27200000000000557
        },
        "Hidalgo, NM": {
            "county": "Hidalgo, NM",
            "value": -0.931670000000004
        },
        "Lea, NM": {
            "county": "Lea, NM",
            "value": 0.0
        },
        "Luna, NM": {
            "county": "Luna, NM",
            "value": 1.1299999999999955
        },
        "McKinley, NM": {
            "county": "McKinley, NM",
            "value": 1.759999999999998
        },
        "Mora, NM": {
            "county": "Mora, NM",
            "value": 0.0723300000000009
        },
        "Otero, NM": {
            "county": "Otero, NM",
            "value": -0.0009999999999976694
        },
        "Rio Arriba, NM": {
            "county": "Rio Arriba, NM",
            "value": 1.3736700000000042
        },
        "San Miguel, NM": {
            "county": "San Miguel, NM",
            "value": -1.6005000000000038
        },
        "Santa Fe, NM": {
            "county": "Santa Fe, NM",
            "value": 1.607670000000006
        },
        "Socorro, NM": {
            "county": "Socorro, NM",
            "value": -0.3126700000000042
        },
        "Taos, NM": {
            "county": "Taos, NM",
            "value": 2.384999999999998
        },
        "Torrance, NM": {
            "county": "Torrance, NM",
            "value": -0.09850000000000136
        },
        "Clark, NV": {
            "county": "Clark, NV",
            "value": -9.834999999999994
        },
        "Douglas, NV": {
            "county": "Douglas, NV",
            "value": 0.0
        },
        "Elko, NV": {
            "county": "Elko, NV",
            "value": 0.4953300000000027
        },
        "Eureka, NV": {
            "county": "Eureka, NV",
            "value": 0.2819999999999965
        },
        "Humboldt, NV": {
            "county": "Humboldt, NV",
            "value": 0.09199999999999875
        },
        "Lincoln, NV": {
            "county": "Lincoln, NV",
            "value": -0.1663300000000021
        },
        "Lyon, NV": {
            "county": "Lyon, NV",
            "value": -1.4583299999999966
        },
        "Mineral, NV": {
            "county": "Mineral, NV",
            "value": 0.8175000000000026
        },
        "Nye, NV": {
            "county": "Nye, NV",
            "value": 1.2553299999999936
        },
        "Pershing, NV": {
            "county": "Pershing, NV",
            "value": 0.10000000000000142
        },
        "Washoe, NV": {
            "county": "Washoe, NV",
            "value": -0.8583300000000023
        },
        "White Pine, NV": {
            "county": "White Pine, NV",
            "value": -0.9420000000000002
        },
        "Chautauqua, NY": {
            "county": "Chautauqua, NY",
            "value": 0.012669999999999959
        },
        "Erie, NY": {
            "county": "Erie, NY",
            "value": 0.7108300000000014
        },
        "Jefferson, NY": {
            "county": "Jefferson, NY",
            "value": 0.2360000000000042
        },
        "Lewis, NY": {
            "county": "Lewis, NY",
            "value": 0.38800000000000523
        },
        "Livingston, NY": {
            "county": "Livingston, NY",
            "value": 0.6340000000000003
        },
        "Monroe, NY": {
            "county": "Monroe, NY",
            "value": 0.683329999999998
        },
        "Oneida, NY": {
            "county": "Oneida, NY",
            "value": 0.29400000000000404
        },
        "Orange, NY": {
            "county": "Orange, NY",
            "value": 0.10149999999999437
        },
        "Oswego, NY": {
            "county": "Oswego, NY",
            "value": -1.6049999999999969
        },
        "St. Lawrence, NY": {
            "county": "St. Lawrence, NY",
            "value": 0.33449999999999847
        },
        "Steuben, NY": {
            "county": "Steuben, NY",
            "value": -0.2225000000000037
        },
        "Tioga, NY": {
            "county": "Tioga, NY",
            "value": -1.5200000000000031
        },
        "Ulster, NY": {
            "county": "Ulster, NY",
            "value": 0.0
        },
        "Washington, NY": {
            "county": "Washington, NY",
            "value": -0.7310000000000016
        },
        "Ashland, OH": {
            "county": "Ashland, OH",
            "value": 0.0
        },
        "Ashtabula, OH": {
            "county": "Ashtabula, OH",
            "value": -0.39332999999999885
        },
        "Clark, OH": {
            "county": "Clark, OH",
            "value": 0.18200000000000216
        },
        "Fayette, OH": {
            "county": "Fayette, OH",
            "value": 0.0
        },
        "Gallia, OH": {
            "county": "Gallia, OH",
            "value": 1.4666700000000006
        },
        "Jefferson, OH": {
            "county": "Jefferson, OH",
            "value": 1.0024999999999977
        },
        "Knox, OH": {
            "county": "Knox, OH",
            "value": 0.06933000000000078
        },
        "Lawrence, OH": {
            "county": "Lawrence, OH",
            "value": 0.6105000000000018
        },
        "Lorain, OH": {
            "county": "Lorain, OH",
            "value": 0.20000000000000284
        },
        "Trumbull, OH": {
            "county": "Trumbull, OH",
            "value": 0.04200000000000159
        },
        "Union, OH": {
            "county": "Union, OH",
            "value": 0.0
        },
        "Van Wert, OH": {
            "county": "Van Wert, OH",
            "value": -0.21600000000000108
        },
        "Wayne, OH": {
            "county": "Wayne, OH",
            "value": 0.2916700000000034
        },
        "Wood, OH": {
            "county": "Wood, OH",
            "value": 0.4055000000000035
        },
        "Alfalfa, OK": {
            "county": "Alfalfa, OK",
            "value": -0.4566700000000026
        },
        "Beckham, OK": {
            "county": "Beckham, OK",
            "value": -0.9080000000000013
        },
        "Blaine, OK": {
            "county": "Blaine, OK",
            "value": -0.806670000000004
        },
        "Canadian, OK": {
            "county": "Canadian, OK",
            "value": -1.6199999999999974
        },
        "Cherokee, OK": {
            "county": "Cherokee, OK",
            "value": 0.0
        },
        "Choctaw, OK": {
            "county": "Choctaw, OK",
            "value": 0.0
        },
        "Custer, OK": {
            "county": "Custer, OK",
            "value": -0.308329999999998
        },
        "Harper, OK": {
            "county": "Harper, OK",
            "value": -0.933329999999998
        },
        "Jefferson, OK": {
            "county": "Jefferson, OK",
            "value": 0.0
        },
        "Kay, OK": {
            "county": "Kay, OK",
            "value": -1.5120000000000005
        },
        "Latimer, OK": {
            "county": "Latimer, OK",
            "value": 0.0
        },
        "Lincoln, OK": {
            "county": "Lincoln, OK",
            "value": -0.14000000000000057
        },
        "McCurtain, OK": {
            "county": "McCurtain, OK",
            "value": -2.1074999999999946
        },
        "Muskogee, OK": {
            "county": "Muskogee, OK",
            "value": -0.5726700000000022
        },
        "Oklahoma, OK": {
            "county": "Oklahoma, OK",
            "value": -0.8866700000000023
        },
        "Okmulgee, OK": {
            "county": "Okmulgee, OK",
            "value": 0.0
        },
        "Osage, OK": {
            "county": "Osage, OK",
            "value": 0.0
        },
        "Payne, OK": {
            "county": "Payne, OK",
            "value": 0.22800000000000153
        },
        "Pittsburg, OK": {
            "county": "Pittsburg, OK",
            "value": 0.0
        },
        "Pontotoc, OK": {
            "county": "Pontotoc, OK",
            "value": 0.5564999999999998
        },
        "Roger Mills, OK": {
            "county": "Roger Mills, OK",
            "value": 0.0
        },
        "Texas, OK": {
            "county": "Texas, OK",
            "value": 1.480000000000004
        },
        "Woodward, OK": {
            "county": "Woodward, OK",
            "value": 0.21000000000000085
        },
        "Baker, OR": {
            "county": "Baker, OR",
            "value": -1.5209999999999937
        },
        "Clackamas, OR": {
            "county": "Clackamas, OR",
            "value": -0.09266999999999825
        },
        "Columbia, OR": {
            "county": "Columbia, OR",
            "value": -0.6973300000000009
        },
        "Coos, OR": {
            "county": "Coos, OR",
            "value": -2.397999999999996
        },
        "Crook, OR": {
            "county": "Crook, OR",
            "value": 0.0
        },
        "Curry, OR": {
            "county": "Curry, OR",
            "value": 0.0
        },
        "Douglas, OR": {
            "county": "Douglas, OR",
            "value": -3.5253299999999967
        },
        "Grant, OR": {
            "county": "Grant, OR",
            "value": 1.674669999999999
        },
        "Harney, OR": {
            "county": "Harney, OR",
            "value": -0.1763300000000001
        },
        "Jackson, OR": {
            "county": "Jackson, OR",
            "value": 4.910000000000004
        },
        "Klamath, OR": {
            "county": "Klamath, OR",
            "value": 0.7573299999999961
        },
        "Lake, OR": {
            "county": "Lake, OR",
            "value": 0.691670000000002
        },
        "Lane, OR": {
            "county": "Lane, OR",
            "value": 0.5679999999999978
        },
        "Lincoln, OR": {
            "county": "Lincoln, OR",
            "value": 0.0
        },
        "Linn, OR": {
            "county": "Linn, OR",
            "value": 0.8016700000000014
        },
        "Malheur, OR": {
            "county": "Malheur, OR",
            "value": 0.010329999999996176
        },
        "Marion, OR": {
            "county": "Marion, OR",
            "value": 1.0344999999999942
        },
        "Tillamook, OR": {
            "county": "Tillamook, OR",
            "value": -1.3500000000000014
        },
        "Umatilla, OR": {
            "county": "Umatilla, OR",
            "value": 1.3983300000000014
        },
        "Wallowa, OR": {
            "county": "Wallowa, OR",
            "value": -1.7224999999999966
        },
        "Washington, OR": {
            "county": "Washington, OR",
            "value": -1.2525000000000048
        },
        "Wheeler, OR": {
            "county": "Wheeler, OR",
            "value": 0.06400000000000006
        },
        "Yamhill, OR": {
            "county": "Yamhill, OR",
            "value": 0.0
        },
        "Beaver, PA": {
            "county": "Beaver, PA",
            "value": -2.1300000000000026
        },
        "Bedford, PA": {
            "county": "Bedford, PA",
            "value": 1.009999999999998
        },
        "Berks, PA": {
            "county": "Berks, PA",
            "value": 0.6986699999999999
        },
        "Bucks, PA": {
            "county": "Bucks, PA",
            "value": 0.41166999999999376
        },
        "Cambria, PA": {
            "county": "Cambria, PA",
            "value": 0.22332999999999714
        },
        "Clinton, PA": {
            "county": "Clinton, PA",
            "value": -0.003669999999999618
        },
        "Crawford, PA": {
            "county": "Crawford, PA",
            "value": -0.4549999999999983
        },
        "Erie, PA": {
            "county": "Erie, PA",
            "value": 0.0
        },
        "Fayette, PA": {
            "county": "Fayette, PA",
            "value": -1.1403299999999987
        },
        "Indiana, PA": {
            "county": "Indiana, PA",
            "value": -0.6284999999999954
        },
        "Lancaster, PA": {
            "county": "Lancaster, PA",
            "value": 0.6019999999999968
        },
        "Luzerne, PA": {
            "county": "Luzerne, PA",
            "value": 4.57
        },
        "McKean, PA": {
            "county": "McKean, PA",
            "value": -0.5983299999999971
        },
        "Monroe, PA": {
            "county": "Monroe, PA",
            "value": 0.20833000000000368
        },
        "Susquehanna, PA": {
            "county": "Susquehanna, PA",
            "value": -0.21249999999999858
        },
        "Tioga, PA": {
            "county": "Tioga, PA",
            "value": 0.5883299999999991
        },
        "Charleston, SC": {
            "county": "Charleston, SC",
            "value": 0.0
        },
        "Cherokee, SC": {
            "county": "Cherokee, SC",
            "value": 0.4660000000000011
        },
        "Dillon, SC": {
            "county": "Dillon, SC",
            "value": -0.5375000000000014
        },
        "Georgetown, SC": {
            "county": "Georgetown, SC",
            "value": -0.12000000000000455
        },
        "Kershaw, SC": {
            "county": "Kershaw, SC",
            "value": 0.0
        },
        "Lancaster, SC": {
            "county": "Lancaster, SC",
            "value": 0.0
        },
        "Laurens, SC": {
            "county": "Laurens, SC",
            "value": 1.4099999999999966
        },
        "Newberry, SC": {
            "county": "Newberry, SC",
            "value": -0.18599999999999994
        },
        "Brookings, SD": {
            "county": "Brookings, SD",
            "value": 0.6456699999999955
        },
        "Butte, SD": {
            "county": "Butte, SD",
            "value": 1.5166700000000048
        },
        "Clark, SD": {
            "county": "Clark, SD",
            "value": 0.018000000000000682
        },
        "Day, SD": {
            "county": "Day, SD",
            "value": 0.07849999999999824
        },
        "Fall River, SD": {
            "county": "Fall River, SD",
            "value": 0.6539999999999964
        },
        "Gregory, SD": {
            "county": "Gregory, SD",
            "value": 0.0
        },
        "Haakon, SD": {
            "county": "Haakon, SD",
            "value": 0.0
        },
        "Hand, SD": {
            "county": "Hand, SD",
            "value": 0.48467000000000127
        },
        "Harding, SD": {
            "county": "Harding, SD",
            "value": 0.26266999999999996
        },
        "Lawrence, SD": {
            "county": "Lawrence, SD",
            "value": 0.8716699999999946
        },
        "Marshall, SD": {
            "county": "Marshall, SD",
            "value": 1.7860000000000014
        },
        "Mellette, SD": {
            "county": "Mellette, SD",
            "value": -0.38582999999999856
        },
        "Pennington, SD": {
            "county": "Pennington, SD",
            "value": 1.551000000000002
        },
        "Perkins, SD": {
            "county": "Perkins, SD",
            "value": 1.024000000000001
        },
        "Stanley, SD": {
            "county": "Stanley, SD",
            "value": -0.47332999999999714
        },
        "Tripp, SD": {
            "county": "Tripp, SD",
            "value": -0.4435000000000002
        },
        "Clay, TN": {
            "county": "Clay, TN",
            "value": 0.0
        },
        "Hawkins, TN": {
            "county": "Hawkins, TN",
            "value": -2.6000000000000014
        },
        "Lawrence, TN": {
            "county": "Lawrence, TN",
            "value": -1.8866700000000023
        },
        "Maury, TN": {
            "county": "Maury, TN",
            "value": 0.2616700000000023
        },
        "McMinn, TN": {
            "county": "McMinn, TN",
            "value": 0.2879999999999967
        },
        "Shelby, TN": {
            "county": "Shelby, TN",
            "value": 0.2366700000000037
        },
        "Tipton, TN": {
            "county": "Tipton, TN",
            "value": -1.4506699999999952
        },
        "Warren, TN": {
            "county": "Warren, TN",
            "value": 0.7205000000000013
        },
        "Wilson, TN": {
            "county": "Wilson, TN",
            "value": -0.9825000000000017
        },
        "Aransas, TX": {
            "county": "Aransas, TX",
            "value": 0.7199999999999989
        },
        "Atascosa, TX": {
            "county": "Atascosa, TX",
            "value": -1.2150000000000034
        },
        "Borden, TX": {
            "county": "Borden, TX",
            "value": 1.509999999999998
        },
        "Bosque, TX": {
            "county": "Bosque, TX",
            "value": 0.0
        },
        "Brewster, TX": {
            "county": "Brewster, TX",
            "value": 1.7591699999999975
        },
        "Cameron, TX": {
            "county": "Cameron, TX",
            "value": 0.5949999999999989
        },
        "Carson, TX": {
            "county": "Carson, TX",
            "value": 1.5083299999999937
        },
        "Cherokee, TX": {
            "county": "Cherokee, TX",
            "value": -0.22366999999999848
        },
        "Comanche, TX": {
            "county": "Comanche, TX",
            "value": 0.0
        },
        "Concho, TX": {
            "county": "Concho, TX",
            "value": -2.0224999999999937
        },
        "Coryell, TX": {
            "county": "Coryell, TX",
            "value": 1.6074999999999946
        },
        "Crane, TX": {
            "county": "Crane, TX",
            "value": 0.0
        },
        "Donley, TX": {
            "county": "Donley, TX",
            "value": 0.722999999999999
        },
        "Edwards, TX": {
            "county": "Edwards, TX",
            "value": -1.4233299999999929
        },
        "Ellis, TX": {
            "county": "Ellis, TX",
            "value": 0.0
        },
        "Erath, TX": {
            "county": "Erath, TX",
            "value": 0.6119999999999948
        },
        "Garza, TX": {
            "county": "Garza, TX",
            "value": -0.5700000000000003
        },
        "Gonzales, TX": {
            "county": "Gonzales, TX",
            "value": -0.39000000000000057
        },
        "Hardeman, TX": {
            "county": "Hardeman, TX",
            "value": -1.384999999999991
        },
        "Haskell, TX": {
            "county": "Haskell, TX",
            "value": -3.0400000000000063
        },
        "Hemphill, TX": {
            "county": "Hemphill, TX",
            "value": 0.0
        },
        "Hidalgo, TX": {
            "county": "Hidalgo, TX",
            "value": 1.3299999999999983
        },
        "Hopkins, TX": {
            "county": "Hopkins, TX",
            "value": -0.6940000000000026
        },
        "Jasper, TX": {
            "county": "Jasper, TX",
            "value": 1.9209999999999923
        },
        "Jeff Davis, TX": {
            "county": "Jeff Davis, TX",
            "value": -0.9899999999999949
        },
        "Jim Wells, TX": {
            "county": "Jim Wells, TX",
            "value": -0.14333000000000595
        },
        "Jones, TX": {
            "county": "Jones, TX",
            "value": -1.2633300000000034
        },
        "Kendall, TX": {
            "county": "Kendall, TX",
            "value": 0.33150000000000546
        },
        "Kerr, TX": {
            "county": "Kerr, TX",
            "value": 0.7730000000000103
        },
        "Lamar, TX": {
            "county": "Lamar, TX",
            "value": 2.027329999999992
        },
        "Lamb, TX": {
            "county": "Lamb, TX",
            "value": 0.7208299999999994
        },
        "Llano, TX": {
            "county": "Llano, TX",
            "value": -0.1599999999999966
        },
        "Madison, TX": {
            "county": "Madison, TX",
            "value": -1.3200000000000074
        },
        "McCulloch, TX": {
            "county": "McCulloch, TX",
            "value": 1.25
        },
        "Midland, TX": {
            "county": "Midland, TX",
            "value": 0.0
        },
        "Milam, TX": {
            "county": "Milam, TX",
            "value": -5.218999999999994
        },
        "Mills, TX": {
            "county": "Mills, TX",
            "value": -1.1000000000000085
        },
        "Montgomery, TX": {
            "county": "Montgomery, TX",
            "value": -0.08833000000001334
        },
        "Nueces, TX": {
            "county": "Nueces, TX",
            "value": 0.2960000000000065
        },
        "Oldham, TX": {
            "county": "Oldham, TX",
            "value": 0.4724999999999966
        },
        "Polk, TX": {
            "county": "Polk, TX",
            "value": 2.5066699999999997
        },
        "Presidio, TX": {
            "county": "Presidio, TX",
            "value": 0.0
        },
        "Red River, TX": {
            "county": "Red River, TX",
            "value": 1.1086699999999965
        },
        "Runnels, TX": {
            "county": "Runnels, TX",
            "value": -1.644999999999996
        },
        "Shackelford, TX": {
            "county": "Shackelford, TX",
            "value": -0.9874999999999972
        },
        "Tarrant, TX": {
            "county": "Tarrant, TX",
            "value": 2.5575000000000045
        },
        "Val Verde, TX": {
            "county": "Val Verde, TX",
            "value": 0.9283400000000057
        },
        "Victoria, TX": {
            "county": "Victoria, TX",
            "value": 0.170669999999987
        },
        "Webb, TX": {
            "county": "Webb, TX",
            "value": -0.5430000000000064
        },
        "Wheeler, TX": {
            "county": "Wheeler, TX",
            "value": 0.6126700000000014
        },
        "Willacy, TX": {
            "county": "Willacy, TX",
            "value": 0.2325000000000017
        },
        "Yoakum, TX": {
            "county": "Yoakum, TX",
            "value": -0.17332999999999998
        },
        "Beaver, UT": {
            "county": "Beaver, UT",
            "value": 1.95167
        },
        "Box Elder, UT": {
            "county": "Box Elder, UT",
            "value": 0.8100000000000023
        },
        "Carbon, UT": {
            "county": "Carbon, UT",
            "value": -0.2875000000000014
        },
        "Davis, UT": {
            "county": "Davis, UT",
            "value": 1.4053299999999993
        },
        "Duchesne, UT": {
            "county": "Duchesne, UT",
            "value": 1.2633300000000034
        },
        "Garfield, UT": {
            "county": "Garfield, UT",
            "value": -0.08432999999999424
        },
        "Grand, UT": {
            "county": "Grand, UT",
            "value": 0.0
        },
        "Iron, UT": {
            "county": "Iron, UT",
            "value": 2.454329999999999
        },
        "Juab, UT": {
            "county": "Juab, UT",
            "value": -0.6724999999999994
        },
        "Kane, UT": {
            "county": "Kane, UT",
            "value": -2.2076700000000002
        },
        "Millard, UT": {
            "county": "Millard, UT",
            "value": -0.4750000000000014
        },
        "Morgan, UT": {
            "county": "Morgan, UT",
            "value": -2.1499999999999986
        },
        "Piute, UT": {
            "county": "Piute, UT",
            "value": 0.0
        },
        "Rich, UT": {
            "county": "Rich, UT",
            "value": 2.009999999999998
        },
        "Salt Lake, UT": {
            "county": "Salt Lake, UT",
            "value": -0.575330000000001
        },
        "Sanpete, UT": {
            "county": "Sanpete, UT",
            "value": 0.4793300000000045
        },
        "Sevier, UT": {
            "county": "Sevier, UT",
            "value": 0.5266700000000029
        },
        "Summit, UT": {
            "county": "Summit, UT",
            "value": -0.32499999999999574
        },
        "Tooele, UT": {
            "county": "Tooele, UT",
            "value": 0.0
        },
        "Uintah, UT": {
            "county": "Uintah, UT",
            "value": 0.36199999999999477
        },
        "Utah, UT": {
            "county": "Utah, UT",
            "value": 2.200000000000003
        },
        "Wasatch, UT": {
            "county": "Wasatch, UT",
            "value": 2.2223299999999995
        },
        "Washington, UT": {
            "county": "Washington, UT",
            "value": 1.5726699999999951
        },
        "Weber, UT": {
            "county": "Weber, UT",
            "value": 3.9963300000000004
        },
        "Orange, VA": {
            "county": "Orange, VA",
            "value": -1.1299999999999955
        },
        "Page, VA": {
            "county": "Page, VA",
            "value": -1.3099999999999952
        },
        "Pulaski, VA": {
            "county": "Pulaski, VA",
            "value": 0.8599999999999994
        },
        "Roanoke, VA": {
            "county": "Roanoke, VA",
            "value": -0.36599999999999966
        },
        "Russell, VA": {
            "county": "Russell, VA",
            "value": 0.669000000000004
        },
        "Sussex, VA": {
            "county": "Sussex, VA",
            "value": -0.28667000000000087
        },
        "Addison, VT": {
            "county": "Addison, VT",
            "value": -0.12000000000000455
        },
        "Chittenden, VT": {
            "county": "Chittenden, VT",
            "value": 0.16167000000000087
        },
        "Chelan, WA": {
            "county": "Chelan, WA",
            "value": 1.524000000000001
        },
        "Grant, WA": {
            "county": "Grant, WA",
            "value": -0.006000000000000227
        },
        "Grays Harbor, WA": {
            "county": "Grays Harbor, WA",
            "value": -0.9183299999999974
        },
        "Klickitat, WA": {
            "county": "Klickitat, WA",
            "value": -2.6466700000000003
        },
        "Okanogan, WA": {
            "county": "Okanogan, WA",
            "value": 1.1483300000000014
        },
        "Pacific, WA": {
            "county": "Pacific, WA",
            "value": -1.6540000000000035
        },
        "Pierce, WA": {
            "county": "Pierce, WA",
            "value": 5.263999999999996
        },
        "Snohomish, WA": {
            "county": "Snohomish, WA",
            "value": -0.79833
        },
        "Stevens, WA": {
            "county": "Stevens, WA",
            "value": -1.2325000000000017
        },
        "Yakima, WA": {
            "county": "Yakima, WA",
            "value": 1.4600000000000009
        },
        "Bayfield, WI": {
            "county": "Bayfield, WI",
            "value": 0.0
        },
        "Crawford, WI": {
            "county": "Crawford, WI",
            "value": -0.731659999999998
        },
        "Door, WI": {
            "county": "Door, WI",
            "value": 0.2949999999999946
        },
        "Douglas, WI": {
            "county": "Douglas, WI",
            "value": -2.778329999999997
        },
        "Green, WI": {
            "county": "Green, WI",
            "value": 0.5829999999999984
        },
        "Iron, WI": {
            "county": "Iron, WI",
            "value": -2.0704999999999956
        },
        "Jefferson, WI": {
            "county": "Jefferson, WI",
            "value": -1.161999999999999
        },
        "Juneau, WI": {
            "county": "Juneau, WI",
            "value": -0.6580000000000013
        },
        "Lafayette, WI": {
            "county": "Lafayette, WI",
            "value": 0.0
        },
        "Langlade, WI": {
            "county": "Langlade, WI",
            "value": -2.102499999999999
        },
        "Manitowoc, WI": {
            "county": "Manitowoc, WI",
            "value": 0.289670000000001
        },
        "Milwaukee, WI": {
            "county": "Milwaukee, WI",
            "value": 0.517000000000003
        },
        "Monroe, WI": {
            "county": "Monroe, WI",
            "value": -0.5760000000000005
        },
        "Oconto, WI": {
            "county": "Oconto, WI",
            "value": 0.0
        },
        "Oneida, WI": {
            "county": "Oneida, WI",
            "value": 2.328330000000001
        },
        "Pierce, WI": {
            "county": "Pierce, WI",
            "value": -1.6899999999999977
        },
        "Polk, WI": {
            "county": "Polk, WI",
            "value": 0.3119999999999976
        },
        "Washington, WI": {
            "county": "Washington, WI",
            "value": -0.6553299999999993
        },
        "Waukesha, WI": {
            "county": "Waukesha, WI",
            "value": -2.0959999999999965
        },
        "Braxton, WV": {
            "county": "Braxton, WV",
            "value": -0.17499999999999716
        },
        "Greenbrier, WV": {
            "county": "Greenbrier, WV",
            "value": 0.38917
        },
        "Jackson, WV": {
            "county": "Jackson, WV",
            "value": 0.2816699999999983
        },
        "Lewis, WV": {
            "county": "Lewis, WV",
            "value": -0.4766600000000025
        },
        "Logan, WV": {
            "county": "Logan, WV",
            "value": 0.37750000000000483
        },
        "Marion, WV": {
            "county": "Marion, WV",
            "value": -0.23333000000000226
        },
        "Marshall, WV": {
            "county": "Marshall, WV",
            "value": -0.184669999999997
        },
        "Preston, WV": {
            "county": "Preston, WV",
            "value": -0.3400000000000034
        },
        "Albany, WY": {
            "county": "Albany, WY",
            "value": -0.21932999999999936
        },
        "Carbon, WY": {
            "county": "Carbon, WY",
            "value": -0.25966999999999985
        },
        "Converse, WY": {
            "county": "Converse, WY",
            "value": 0.0
        },
        "Fremont, WY": {
            "county": "Fremont, WY",
            "value": 3.67633
        },
        "Goshen, WY": {
            "county": "Goshen, WY",
            "value": 0.5366699999999938
        },
        "Laramie, WY": {
            "county": "Laramie, WY",
            "value": 1.556669999999997
        },
        "Lincoln, WY": {
            "county": "Lincoln, WY",
            "value": 1.7141699999999958
        },
        "Natrona, WY": {
            "county": "Natrona, WY",
            "value": -0.8533299999999997
        },
        "Park, WY": {
            "county": "Park, WY",
            "value": 1.9583299999999966
        },
        "Sublette, WY": {
            "county": "Sublette, WY",
            "value": -0.2150000000000034
        },
        "Teton, WY": {
            "county": "Teton, WY",
            "value": 1.3449999999999989
        },
        "Washakie, WY": {
            "county": "Washakie, WY",
            "value": 1.5386699999999962
        },
        "Hoonah-Angoon, AK": {
            "county": "Hoonah-Angoon, AK",
            "value": 0.0
        },
        "Juneau, AK": {
            "county": "Juneau, AK",
            "value": 0.0
        },
        "Lake and Peninsula, AK": {
            "county": "Lake and Peninsula, AK",
            "value": 0.0
        },
        "North Slope, AK": {
            "county": "North Slope, AK",
            "value": 0.0
        },
        "Sitka, AK": {
            "county": "Sitka, AK",
            "value": 0.0
        },
        "Barbour, AL": {
            "county": "Barbour, AL",
            "value": 0.0
        },
        "Covington, AL": {
            "county": "Covington, AL",
            "value": 0.0
        },
        "Cullman, AL": {
            "county": "Cullman, AL",
            "value": 0.0
        },
        "Dallas, AL": {
            "county": "Dallas, AL",
            "value": 0.0
        },
        "Escambia, AL": {
            "county": "Escambia, AL",
            "value": 0.0
        },
        "Marshall, AL": {
            "county": "Marshall, AL",
            "value": 0.0
        },
        "Winston, AL": {
            "county": "Winston, AL",
            "value": 0.0
        },
        "Crittenden, AR": {
            "county": "Crittenden, AR",
            "value": 0.0
        },
        "Logan, AR": {
            "county": "Logan, AR",
            "value": 0.0
        },
        "Navajo, AZ": {
            "county": "Navajo, AZ",
            "value": 0.0
        },
        "Alpine, CA": {
            "county": "Alpine, CA",
            "value": 0.0
        },
        "Del Norte, CA": {
            "county": "Del Norte, CA",
            "value": 0.0
        },
        "Glenn, CA": {
            "county": "Glenn, CA",
            "value": 0.0
        },
        "Mendocino, CA": {
            "county": "Mendocino, CA",
            "value": 0.0
        },
        "Merced, CA": {
            "county": "Merced, CA",
            "value": 0.0
        },
        "Modoc, CA": {
            "county": "Modoc, CA",
            "value": 0.0
        },
        "San Benito, CA": {
            "county": "San Benito, CA",
            "value": 0.0
        },
        "Ventura, CA": {
            "county": "Ventura, CA",
            "value": 0.0
        },
        "Boulder, CO": {
            "county": "Boulder, CO",
            "value": 0.0
        },
        "Grand, CO": {
            "county": "Grand, CO",
            "value": 0.0
        },
        "Montezuma, CO": {
            "county": "Montezuma, CO",
            "value": 0.0
        },
        "Pitkin, CO": {
            "county": "Pitkin, CO",
            "value": 0.0
        },
        "Rio Grande, CO": {
            "county": "Rio Grande, CO",
            "value": 0.0
        },
        "Saguache, CO": {
            "county": "Saguache, CO",
            "value": 0.0
        },
        "New Haven, CT": {
            "county": "New Haven, CT",
            "value": 0.0
        },
        "Sussex, DE": {
            "county": "Sussex, DE",
            "value": 0.0
        },
        "Jackson, FL": {
            "county": "Jackson, FL",
            "value": 0.0
        },
        "Lake, FL": {
            "county": "Lake, FL",
            "value": 0.0
        },
        "Liberty, FL": {
            "county": "Liberty, FL",
            "value": 0.0
        },
        "Manatee, FL": {
            "county": "Manatee, FL",
            "value": 0.0
        },
        "Pinellas, FL": {
            "county": "Pinellas, FL",
            "value": 0.0
        },
        "Wakulla, FL": {
            "county": "Wakulla, FL",
            "value": 0.0
        },
        "Clarke, GA": {
            "county": "Clarke, GA",
            "value": 0.0
        },
        "Lumpkin, GA": {
            "county": "Lumpkin, GA",
            "value": 0.0
        },
        "Mitchell, GA": {
            "county": "Mitchell, GA",
            "value": 0.0
        },
        "Sumter, GA": {
            "county": "Sumter, GA",
            "value": 0.0
        },
        "Wilkinson, GA": {
            "county": "Wilkinson, GA",
            "value": 0.0
        },
        "Adams, ID": {
            "county": "Adams, ID",
            "value": 0.0
        },
        "Boise, ID": {
            "county": "Boise, ID",
            "value": 0.0
        },
        "Nez Perce, ID": {
            "county": "Nez Perce, ID",
            "value": 0.0
        },
        "Cook, IL": {
            "county": "Cook, IL",
            "value": 0.0
        },
        "Jackson, IL": {
            "county": "Jackson, IL",
            "value": 0.0
        },
        "Shelby, IL": {
            "county": "Shelby, IL",
            "value": 0.0
        },
        "St. Clair, IL": {
            "county": "St. Clair, IL",
            "value": 0.0
        },
        "Johnson, IN": {
            "county": "Johnson, IN",
            "value": 0.0
        },
        "Lawrence, IN": {
            "county": "Lawrence, IN",
            "value": 0.0
        },
        "Pulaski, IN": {
            "county": "Pulaski, IN",
            "value": 0.0
        },
        "Spencer, IN": {
            "county": "Spencer, IN",
            "value": 0.0
        },
        "Barber, KS": {
            "county": "Barber, KS",
            "value": 0.0
        },
        "Jackson, KS": {
            "county": "Jackson, KS",
            "value": 0.0
        },
        "Jefferson, KS": {
            "county": "Jefferson, KS",
            "value": 0.0
        },
        "Nemaha, KS": {
            "county": "Nemaha, KS",
            "value": 0.0
        },
        "Neosho, KS": {
            "county": "Neosho, KS",
            "value": 0.0
        },
        "Edmonson, KY": {
            "county": "Edmonson, KY",
            "value": 0.0
        },
        "Greenup, KY": {
            "county": "Greenup, KY",
            "value": 0.0
        },
        "Logan, KY": {
            "county": "Logan, KY",
            "value": 0.0
        },
        "Lyon, KY": {
            "county": "Lyon, KY",
            "value": 0.0
        },
        "McCracken, KY": {
            "county": "McCracken, KY",
            "value": 0.0
        },
        "Owsley, KY": {
            "county": "Owsley, KY",
            "value": 0.0
        },
        "Pulaski, KY": {
            "county": "Pulaski, KY",
            "value": 0.0
        },
        "Wolfe, KY": {
            "county": "Wolfe, KY",
            "value": 0.0
        },
        "Jackson, LA": {
            "county": "Jackson, LA",
            "value": 0.0
        },
        "Union, LA": {
            "county": "Union, LA",
            "value": 0.0
        },
        "Frederick, MD": {
            "county": "Frederick, MD",
            "value": 0.0
        },
        "Washington, MD": {
            "county": "Washington, MD",
            "value": 0.0
        },
        "Piscataquis, ME": {
            "county": "Piscataquis, ME",
            "value": 0.0
        },
        "Gogebic, MI": {
            "county": "Gogebic, MI",
            "value": 0.0
        },
        "Lake, MI": {
            "county": "Lake, MI",
            "value": 0.0
        },
        "Lenawee, MI": {
            "county": "Lenawee, MI",
            "value": 0.0
        },
        "Montmorency, MI": {
            "county": "Montmorency, MI",
            "value": 0.0
        },
        "Tuscola, MI": {
            "county": "Tuscola, MI",
            "value": 0.0
        },
        "Anoka, MN": {
            "county": "Anoka, MN",
            "value": 0.0
        },
        "Koochiching, MN": {
            "county": "Koochiching, MN",
            "value": 0.0
        },
        "Lake, MN": {
            "county": "Lake, MN",
            "value": 0.0
        },
        "Martin, MN": {
            "county": "Martin, MN",
            "value": 0.0
        },
        "McLeod, MN": {
            "county": "McLeod, MN",
            "value": 0.0
        },
        "Meeker, MN": {
            "county": "Meeker, MN",
            "value": 0.0
        },
        "St. Louis, MN": {
            "county": "St. Louis, MN",
            "value": 0.0
        },
        "Wilkin, MN": {
            "county": "Wilkin, MN",
            "value": 0.0
        },
        "Carter, MO": {
            "county": "Carter, MO",
            "value": 0.0
        },
        "Douglas, MO": {
            "county": "Douglas, MO",
            "value": 0.0
        },
        "Franklin, MO": {
            "county": "Franklin, MO",
            "value": 0.0
        },
        "Ray, MO": {
            "county": "Ray, MO",
            "value": 0.0
        },
        "Schuyler, MO": {
            "county": "Schuyler, MO",
            "value": 0.0
        },
        "Texas, MO": {
            "county": "Texas, MO",
            "value": 0.0
        },
        "Forrest, MS": {
            "county": "Forrest, MS",
            "value": 0.0
        },
        "Kemper, MS": {
            "county": "Kemper, MS",
            "value": 0.0
        },
        "Smith, MS": {
            "county": "Smith, MS",
            "value": 0.0
        },
        "Tate, MS": {
            "county": "Tate, MS",
            "value": 0.0
        },
        "Wayne, MS": {
            "county": "Wayne, MS",
            "value": 0.0
        },
        "Fallon, MT": {
            "county": "Fallon, MT",
            "value": 0.0
        },
        "Flathead, MT": {
            "county": "Flathead, MT",
            "value": 0.0
        },
        "Judith Basin, MT": {
            "county": "Judith Basin, MT",
            "value": 0.0
        },
        "Sanders, MT": {
            "county": "Sanders, MT",
            "value": 0.0
        },
        "Sweet Grass, MT": {
            "county": "Sweet Grass, MT",
            "value": 0.0
        },
        "Beaufort, NC": {
            "county": "Beaufort, NC",
            "value": 0.0
        },
        "Clay, NC": {
            "county": "Clay, NC",
            "value": 0.0
        },
        "Nash, NC": {
            "county": "Nash, NC",
            "value": 0.0
        },
        "Pamlico, NC": {
            "county": "Pamlico, NC",
            "value": 0.0
        },
        "Washington, NC": {
            "county": "Washington, NC",
            "value": 0.0
        },
        "Adams, ND": {
            "county": "Adams, ND",
            "value": 0.0
        },
        "Chase, NE": {
            "county": "Chase, NE",
            "value": 0.0
        },
        "Cherry, NE": {
            "county": "Cherry, NE",
            "value": 0.0
        },
        "Dixon, NE": {
            "county": "Dixon, NE",
            "value": 0.0
        },
        "Garfield, NE": {
            "county": "Garfield, NE",
            "value": 0.0
        },
        "Pawnee, NE": {
            "county": "Pawnee, NE",
            "value": 0.0
        },
        "Stanton, NE": {
            "county": "Stanton, NE",
            "value": 0.0
        },
        "Carroll, NH": {
            "county": "Carroll, NH",
            "value": 0.0
        },
        "Rockingham, NH": {
            "county": "Rockingham, NH",
            "value": 0.0
        },
        "Hudson, NJ": {
            "county": "Hudson, NJ",
            "value": 0.0
        },
        "Chaves, NM": {
            "county": "Chaves, NM",
            "value": 0.0
        },
        "Dona Ana, NM": {
            "county": "Dona Ana, NM",
            "value": 0.0
        },
        "Harding, NM": {
            "county": "Harding, NM",
            "value": 0.0
        },
        "Quay, NM": {
            "county": "Quay, NM",
            "value": 0.0
        },
        "San Juan, NM": {
            "county": "San Juan, NM",
            "value": 0.0
        },
        "Union, NM": {
            "county": "Union, NM",
            "value": 0.0
        },
        "Lander, NV": {
            "county": "Lander, NV",
            "value": 0.0
        },
        "Allegany, NY": {
            "county": "Allegany, NY",
            "value": 0.0
        },
        "Bronx, NY": {
            "county": "Bronx, NY",
            "value": 0.0
        },
        "Chenango, NY": {
            "county": "Chenango, NY",
            "value": 0.0
        },
        "Dutchess, NY": {
            "county": "Dutchess, NY",
            "value": 0.0
        },
        "Schenectady, NY": {
            "county": "Schenectady, NY",
            "value": 0.0
        },
        "Westchester, NY": {
            "county": "Westchester, NY",
            "value": 0.0
        },
        "Noble, OH": {
            "county": "Noble, OH",
            "value": 0.0
        },
        "Summit, OH": {
            "county": "Summit, OH",
            "value": 0.0
        },
        "Atoka, OK": {
            "county": "Atoka, OK",
            "value": 0.0
        },
        "Coal, OK": {
            "county": "Coal, OK",
            "value": 0.0
        },
        "Dewey, OK": {
            "county": "Dewey, OK",
            "value": 0.0
        },
        "Garfield, OK": {
            "county": "Garfield, OK",
            "value": 0.0
        },
        "Garvin, OK": {
            "county": "Garvin, OK",
            "value": 0.0
        },
        "Grady, OK": {
            "county": "Grady, OK",
            "value": 0.0
        },
        "Johnston, OK": {
            "county": "Johnston, OK",
            "value": 0.0
        },
        "McClain, OK": {
            "county": "McClain, OK",
            "value": 0.0
        },
        "Tillman, OK": {
            "county": "Tillman, OK",
            "value": 0.0
        },
        "Benton, OR": {
            "county": "Benton, OR",
            "value": 0.0
        },
        "Jefferson, OR": {
            "county": "Jefferson, OR",
            "value": 0.0
        },
        "Josephine, OR": {
            "county": "Josephine, OR",
            "value": 0.0
        },
        "Union, OR": {
            "county": "Union, OR",
            "value": 0.0
        },
        "Wasco, OR": {
            "county": "Wasco, OR",
            "value": 0.0
        },
        "Huntingdon, PA": {
            "county": "Huntingdon, PA",
            "value": 0.0
        },
        "Jefferson, PA": {
            "county": "Jefferson, PA",
            "value": 0.0
        },
        "Mercer, PA": {
            "county": "Mercer, PA",
            "value": 0.0
        },
        "Potter, PA": {
            "county": "Potter, PA",
            "value": 0.0
        },
        "Somerset, PA": {
            "county": "Somerset, PA",
            "value": 0.0
        },
        "Washington, RI": {
            "county": "Washington, RI",
            "value": 0.0
        },
        "Oconee, SC": {
            "county": "Oconee, SC",
            "value": 0.0
        },
        "Custer, SD": {
            "county": "Custer, SD",
            "value": 0.0
        },
        "McPherson, SD": {
            "county": "McPherson, SD",
            "value": 0.0
        },
        "Blount, TN": {
            "county": "Blount, TN",
            "value": 0.0
        },
        "Fentress, TN": {
            "county": "Fentress, TN",
            "value": 0.0
        },
        "Lewis, TN": {
            "county": "Lewis, TN",
            "value": 0.0
        },
        "Loudon, TN": {
            "county": "Loudon, TN",
            "value": 0.0
        },
        "Marshall, TN": {
            "county": "Marshall, TN",
            "value": 0.0
        },
        "Sumner, TN": {
            "county": "Sumner, TN",
            "value": 0.0
        },
        "Williamson, TN": {
            "county": "Williamson, TN",
            "value": 0.0
        },
        "Anderson, TX": {
            "county": "Anderson, TX",
            "value": 0.0
        },
        "Brazoria, TX": {
            "county": "Brazoria, TX",
            "value": 0.0
        },
        "Duval, TX": {
            "county": "Duval, TX",
            "value": 0.0
        },
        "Harrison, TX": {
            "county": "Harrison, TX",
            "value": 0.0
        },
        "Hartley, TX": {
            "county": "Hartley, TX",
            "value": 0.0
        },
        "Kleberg, TX": {
            "county": "Kleberg, TX",
            "value": 0.0
        },
        "Liberty, TX": {
            "county": "Liberty, TX",
            "value": 0.0
        },
        "McMullen, TX": {
            "county": "McMullen, TX",
            "value": 0.0
        },
        "Palo Pinto, TX": {
            "county": "Palo Pinto, TX",
            "value": 0.0
        },
        "San Saba, TX": {
            "county": "San Saba, TX",
            "value": 0.0
        },
        "Taylor, TX": {
            "county": "Taylor, TX",
            "value": 0.0
        },
        "Travis, TX": {
            "county": "Travis, TX",
            "value": 0.0
        },
        "Wayne, UT": {
            "county": "Wayne, UT",
            "value": 0.0
        },
        "Albemarle, VA": {
            "county": "Albemarle, VA",
            "value": 0.0
        },
        "Chesapeake City, VA": {
            "county": "Chesapeake City, VA",
            "value": 0.0
        },
        "Wise, VA": {
            "county": "Wise, VA",
            "value": 0.0
        },
        "Caledonia, VT": {
            "county": "Caledonia, VT",
            "value": 0.0
        },
        "Essex, VT": {
            "county": "Essex, VT",
            "value": 0.0
        },
        "Windsor, VT": {
            "county": "Windsor, VT",
            "value": 0.0
        },
        "Franklin, WA": {
            "county": "Franklin, WA",
            "value": 0.0
        },
        "Jefferson, WA": {
            "county": "Jefferson, WA",
            "value": 0.0
        },
        "Skagit, WA": {
            "county": "Skagit, WA",
            "value": 0.0
        },
        "Whatcom, WA": {
            "county": "Whatcom, WA",
            "value": 0.0
        },
        "Brown, WI": {
            "county": "Brown, WI",
            "value": 0.0
        },
        "Marinette, WI": {
            "county": "Marinette, WI",
            "value": 0.0
        },
        "Ozaukee, WI": {
            "county": "Ozaukee, WI",
            "value": 0.0
        },
        "Walworth, WI": {
            "county": "Walworth, WI",
            "value": 0.0
        },
        "Fayette, WV": {
            "county": "Fayette, WV",
            "value": 0.0
        },
        "Jefferson, WV": {
            "county": "Jefferson, WV",
            "value": 0.0
        },
        "Pocahontas, WV": {
            "county": "Pocahontas, WV",
            "value": 0.0
        },
        "Tucker, WV": {
            "county": "Tucker, WV",
            "value": 0.0
        },
        "Campbell, WY": {
            "county": "Campbell, WY",
            "value": 0.0
        },
        "Sweetwater, WY": {
            "county": "Sweetwater, WY",
            "value": 0.0
        },
        "Autauga, AL": {
            "county": "Autauga, AL",
            "value": 0
        },
        "Baldwin, AL": {
            "county": "Baldwin, AL",
            "value": 0
        },
        "Bibb, AL": {
            "county": "Bibb, AL",
            "value": 0
        },
        "Blount, AL": {
            "county": "Blount, AL",
            "value": 0
        },
        "Bullock, AL": {
            "county": "Bullock, AL",
            "value": 0
        },
        "Butler, AL": {
            "county": "Butler, AL",
            "value": 0
        },
        "Calhoun, AL": {
            "county": "Calhoun, AL",
            "value": 0
        },
        "Chambers, AL": {
            "county": "Chambers, AL",
            "value": 0
        },
        "Cherokee, AL": {
            "county": "Cherokee, AL",
            "value": 0
        },
        "Chilton, AL": {
            "county": "Chilton, AL",
            "value": 0
        },
        "Choctaw, AL": {
            "county": "Choctaw, AL",
            "value": 0
        },
        "Clarke, AL": {
            "county": "Clarke, AL",
            "value": 0
        },
        "Clay, AL": {
            "county": "Clay, AL",
            "value": 0
        },
        "Cleburne, AL": {
            "county": "Cleburne, AL",
            "value": 0
        },
        "Coffee, AL": {
            "county": "Coffee, AL",
            "value": 0
        },
        "Colbert, AL": {
            "county": "Colbert, AL",
            "value": 0
        },
        "Conecuh, AL": {
            "county": "Conecuh, AL",
            "value": 0
        },
        "Coosa, AL": {
            "county": "Coosa, AL",
            "value": 0
        },
        "Crenshaw, AL": {
            "county": "Crenshaw, AL",
            "value": 0
        },
        "Dale, AL": {
            "county": "Dale, AL",
            "value": 0
        },
        "DeKalb, AL": {
            "county": "DeKalb, AL",
            "value": 0
        },
        "Elmore, AL": {
            "county": "Elmore, AL",
            "value": 0
        },
        "Etowah, AL": {
            "county": "Etowah, AL",
            "value": 0
        },
        "Fayette, AL": {
            "county": "Fayette, AL",
            "value": 0
        },
        "Franklin, AL": {
            "county": "Franklin, AL",
            "value": 0
        },
        "Geneva, AL": {
            "county": "Geneva, AL",
            "value": 0
        },
        "Greene, AL": {
            "county": "Greene, AL",
            "value": 0
        },
        "Hale, AL": {
            "county": "Hale, AL",
            "value": 0
        },
        "Henry, AL": {
            "county": "Henry, AL",
            "value": 0
        },
        "Houston, AL": {
            "county": "Houston, AL",
            "value": 0
        },
        "Jackson, AL": {
            "county": "Jackson, AL",
            "value": 0
        },
        "Jefferson, AL": {
            "county": "Jefferson, AL",
            "value": 0
        },
        "Lamar, AL": {
            "county": "Lamar, AL",
            "value": 0
        },
        "Lauderdale, AL": {
            "county": "Lauderdale, AL",
            "value": 0
        },
        "Lawrence, AL": {
            "county": "Lawrence, AL",
            "value": 0
        },
        "Lee, AL": {
            "county": "Lee, AL",
            "value": 0
        },
        "Limestone, AL": {
            "county": "Limestone, AL",
            "value": 0
        },
        "Lowndes, AL": {
            "county": "Lowndes, AL",
            "value": 0
        },
        "Macon, AL": {
            "county": "Macon, AL",
            "value": 0
        },
        "Madison, AL": {
            "county": "Madison, AL",
            "value": 0
        },
        "Marengo, AL": {
            "county": "Marengo, AL",
            "value": 0
        },
        "Marion, AL": {
            "county": "Marion, AL",
            "value": 0
        },
        "Mobile, AL": {
            "county": "Mobile, AL",
            "value": 0
        },
        "Monroe, AL": {
            "county": "Monroe, AL",
            "value": 0
        },
        "Montgomery, AL": {
            "county": "Montgomery, AL",
            "value": 0
        },
        "Morgan, AL": {
            "county": "Morgan, AL",
            "value": 0
        },
        "Perry, AL": {
            "county": "Perry, AL",
            "value": 0
        },
        "Pickens, AL": {
            "county": "Pickens, AL",
            "value": 0
        },
        "Pike, AL": {
            "county": "Pike, AL",
            "value": 0
        },
        "Randolph, AL": {
            "county": "Randolph, AL",
            "value": 0
        },
        "Russell, AL": {
            "county": "Russell, AL",
            "value": 0
        },
        "St. Clair, AL": {
            "county": "St. Clair, AL",
            "value": 0
        },
        "Shelby, AL": {
            "county": "Shelby, AL",
            "value": 0
        },
        "Sumter, AL": {
            "county": "Sumter, AL",
            "value": 0
        },
        "Talladega, AL": {
            "county": "Talladega, AL",
            "value": 0
        },
        "Tallapoosa, AL": {
            "county": "Tallapoosa, AL",
            "value": 0
        },
        "Tuscaloosa, AL": {
            "county": "Tuscaloosa, AL",
            "value": 0
        },
        "Walker, AL": {
            "county": "Walker, AL",
            "value": 0
        },
        "Washington, AL": {
            "county": "Washington, AL",
            "value": 0
        },
        "Wilcox, AL": {
            "county": "Wilcox, AL",
            "value": 0
        },
        "Aleutians East, AK": {
            "county": "Aleutians East, AK",
            "value": 0
        },
        "Bristol Bay, AK": {
            "county": "Bristol Bay, AK",
            "value": 0
        },
        "Dillingham, AK": {
            "county": "Dillingham, AK",
            "value": 0
        },
        "Haines, AK": {
            "county": "Haines, AK",
            "value": 0
        },
        "Skagway-Hoonah-Angoon, AK": {
            "county": "Skagway-Hoonah-Angoon, AK",
            "value": 0
        },
        "Kodiak Island, AK": {
            "county": "Kodiak Island, AK",
            "value": 0
        },
        "Nome, AK": {
            "county": "Nome, AK",
            "value": 0
        },
        "Wrangell-Petersburg, AK": {
            "county": "Wrangell-Petersburg, AK",
            "value": 0
        },
        "Prince of Wales-Outer Ketchikan, AK": {
            "county": "Prince of Wales-Outer Ketchikan, AK",
            "value": 0
        },
        "SE Fairbanks, AK": {
            "county": "SE Fairbanks, AK",
            "value": 0
        },
        "Wade Hampton, AK": {
            "county": "Wade Hampton, AK",
            "value": 0
        },
        "Yakutat, AK": {
            "county": "Yakutat, AK",
            "value": 0
        },
        "Gila, AZ": {
            "county": "Gila, AZ",
            "value": 0
        },
        "Graham, AZ": {
            "county": "Graham, AZ",
            "value": 0
        },
        "La Paz, AZ": {
            "county": "La Paz, AZ",
            "value": 0
        },
        "Maricopa, AZ": {
            "county": "Maricopa, AZ",
            "value": 0
        },
        "Pinal, AZ": {
            "county": "Pinal, AZ",
            "value": 0
        },
        "Santa Cruz, AZ": {
            "county": "Santa Cruz, AZ",
            "value": 0
        },
        "Yuma, AZ": {
            "county": "Yuma, AZ",
            "value": 0
        },
        "Arkansas, AR": {
            "county": "Arkansas, AR",
            "value": 0
        },
        "Ashley, AR": {
            "county": "Ashley, AR",
            "value": 0
        },
        "Baxter, AR": {
            "county": "Baxter, AR",
            "value": 0
        },
        "Benton, AR": {
            "county": "Benton, AR",
            "value": 0
        },
        "Boone, AR": {
            "county": "Boone, AR",
            "value": 0
        },
        "Bradley, AR": {
            "county": "Bradley, AR",
            "value": 0
        },
        "Calhoun, AR": {
            "county": "Calhoun, AR",
            "value": 0
        },
        "Carroll, AR": {
            "county": "Carroll, AR",
            "value": 0
        },
        "Chicot, AR": {
            "county": "Chicot, AR",
            "value": 0
        },
        "Clark, AR": {
            "county": "Clark, AR",
            "value": 0
        },
        "Clay, AR": {
            "county": "Clay, AR",
            "value": 0
        },
        "Cleburne, AR": {
            "county": "Cleburne, AR",
            "value": 0
        },
        "Cleveland, AR": {
            "county": "Cleveland, AR",
            "value": 0
        },
        "Columbia, AR": {
            "county": "Columbia, AR",
            "value": 0
        },
        "Conway, AR": {
            "county": "Conway, AR",
            "value": 0
        },
        "Craighead, AR": {
            "county": "Craighead, AR",
            "value": 0
        },
        "Crawford, AR": {
            "county": "Crawford, AR",
            "value": 0
        },
        "Cross, AR": {
            "county": "Cross, AR",
            "value": 0
        },
        "Dallas, AR": {
            "county": "Dallas, AR",
            "value": 0
        },
        "Desha, AR": {
            "county": "Desha, AR",
            "value": 0
        },
        "Drew, AR": {
            "county": "Drew, AR",
            "value": 0
        },
        "Faulkner, AR": {
            "county": "Faulkner, AR",
            "value": 0
        },
        "Franklin, AR": {
            "county": "Franklin, AR",
            "value": 0
        },
        "Fulton, AR": {
            "county": "Fulton, AR",
            "value": 0
        },
        "Garland, AR": {
            "county": "Garland, AR",
            "value": 0
        },
        "Grant, AR": {
            "county": "Grant, AR",
            "value": 0
        },
        "Greene, AR": {
            "county": "Greene, AR",
            "value": 0
        },
        "Hempstead, AR": {
            "county": "Hempstead, AR",
            "value": 0
        },
        "Hot Spring, AR": {
            "county": "Hot Spring, AR",
            "value": 0
        },
        "Howard, AR": {
            "county": "Howard, AR",
            "value": 0
        },
        "Independence, AR": {
            "county": "Independence, AR",
            "value": 0
        },
        "Izard, AR": {
            "county": "Izard, AR",
            "value": 0
        },
        "Jackson, AR": {
            "county": "Jackson, AR",
            "value": 0
        },
        "Jefferson, AR": {
            "county": "Jefferson, AR",
            "value": 0
        },
        "Johnson, AR": {
            "county": "Johnson, AR",
            "value": 0
        },
        "Lafayette, AR": {
            "county": "Lafayette, AR",
            "value": 0
        },
        "Lawrence, AR": {
            "county": "Lawrence, AR",
            "value": 0
        },
        "Lee, AR": {
            "county": "Lee, AR",
            "value": 0
        },
        "Lincoln, AR": {
            "county": "Lincoln, AR",
            "value": 0
        },
        "Little River, AR": {
            "county": "Little River, AR",
            "value": 0
        },
        "Lonoke, AR": {
            "county": "Lonoke, AR",
            "value": 0
        },
        "Madison, AR": {
            "county": "Madison, AR",
            "value": 0
        },
        "Marion, AR": {
            "county": "Marion, AR",
            "value": 0
        },
        "Miller, AR": {
            "county": "Miller, AR",
            "value": 0
        },
        "Mississippi, AR": {
            "county": "Mississippi, AR",
            "value": 0
        },
        "Monroe, AR": {
            "county": "Monroe, AR",
            "value": 0
        },
        "Montgomery, AR": {
            "county": "Montgomery, AR",
            "value": 0
        },
        "Nevada, AR": {
            "county": "Nevada, AR",
            "value": 0
        },
        "Newton, AR": {
            "county": "Newton, AR",
            "value": 0
        },
        "Ouachita, AR": {
            "county": "Ouachita, AR",
            "value": 0
        },
        "Perry, AR": {
            "county": "Perry, AR",
            "value": 0
        },
        "Phillips, AR": {
            "county": "Phillips, AR",
            "value": 0
        },
        "Pike, AR": {
            "county": "Pike, AR",
            "value": 0
        },
        "Poinsett, AR": {
            "county": "Poinsett, AR",
            "value": 0
        },
        "Polk, AR": {
            "county": "Polk, AR",
            "value": 0
        },
        "Pope, AR": {
            "county": "Pope, AR",
            "value": 0
        },
        "Prairie, AR": {
            "county": "Prairie, AR",
            "value": 0
        },
        "Pulaski, AR": {
            "county": "Pulaski, AR",
            "value": 0
        },
        "Randolph, AR": {
            "county": "Randolph, AR",
            "value": 0
        },
        "St. Francis, AR": {
            "county": "St. Francis, AR",
            "value": 0
        },
        "Saline, AR": {
            "county": "Saline, AR",
            "value": 0
        },
        "Scott, AR": {
            "county": "Scott, AR",
            "value": 0
        },
        "Searcy, AR": {
            "county": "Searcy, AR",
            "value": 0
        },
        "Sebastian, AR": {
            "county": "Sebastian, AR",
            "value": 0
        },
        "Sevier, AR": {
            "county": "Sevier, AR",
            "value": 0
        },
        "Sharp, AR": {
            "county": "Sharp, AR",
            "value": 0
        },
        "Stone, AR": {
            "county": "Stone, AR",
            "value": 0
        },
        "Union, AR": {
            "county": "Union, AR",
            "value": 0
        },
        "Van Buren, AR": {
            "county": "Van Buren, AR",
            "value": 0
        },
        "Washington, AR": {
            "county": "Washington, AR",
            "value": 0
        },
        "White, AR": {
            "county": "White, AR",
            "value": 0
        },
        "Woodruff, AR": {
            "county": "Woodruff, AR",
            "value": 0
        },
        "Yell, AR": {
            "county": "Yell, AR",
            "value": 0
        },
        "Amador, CA": {
            "county": "Amador, CA",
            "value": 0
        },
        "Calaveras, CA": {
            "county": "Calaveras, CA",
            "value": 0
        },
        "Colusa, CA": {
            "county": "Colusa, CA",
            "value": 0
        },
        "Contra Costa, CA": {
            "county": "Contra Costa, CA",
            "value": 0
        },
        "Imperial, CA": {
            "county": "Imperial, CA",
            "value": 0
        },
        "Lake, CA": {
            "county": "Lake, CA",
            "value": 0
        },
        "Madera, CA": {
            "county": "Madera, CA",
            "value": 0
        },
        "Marin, CA": {
            "county": "Marin, CA",
            "value": 0
        },
        "Mono, CA": {
            "county": "Mono, CA",
            "value": 0
        },
        "Napa, CA": {
            "county": "Napa, CA",
            "value": 0
        },
        "Orange, CA": {
            "county": "Orange, CA",
            "value": 0
        },
        "Plumas, CA": {
            "county": "Plumas, CA",
            "value": 0
        },
        "San Joaquin, CA": {
            "county": "San Joaquin, CA",
            "value": 0
        },
        "San Mateo, CA": {
            "county": "San Mateo, CA",
            "value": 0
        },
        "Santa Clara, CA": {
            "county": "Santa Clara, CA",
            "value": 0
        },
        "Santa Cruz, CA": {
            "county": "Santa Cruz, CA",
            "value": 0
        },
        "Solano, CA": {
            "county": "Solano, CA",
            "value": 0
        },
        "Sutter, CA": {
            "county": "Sutter, CA",
            "value": 0
        },
        "Tuolumne, CA": {
            "county": "Tuolumne, CA",
            "value": 0
        },
        "Yolo, CA": {
            "county": "Yolo, CA",
            "value": 0
        },
        "Adams, CO": {
            "county": "Adams, CO",
            "value": 0
        },
        "Alamosa, CO": {
            "county": "Alamosa, CO",
            "value": 0
        },
        "Arapahoe, CO": {
            "county": "Arapahoe, CO",
            "value": 0
        },
        "Archuleta, CO": {
            "county": "Archuleta, CO",
            "value": 0
        },
        "Bent, CO": {
            "county": "Bent, CO",
            "value": 0
        },
        "Broomfield, CO": {
            "county": "Broomfield, CO",
            "value": 0
        },
        "Chaffee, CO": {
            "county": "Chaffee, CO",
            "value": 0
        },
        "Cheyenne, CO": {
            "county": "Cheyenne, CO",
            "value": 0
        },
        "Clear Creek, CO": {
            "county": "Clear Creek, CO",
            "value": 0
        },
        "Conejos, CO": {
            "county": "Conejos, CO",
            "value": 0
        },
        "Costilla, CO": {
            "county": "Costilla, CO",
            "value": 0
        },
        "Crowley, CO": {
            "county": "Crowley, CO",
            "value": 0
        },
        "Custer, CO": {
            "county": "Custer, CO",
            "value": 0
        },
        "Dolores, CO": {
            "county": "Dolores, CO",
            "value": 0
        },
        "Eagle, CO": {
            "county": "Eagle, CO",
            "value": 0
        },
        "Elbert, CO": {
            "county": "Elbert, CO",
            "value": 0
        },
        "El Paso, CO": {
            "county": "El Paso, CO",
            "value": 0
        },
        "Fremont, CO": {
            "county": "Fremont, CO",
            "value": 0
        },
        "Gilpin, CO": {
            "county": "Gilpin, CO",
            "value": 0
        },
        "Gunnison, CO": {
            "county": "Gunnison, CO",
            "value": 0
        },
        "Kiowa, CO": {
            "county": "Kiowa, CO",
            "value": 0
        },
        "Kit Carson, CO": {
            "county": "Kit Carson, CO",
            "value": 0
        },
        "Lake, CO": {
            "county": "Lake, CO",
            "value": 0
        },
        "La Plata, CO": {
            "county": "La Plata, CO",
            "value": 0
        },
        "Larimer, CO": {
            "county": "Larimer, CO",
            "value": 0
        },
        "Lincoln, CO": {
            "county": "Lincoln, CO",
            "value": 0
        },
        "Logan, CO": {
            "county": "Logan, CO",
            "value": 0
        },
        "Morgan, CO": {
            "county": "Morgan, CO",
            "value": 0
        },
        "Ouray, CO": {
            "county": "Ouray, CO",
            "value": 0
        },
        "Park, CO": {
            "county": "Park, CO",
            "value": 0
        },
        "Phillips, CO": {
            "county": "Phillips, CO",
            "value": 0
        },
        "Prowers, CO": {
            "county": "Prowers, CO",
            "value": 0
        },
        "Pueblo, CO": {
            "county": "Pueblo, CO",
            "value": 0
        },
        "San Juan, CO": {
            "county": "San Juan, CO",
            "value": 0
        },
        "San Miguel, CO": {
            "county": "San Miguel, CO",
            "value": 0
        },
        "Sedgwick, CO": {
            "county": "Sedgwick, CO",
            "value": 0
        },
        "Summit, CO": {
            "county": "Summit, CO",
            "value": 0
        },
        "Teller, CO": {
            "county": "Teller, CO",
            "value": 0
        },
        "Washington, CO": {
            "county": "Washington, CO",
            "value": 0
        },
        "Weld, CO": {
            "county": "Weld, CO",
            "value": 0
        },
        "Yuma, CO": {
            "county": "Yuma, CO",
            "value": 0
        },
        "Fairfield, CT": {
            "county": "Fairfield, CT",
            "value": 0
        },
        "Litchfield, CT": {
            "county": "Litchfield, CT",
            "value": 0
        },
        "Middlesex, CT": {
            "county": "Middlesex, CT",
            "value": 0
        },
        "New London, CT": {
            "county": "New London, CT",
            "value": 0
        },
        "Tolland, CT": {
            "county": "Tolland, CT",
            "value": 0
        },
        "Kent, DE": {
            "county": "Kent, DE",
            "value": 0
        },
        "New Castle, DE": {
            "county": "New Castle, DE",
            "value": 0
        },
        "Washington, DC": {
            "county": "Washington, DC",
            "value": 0
        },
        "Alachua, FL": {
            "county": "Alachua, FL",
            "value": 0
        },
        "Baker, FL": {
            "county": "Baker, FL",
            "value": 0
        },
        "Bay, FL": {
            "county": "Bay, FL",
            "value": 0
        },
        "Bradford, FL": {
            "county": "Bradford, FL",
            "value": 0
        },
        "Brevard, FL": {
            "county": "Brevard, FL",
            "value": 0
        },
        "Broward, FL": {
            "county": "Broward, FL",
            "value": 0
        },
        "Calhoun, FL": {
            "county": "Calhoun, FL",
            "value": 0
        },
        "Charlotte, FL": {
            "county": "Charlotte, FL",
            "value": 0
        },
        "Citrus, FL": {
            "county": "Citrus, FL",
            "value": 0
        },
        "Clay, FL": {
            "county": "Clay, FL",
            "value": 0
        },
        "Collier, FL": {
            "county": "Collier, FL",
            "value": 0
        },
        "Columbia, FL": {
            "county": "Columbia, FL",
            "value": 0
        },
        "DeSoto, FL": {
            "county": "DeSoto, FL",
            "value": 0
        },
        "Dixie, FL": {
            "county": "Dixie, FL",
            "value": 0
        },
        "Escambia, FL": {
            "county": "Escambia, FL",
            "value": 0
        },
        "Flagler, FL": {
            "county": "Flagler, FL",
            "value": 0
        },
        "Franklin, FL": {
            "county": "Franklin, FL",
            "value": 0
        },
        "Gadsden, FL": {
            "county": "Gadsden, FL",
            "value": 0
        },
        "Gilchrist, FL": {
            "county": "Gilchrist, FL",
            "value": 0
        },
        "Glades, FL": {
            "county": "Glades, FL",
            "value": 0
        },
        "Gulf, FL": {
            "county": "Gulf, FL",
            "value": 0
        },
        "Hamilton, FL": {
            "county": "Hamilton, FL",
            "value": 0
        },
        "Hardee, FL": {
            "county": "Hardee, FL",
            "value": 0
        },
        "Hendry, FL": {
            "county": "Hendry, FL",
            "value": 0
        },
        "Hernando, FL": {
            "county": "Hernando, FL",
            "value": 0
        },
        "Highlands, FL": {
            "county": "Highlands, FL",
            "value": 0
        },
        "Hillsborough, FL": {
            "county": "Hillsborough, FL",
            "value": 0
        },
        "Holmes, FL": {
            "county": "Holmes, FL",
            "value": 0
        },
        "Indian River, FL": {
            "county": "Indian River, FL",
            "value": 0
        },
        "Jefferson, FL": {
            "county": "Jefferson, FL",
            "value": 0
        },
        "Lafayette, FL": {
            "county": "Lafayette, FL",
            "value": 0
        },
        "Lee, FL": {
            "county": "Lee, FL",
            "value": 0
        },
        "Leon, FL": {
            "county": "Leon, FL",
            "value": 0
        },
        "Levy, FL": {
            "county": "Levy, FL",
            "value": 0
        },
        "Madison, FL": {
            "county": "Madison, FL",
            "value": 0
        },
        "Marion, FL": {
            "county": "Marion, FL",
            "value": 0
        },
        "Martin, FL": {
            "county": "Martin, FL",
            "value": 0
        },
        "Monroe, FL": {
            "county": "Monroe, FL",
            "value": 0
        },
        "Nassau, FL": {
            "county": "Nassau, FL",
            "value": 0
        },
        "Okaloosa, FL": {
            "county": "Okaloosa, FL",
            "value": 0
        },
        "Okeechobee, FL": {
            "county": "Okeechobee, FL",
            "value": 0
        },
        "Orange, FL": {
            "county": "Orange, FL",
            "value": 0
        },
        "Osceola, FL": {
            "county": "Osceola, FL",
            "value": 0
        },
        "Palm Beach, FL": {
            "county": "Palm Beach, FL",
            "value": 0
        },
        "Pasco, FL": {
            "county": "Pasco, FL",
            "value": 0
        },
        "Polk, FL": {
            "county": "Polk, FL",
            "value": 0
        },
        "Putnam, FL": {
            "county": "Putnam, FL",
            "value": 0
        },
        "St. Johns, FL": {
            "county": "St. Johns, FL",
            "value": 0
        },
        "St. Lucie, FL": {
            "county": "St. Lucie, FL",
            "value": 0
        },
        "Santa Rosa, FL": {
            "county": "Santa Rosa, FL",
            "value": 0
        },
        "Sarasota, FL": {
            "county": "Sarasota, FL",
            "value": 0
        },
        "Seminole, FL": {
            "county": "Seminole, FL",
            "value": 0
        },
        "Sumter, FL": {
            "county": "Sumter, FL",
            "value": 0
        },
        "Suwannee, FL": {
            "county": "Suwannee, FL",
            "value": 0
        },
        "Taylor, FL": {
            "county": "Taylor, FL",
            "value": 0
        },
        "Union, FL": {
            "county": "Union, FL",
            "value": 0
        },
        "Volusia, FL": {
            "county": "Volusia, FL",
            "value": 0
        },
        "Walton, FL": {
            "county": "Walton, FL",
            "value": 0
        },
        "Washington, FL": {
            "county": "Washington, FL",
            "value": 0
        },
        "Appling, GA": {
            "county": "Appling, GA",
            "value": 0
        },
        "Atkinson, GA": {
            "county": "Atkinson, GA",
            "value": 0
        },
        "Bacon, GA": {
            "county": "Bacon, GA",
            "value": 0
        },
        "Baker, GA": {
            "county": "Baker, GA",
            "value": 0
        },
        "Baldwin, GA": {
            "county": "Baldwin, GA",
            "value": 0
        },
        "Banks, GA": {
            "county": "Banks, GA",
            "value": 0
        },
        "Barrow, GA": {
            "county": "Barrow, GA",
            "value": 0
        },
        "Bartow, GA": {
            "county": "Bartow, GA",
            "value": 0
        },
        "Ben Hill, GA": {
            "county": "Ben Hill, GA",
            "value": 0
        },
        "Berrien, GA": {
            "county": "Berrien, GA",
            "value": 0
        },
        "Bibb, GA": {
            "county": "Bibb, GA",
            "value": 0
        },
        "Bleckley, GA": {
            "county": "Bleckley, GA",
            "value": 0
        },
        "Brantley, GA": {
            "county": "Brantley, GA",
            "value": 0
        },
        "Brooks, GA": {
            "county": "Brooks, GA",
            "value": 0
        },
        "Bryan, GA": {
            "county": "Bryan, GA",
            "value": 0
        },
        "Bulloch, GA": {
            "county": "Bulloch, GA",
            "value": 0
        },
        "Burke, GA": {
            "county": "Burke, GA",
            "value": 0
        },
        "Butts, GA": {
            "county": "Butts, GA",
            "value": 0
        },
        "Calhoun, GA": {
            "county": "Calhoun, GA",
            "value": 0
        },
        "Camden, GA": {
            "county": "Camden, GA",
            "value": 0
        },
        "Candler, GA": {
            "county": "Candler, GA",
            "value": 0
        },
        "Carroll, GA": {
            "county": "Carroll, GA",
            "value": 0
        },
        "Catoosa, GA": {
            "county": "Catoosa, GA",
            "value": 0
        },
        "Charlton, GA": {
            "county": "Charlton, GA",
            "value": 0
        },
        "Chattahoochee, GA": {
            "county": "Chattahoochee, GA",
            "value": 0
        },
        "Chattooga, GA": {
            "county": "Chattooga, GA",
            "value": 0
        },
        "Cherokee, GA": {
            "county": "Cherokee, GA",
            "value": 0
        },
        "Clay, GA": {
            "county": "Clay, GA",
            "value": 0
        },
        "Clayton, GA": {
            "county": "Clayton, GA",
            "value": 0
        },
        "Clinch, GA": {
            "county": "Clinch, GA",
            "value": 0
        },
        "Cobb, GA": {
            "county": "Cobb, GA",
            "value": 0
        },
        "Coffee, GA": {
            "county": "Coffee, GA",
            "value": 0
        },
        "Colquitt, GA": {
            "county": "Colquitt, GA",
            "value": 0
        },
        "Columbia, GA": {
            "county": "Columbia, GA",
            "value": 0
        },
        "Cook, GA": {
            "county": "Cook, GA",
            "value": 0
        },
        "Coweta, GA": {
            "county": "Coweta, GA",
            "value": 0
        },
        "Crawford, GA": {
            "county": "Crawford, GA",
            "value": 0
        },
        "Crisp, GA": {
            "county": "Crisp, GA",
            "value": 0
        },
        "Dade, GA": {
            "county": "Dade, GA",
            "value": 0
        },
        "Dawson, GA": {
            "county": "Dawson, GA",
            "value": 0
        },
        "Decatur, GA": {
            "county": "Decatur, GA",
            "value": 0
        },
        "DeKalb, GA": {
            "county": "DeKalb, GA",
            "value": 0
        },
        "Dodge, GA": {
            "county": "Dodge, GA",
            "value": 0
        },
        "Dooly, GA": {
            "county": "Dooly, GA",
            "value": 0
        },
        "Dougherty, GA": {
            "county": "Dougherty, GA",
            "value": 0
        },
        "Douglas, GA": {
            "county": "Douglas, GA",
            "value": 0
        },
        "Early, GA": {
            "county": "Early, GA",
            "value": 0
        },
        "Echols, GA": {
            "county": "Echols, GA",
            "value": 0
        },
        "Effingham, GA": {
            "county": "Effingham, GA",
            "value": 0
        },
        "Elbert, GA": {
            "county": "Elbert, GA",
            "value": 0
        },
        "Emanuel, GA": {
            "county": "Emanuel, GA",
            "value": 0
        },
        "Evans, GA": {
            "county": "Evans, GA",
            "value": 0
        },
        "Fannin, GA": {
            "county": "Fannin, GA",
            "value": 0
        },
        "Fayette, GA": {
            "county": "Fayette, GA",
            "value": 0
        },
        "Floyd, GA": {
            "county": "Floyd, GA",
            "value": 0
        },
        "Forsyth, GA": {
            "county": "Forsyth, GA",
            "value": 0
        },
        "Franklin, GA": {
            "county": "Franklin, GA",
            "value": 0
        },
        "Fulton, GA": {
            "county": "Fulton, GA",
            "value": 0
        },
        "Gilmer, GA": {
            "county": "Gilmer, GA",
            "value": 0
        },
        "Glascock, GA": {
            "county": "Glascock, GA",
            "value": 0
        },
        "Glynn, GA": {
            "county": "Glynn, GA",
            "value": 0
        },
        "Gordon, GA": {
            "county": "Gordon, GA",
            "value": 0
        },
        "Grady, GA": {
            "county": "Grady, GA",
            "value": 0
        },
        "Greene, GA": {
            "county": "Greene, GA",
            "value": 0
        },
        "Gwinnett, GA": {
            "county": "Gwinnett, GA",
            "value": 0
        },
        "Habersham, GA": {
            "county": "Habersham, GA",
            "value": 0
        },
        "Hancock, GA": {
            "county": "Hancock, GA",
            "value": 0
        },
        "Haralson, GA": {
            "county": "Haralson, GA",
            "value": 0
        },
        "Harris, GA": {
            "county": "Harris, GA",
            "value": 0
        },
        "Hart, GA": {
            "county": "Hart, GA",
            "value": 0
        },
        "Heard, GA": {
            "county": "Heard, GA",
            "value": 0
        },
        "Henry, GA": {
            "county": "Henry, GA",
            "value": 0
        },
        "Houston, GA": {
            "county": "Houston, GA",
            "value": 0
        },
        "Irwin, GA": {
            "county": "Irwin, GA",
            "value": 0
        },
        "Jackson, GA": {
            "county": "Jackson, GA",
            "value": 0
        },
        "Jasper, GA": {
            "county": "Jasper, GA",
            "value": 0
        },
        "Jeff Davis, GA": {
            "county": "Jeff Davis, GA",
            "value": 0
        },
        "Jefferson, GA": {
            "county": "Jefferson, GA",
            "value": 0
        },
        "Jenkins, GA": {
            "county": "Jenkins, GA",
            "value": 0
        },
        "Johnson, GA": {
            "county": "Johnson, GA",
            "value": 0
        },
        "Jones, GA": {
            "county": "Jones, GA",
            "value": 0
        },
        "Lamar, GA": {
            "county": "Lamar, GA",
            "value": 0
        },
        "Lanier, GA": {
            "county": "Lanier, GA",
            "value": 0
        },
        "Laurens, GA": {
            "county": "Laurens, GA",
            "value": 0
        },
        "Lee, GA": {
            "county": "Lee, GA",
            "value": 0
        },
        "Liberty, GA": {
            "county": "Liberty, GA",
            "value": 0
        },
        "Lincoln, GA": {
            "county": "Lincoln, GA",
            "value": 0
        },
        "Long, GA": {
            "county": "Long, GA",
            "value": 0
        },
        "Lowndes, GA": {
            "county": "Lowndes, GA",
            "value": 0
        },
        "McDuffie, GA": {
            "county": "McDuffie, GA",
            "value": 0
        },
        "McIntosh, GA": {
            "county": "McIntosh, GA",
            "value": 0
        },
        "Macon, GA": {
            "county": "Macon, GA",
            "value": 0
        },
        "Madison, GA": {
            "county": "Madison, GA",
            "value": 0
        },
        "Marion, GA": {
            "county": "Marion, GA",
            "value": 0
        },
        "Meriwether, GA": {
            "county": "Meriwether, GA",
            "value": 0
        },
        "Miller, GA": {
            "county": "Miller, GA",
            "value": 0
        },
        "Monroe, GA": {
            "county": "Monroe, GA",
            "value": 0
        },
        "Montgomery, GA": {
            "county": "Montgomery, GA",
            "value": 0
        },
        "Morgan, GA": {
            "county": "Morgan, GA",
            "value": 0
        },
        "Murray, GA": {
            "county": "Murray, GA",
            "value": 0
        },
        "Muscogee, GA": {
            "county": "Muscogee, GA",
            "value": 0
        },
        "Newton, GA": {
            "county": "Newton, GA",
            "value": 0
        },
        "Oconee, GA": {
            "county": "Oconee, GA",
            "value": 0
        },
        "Oglethorpe, GA": {
            "county": "Oglethorpe, GA",
            "value": 0
        },
        "Paulding, GA": {
            "county": "Paulding, GA",
            "value": 0
        },
        "Peach, GA": {
            "county": "Peach, GA",
            "value": 0
        },
        "Pickens, GA": {
            "county": "Pickens, GA",
            "value": 0
        },
        "Pierce, GA": {
            "county": "Pierce, GA",
            "value": 0
        },
        "Pike, GA": {
            "county": "Pike, GA",
            "value": 0
        },
        "Polk, GA": {
            "county": "Polk, GA",
            "value": 0
        },
        "Pulaski, GA": {
            "county": "Pulaski, GA",
            "value": 0
        },
        "Putnam, GA": {
            "county": "Putnam, GA",
            "value": 0
        },
        "Quitman, GA": {
            "county": "Quitman, GA",
            "value": 0
        },
        "Rabun, GA": {
            "county": "Rabun, GA",
            "value": 0
        },
        "Randolph, GA": {
            "county": "Randolph, GA",
            "value": 0
        },
        "Rockdale, GA": {
            "county": "Rockdale, GA",
            "value": 0
        },
        "Schley, GA": {
            "county": "Schley, GA",
            "value": 0
        },
        "Screven, GA": {
            "county": "Screven, GA",
            "value": 0
        },
        "Seminole, GA": {
            "county": "Seminole, GA",
            "value": 0
        },
        "Spalding, GA": {
            "county": "Spalding, GA",
            "value": 0
        },
        "Stephens, GA": {
            "county": "Stephens, GA",
            "value": 0
        },
        "Stewart, GA": {
            "county": "Stewart, GA",
            "value": 0
        },
        "Talbot, GA": {
            "county": "Talbot, GA",
            "value": 0
        },
        "Taliaferro, GA": {
            "county": "Taliaferro, GA",
            "value": 0
        },
        "Tattnall, GA": {
            "county": "Tattnall, GA",
            "value": 0
        },
        "Taylor, GA": {
            "county": "Taylor, GA",
            "value": 0
        },
        "Telfair, GA": {
            "county": "Telfair, GA",
            "value": 0
        },
        "Terrell, GA": {
            "county": "Terrell, GA",
            "value": 0
        },
        "Thomas, GA": {
            "county": "Thomas, GA",
            "value": 0
        },
        "Tift, GA": {
            "county": "Tift, GA",
            "value": 0
        },
        "Toombs, GA": {
            "county": "Toombs, GA",
            "value": 0
        },
        "Towns, GA": {
            "county": "Towns, GA",
            "value": 0
        },
        "Treutlen, GA": {
            "county": "Treutlen, GA",
            "value": 0
        },
        "Troup, GA": {
            "county": "Troup, GA",
            "value": 0
        },
        "Turner, GA": {
            "county": "Turner, GA",
            "value": 0
        },
        "Twiggs, GA": {
            "county": "Twiggs, GA",
            "value": 0
        },
        "Union, GA": {
            "county": "Union, GA",
            "value": 0
        },
        "Upson, GA": {
            "county": "Upson, GA",
            "value": 0
        },
        "Walker, GA": {
            "county": "Walker, GA",
            "value": 0
        },
        "Walton, GA": {
            "county": "Walton, GA",
            "value": 0
        },
        "Ware, GA": {
            "county": "Ware, GA",
            "value": 0
        },
        "Warren, GA": {
            "county": "Warren, GA",
            "value": 0
        },
        "Washington, GA": {
            "county": "Washington, GA",
            "value": 0
        },
        "Wayne, GA": {
            "county": "Wayne, GA",
            "value": 0
        },
        "Webster, GA": {
            "county": "Webster, GA",
            "value": 0
        },
        "Wheeler, GA": {
            "county": "Wheeler, GA",
            "value": 0
        },
        "White, GA": {
            "county": "White, GA",
            "value": 0
        },
        "Whitfield, GA": {
            "county": "Whitfield, GA",
            "value": 0
        },
        "Wilcox, GA": {
            "county": "Wilcox, GA",
            "value": 0
        },
        "Wilkes, GA": {
            "county": "Wilkes, GA",
            "value": 0
        },
        "Worth, GA": {
            "county": "Worth, GA",
            "value": 0
        },
        "Kauai, HI": {
            "county": "Kauai, HI",
            "value": 0
        },
        "Bannock, ID": {
            "county": "Bannock, ID",
            "value": 0
        },
        "Bear Lake, ID": {
            "county": "Bear Lake, ID",
            "value": 0
        },
        "Benewah, ID": {
            "county": "Benewah, ID",
            "value": 0
        },
        "Bonner, ID": {
            "county": "Bonner, ID",
            "value": 0
        },
        "Bonneville, ID": {
            "county": "Bonneville, ID",
            "value": 0
        },
        "Boundary, ID": {
            "county": "Boundary, ID",
            "value": 0
        },
        "Camas, ID": {
            "county": "Camas, ID",
            "value": 0
        },
        "Canyon, ID": {
            "county": "Canyon, ID",
            "value": 0
        },
        "Caribou, ID": {
            "county": "Caribou, ID",
            "value": 0
        },
        "Clark, ID": {
            "county": "Clark, ID",
            "value": 0
        },
        "Franklin, ID": {
            "county": "Franklin, ID",
            "value": 0
        },
        "Fremont, ID": {
            "county": "Fremont, ID",
            "value": 0
        },
        "Gem, ID": {
            "county": "Gem, ID",
            "value": 0
        },
        "Gooding, ID": {
            "county": "Gooding, ID",
            "value": 0
        },
        "Jefferson, ID": {
            "county": "Jefferson, ID",
            "value": 0
        },
        "Jerome, ID": {
            "county": "Jerome, ID",
            "value": 0
        },
        "Kootenai, ID": {
            "county": "Kootenai, ID",
            "value": 0
        },
        "Latah, ID": {
            "county": "Latah, ID",
            "value": 0
        },
        "Lewis, ID": {
            "county": "Lewis, ID",
            "value": 0
        },
        "Lincoln, ID": {
            "county": "Lincoln, ID",
            "value": 0
        },
        "Minidoka, ID": {
            "county": "Minidoka, ID",
            "value": 0
        },
        "Oneida, ID": {
            "county": "Oneida, ID",
            "value": 0
        },
        "Payette, ID": {
            "county": "Payette, ID",
            "value": 0
        },
        "Teton, ID": {
            "county": "Teton, ID",
            "value": 0
        },
        "Twin Falls, ID": {
            "county": "Twin Falls, ID",
            "value": 0
        },
        "Adams, IL": {
            "county": "Adams, IL",
            "value": 0
        },
        "Bond, IL": {
            "county": "Bond, IL",
            "value": 0
        },
        "Boone, IL": {
            "county": "Boone, IL",
            "value": 0
        },
        "Brown, IL": {
            "county": "Brown, IL",
            "value": 0
        },
        "Bureau, IL": {
            "county": "Bureau, IL",
            "value": 0
        },
        "Calhoun, IL": {
            "county": "Calhoun, IL",
            "value": 0
        },
        "Carroll, IL": {
            "county": "Carroll, IL",
            "value": 0
        },
        "Cass, IL": {
            "county": "Cass, IL",
            "value": 0
        },
        "Christian, IL": {
            "county": "Christian, IL",
            "value": 0
        },
        "Clark, IL": {
            "county": "Clark, IL",
            "value": 0
        },
        "Clay, IL": {
            "county": "Clay, IL",
            "value": 0
        },
        "Clinton, IL": {
            "county": "Clinton, IL",
            "value": 0
        },
        "Cumberland, IL": {
            "county": "Cumberland, IL",
            "value": 0
        },
        "DeKalb, IL": {
            "county": "DeKalb, IL",
            "value": 0
        },
        "De Witt, IL": {
            "county": "De Witt, IL",
            "value": 0
        },
        "Douglas, IL": {
            "county": "Douglas, IL",
            "value": 0
        },
        "DuPage, IL": {
            "county": "DuPage, IL",
            "value": 0
        },
        "Edwards, IL": {
            "county": "Edwards, IL",
            "value": 0
        },
        "Effingham, IL": {
            "county": "Effingham, IL",
            "value": 0
        },
        "Ford, IL": {
            "county": "Ford, IL",
            "value": 0
        },
        "Franklin, IL": {
            "county": "Franklin, IL",
            "value": 0
        },
        "Fulton, IL": {
            "county": "Fulton, IL",
            "value": 0
        },
        "Gallatin, IL": {
            "county": "Gallatin, IL",
            "value": 0
        },
        "Greene, IL": {
            "county": "Greene, IL",
            "value": 0
        },
        "Grundy, IL": {
            "county": "Grundy, IL",
            "value": 0
        },
        "Hamilton, IL": {
            "county": "Hamilton, IL",
            "value": 0
        },
        "Henderson, IL": {
            "county": "Henderson, IL",
            "value": 0
        },
        "Iroquois, IL": {
            "county": "Iroquois, IL",
            "value": 0
        },
        "Jasper, IL": {
            "county": "Jasper, IL",
            "value": 0
        },
        "Jefferson, IL": {
            "county": "Jefferson, IL",
            "value": 0
        },
        "Jersey, IL": {
            "county": "Jersey, IL",
            "value": 0
        },
        "Jo Daviess, IL": {
            "county": "Jo Daviess, IL",
            "value": 0
        },
        "Johnson, IL": {
            "county": "Johnson, IL",
            "value": 0
        },
        "Kendall, IL": {
            "county": "Kendall, IL",
            "value": 0
        },
        "Knox, IL": {
            "county": "Knox, IL",
            "value": 0
        },
        "La Salle, IL": {
            "county": "La Salle, IL",
            "value": 0
        },
        "Lee, IL": {
            "county": "Lee, IL",
            "value": 0
        },
        "McDonough, IL": {
            "county": "McDonough, IL",
            "value": 0
        },
        "Macon, IL": {
            "county": "Macon, IL",
            "value": 0
        },
        "Macoupin, IL": {
            "county": "Macoupin, IL",
            "value": 0
        },
        "Madison, IL": {
            "county": "Madison, IL",
            "value": 0
        },
        "Marshall, IL": {
            "county": "Marshall, IL",
            "value": 0
        },
        "Mason, IL": {
            "county": "Mason, IL",
            "value": 0
        },
        "Massac, IL": {
            "county": "Massac, IL",
            "value": 0
        },
        "Menard, IL": {
            "county": "Menard, IL",
            "value": 0
        },
        "Monroe, IL": {
            "county": "Monroe, IL",
            "value": 0
        },
        "Montgomery, IL": {
            "county": "Montgomery, IL",
            "value": 0
        },
        "Morgan, IL": {
            "county": "Morgan, IL",
            "value": 0
        },
        "Moultrie, IL": {
            "county": "Moultrie, IL",
            "value": 0
        },
        "Ogle, IL": {
            "county": "Ogle, IL",
            "value": 0
        },
        "Peoria, IL": {
            "county": "Peoria, IL",
            "value": 0
        },
        "Perry, IL": {
            "county": "Perry, IL",
            "value": 0
        },
        "Piatt, IL": {
            "county": "Piatt, IL",
            "value": 0
        },
        "Pope, IL": {
            "county": "Pope, IL",
            "value": 0
        },
        "Richland, IL": {
            "county": "Richland, IL",
            "value": 0
        },
        "Rock Island, IL": {
            "county": "Rock Island, IL",
            "value": 0
        },
        "Saline, IL": {
            "county": "Saline, IL",
            "value": 0
        },
        "Schuyler, IL": {
            "county": "Schuyler, IL",
            "value": 0
        },
        "Stark, IL": {
            "county": "Stark, IL",
            "value": 0
        },
        "Stephenson, IL": {
            "county": "Stephenson, IL",
            "value": 0
        },
        "Tazewell, IL": {
            "county": "Tazewell, IL",
            "value": 0
        },
        "Union, IL": {
            "county": "Union, IL",
            "value": 0
        },
        "Warren, IL": {
            "county": "Warren, IL",
            "value": 0
        },
        "Washington, IL": {
            "county": "Washington, IL",
            "value": 0
        },
        "White, IL": {
            "county": "White, IL",
            "value": 0
        },
        "Whiteside, IL": {
            "county": "Whiteside, IL",
            "value": 0
        },
        "Williamson, IL": {
            "county": "Williamson, IL",
            "value": 0
        },
        "Winnebago, IL": {
            "county": "Winnebago, IL",
            "value": 0
        },
        "Woodford, IL": {
            "county": "Woodford, IL",
            "value": 0
        },
        "Adams, IN": {
            "county": "Adams, IN",
            "value": 0
        },
        "Allen, IN": {
            "county": "Allen, IN",
            "value": 0
        },
        "Bartholomew, IN": {
            "county": "Bartholomew, IN",
            "value": 0
        },
        "Benton, IN": {
            "county": "Benton, IN",
            "value": 0
        },
        "Blackford, IN": {
            "county": "Blackford, IN",
            "value": 0
        },
        "Brown, IN": {
            "county": "Brown, IN",
            "value": 0
        },
        "Carroll, IN": {
            "county": "Carroll, IN",
            "value": 0
        },
        "Cass, IN": {
            "county": "Cass, IN",
            "value": 0
        },
        "Clark, IN": {
            "county": "Clark, IN",
            "value": 0
        },
        "Clay, IN": {
            "county": "Clay, IN",
            "value": 0
        },
        "Clinton, IN": {
            "county": "Clinton, IN",
            "value": 0
        },
        "Crawford, IN": {
            "county": "Crawford, IN",
            "value": 0
        },
        "Daviess, IN": {
            "county": "Daviess, IN",
            "value": 0
        },
        "Dearborn, IN": {
            "county": "Dearborn, IN",
            "value": 0
        },
        "Decatur, IN": {
            "county": "Decatur, IN",
            "value": 0
        },
        "DeKalb, IN": {
            "county": "DeKalb, IN",
            "value": 0
        },
        "Delaware, IN": {
            "county": "Delaware, IN",
            "value": 0
        },
        "Dubois, IN": {
            "county": "Dubois, IN",
            "value": 0
        },
        "Elkhart, IN": {
            "county": "Elkhart, IN",
            "value": 0
        },
        "Fayette, IN": {
            "county": "Fayette, IN",
            "value": 0
        },
        "Floyd, IN": {
            "county": "Floyd, IN",
            "value": 0
        },
        "Fountain, IN": {
            "county": "Fountain, IN",
            "value": 0
        },
        "Franklin, IN": {
            "county": "Franklin, IN",
            "value": 0
        },
        "Fulton, IN": {
            "county": "Fulton, IN",
            "value": 0
        },
        "Gibson, IN": {
            "county": "Gibson, IN",
            "value": 0
        },
        "Grant, IN": {
            "county": "Grant, IN",
            "value": 0
        },
        "Greene, IN": {
            "county": "Greene, IN",
            "value": 0
        },
        "Hamilton, IN": {
            "county": "Hamilton, IN",
            "value": 0
        },
        "Harrison, IN": {
            "county": "Harrison, IN",
            "value": 0
        },
        "Hendricks, IN": {
            "county": "Hendricks, IN",
            "value": 0
        },
        "Henry, IN": {
            "county": "Henry, IN",
            "value": 0
        },
        "Huntington, IN": {
            "county": "Huntington, IN",
            "value": 0
        },
        "Jackson, IN": {
            "county": "Jackson, IN",
            "value": 0
        },
        "Jasper, IN": {
            "county": "Jasper, IN",
            "value": 0
        },
        "Jay, IN": {
            "county": "Jay, IN",
            "value": 0
        },
        "Jefferson, IN": {
            "county": "Jefferson, IN",
            "value": 0
        },
        "Jennings, IN": {
            "county": "Jennings, IN",
            "value": 0
        },
        "Knox, IN": {
            "county": "Knox, IN",
            "value": 0
        },
        "Kosciusko, IN": {
            "county": "Kosciusko, IN",
            "value": 0
        },
        "LaGrange, IN": {
            "county": "LaGrange, IN",
            "value": 0
        },
        "Lake, IN": {
            "county": "Lake, IN",
            "value": 0
        },
        "LaPorte, IN": {
            "county": "LaPorte, IN",
            "value": 0
        },
        "Marshall, IN": {
            "county": "Marshall, IN",
            "value": 0
        },
        "Martin, IN": {
            "county": "Martin, IN",
            "value": 0
        },
        "Miami, IN": {
            "county": "Miami, IN",
            "value": 0
        },
        "Monroe, IN": {
            "county": "Monroe, IN",
            "value": 0
        },
        "Morgan, IN": {
            "county": "Morgan, IN",
            "value": 0
        },
        "Newton, IN": {
            "county": "Newton, IN",
            "value": 0
        },
        "Noble, IN": {
            "county": "Noble, IN",
            "value": 0
        },
        "Ohio, IN": {
            "county": "Ohio, IN",
            "value": 0
        },
        "Owen, IN": {
            "county": "Owen, IN",
            "value": 0
        },
        "Parke, IN": {
            "county": "Parke, IN",
            "value": 0
        },
        "Perry, IN": {
            "county": "Perry, IN",
            "value": 0
        },
        "Pike, IN": {
            "county": "Pike, IN",
            "value": 0
        },
        "Porter, IN": {
            "county": "Porter, IN",
            "value": 0
        },
        "Posey, IN": {
            "county": "Posey, IN",
            "value": 0
        },
        "Putnam, IN": {
            "county": "Putnam, IN",
            "value": 0
        },
        "Ripley, IN": {
            "county": "Ripley, IN",
            "value": 0
        },
        "Rush, IN": {
            "county": "Rush, IN",
            "value": 0
        },
        "St. Joseph, IN": {
            "county": "St. Joseph, IN",
            "value": 0
        },
        "Scott, IN": {
            "county": "Scott, IN",
            "value": 0
        },
        "Starke, IN": {
            "county": "Starke, IN",
            "value": 0
        },
        "Sullivan, IN": {
            "county": "Sullivan, IN",
            "value": 0
        },
        "Switzerland, IN": {
            "county": "Switzerland, IN",
            "value": 0
        },
        "Tipton, IN": {
            "county": "Tipton, IN",
            "value": 0
        },
        "Union, IN": {
            "county": "Union, IN",
            "value": 0
        },
        "Vanderburgh, IN": {
            "county": "Vanderburgh, IN",
            "value": 0
        },
        "Vermillion, IN": {
            "county": "Vermillion, IN",
            "value": 0
        },
        "Vigo, IN": {
            "county": "Vigo, IN",
            "value": 0
        },
        "Wabash, IN": {
            "county": "Wabash, IN",
            "value": 0
        },
        "Warren, IN": {
            "county": "Warren, IN",
            "value": 0
        },
        "Warrick, IN": {
            "county": "Warrick, IN",
            "value": 0
        },
        "Wayne, IN": {
            "county": "Wayne, IN",
            "value": 0
        },
        "Wells, IN": {
            "county": "Wells, IN",
            "value": 0
        },
        "White, IN": {
            "county": "White, IN",
            "value": 0
        },
        "Whitley, IN": {
            "county": "Whitley, IN",
            "value": 0
        },
        "Adair, IA": {
            "county": "Adair, IA",
            "value": 0
        },
        "Adams, IA": {
            "county": "Adams, IA",
            "value": 0
        },
        "Allamakee, IA": {
            "county": "Allamakee, IA",
            "value": 0
        },
        "Appanoose, IA": {
            "county": "Appanoose, IA",
            "value": 0
        },
        "Audubon, IA": {
            "county": "Audubon, IA",
            "value": 0
        },
        "Black Hawk, IA": {
            "county": "Black Hawk, IA",
            "value": 0
        },
        "Boone, IA": {
            "county": "Boone, IA",
            "value": 0
        },
        "Buena Vista, IA": {
            "county": "Buena Vista, IA",
            "value": 0
        },
        "Butler, IA": {
            "county": "Butler, IA",
            "value": 0
        },
        "Calhoun, IA": {
            "county": "Calhoun, IA",
            "value": 0
        },
        "Carroll, IA": {
            "county": "Carroll, IA",
            "value": 0
        },
        "Cedar, IA": {
            "county": "Cedar, IA",
            "value": 0
        },
        "Cerro Gordo, IA": {
            "county": "Cerro Gordo, IA",
            "value": 0
        },
        "Chickasaw, IA": {
            "county": "Chickasaw, IA",
            "value": 0
        },
        "Clarke, IA": {
            "county": "Clarke, IA",
            "value": 0
        },
        "Clayton, IA": {
            "county": "Clayton, IA",
            "value": 0
        },
        "Clinton, IA": {
            "county": "Clinton, IA",
            "value": 0
        },
        "Crawford, IA": {
            "county": "Crawford, IA",
            "value": 0
        },
        "Dallas, IA": {
            "county": "Dallas, IA",
            "value": 0
        },
        "Davis, IA": {
            "county": "Davis, IA",
            "value": 0
        },
        "Decatur, IA": {
            "county": "Decatur, IA",
            "value": 0
        },
        "Delaware, IA": {
            "county": "Delaware, IA",
            "value": 0
        },
        "Des Moines, IA": {
            "county": "Des Moines, IA",
            "value": 0
        },
        "Dickinson, IA": {
            "county": "Dickinson, IA",
            "value": 0
        },
        "Emmet, IA": {
            "county": "Emmet, IA",
            "value": 0
        },
        "Fayette, IA": {
            "county": "Fayette, IA",
            "value": 0
        },
        "Floyd, IA": {
            "county": "Floyd, IA",
            "value": 0
        },
        "Franklin, IA": {
            "county": "Franklin, IA",
            "value": 0
        },
        "Fremont, IA": {
            "county": "Fremont, IA",
            "value": 0
        },
        "Greene, IA": {
            "county": "Greene, IA",
            "value": 0
        },
        "Guthrie, IA": {
            "county": "Guthrie, IA",
            "value": 0
        },
        "Hamilton, IA": {
            "county": "Hamilton, IA",
            "value": 0
        },
        "Hancock, IA": {
            "county": "Hancock, IA",
            "value": 0
        },
        "Henry, IA": {
            "county": "Henry, IA",
            "value": 0
        },
        "Howard, IA": {
            "county": "Howard, IA",
            "value": 0
        },
        "Humboldt, IA": {
            "county": "Humboldt, IA",
            "value": 0
        },
        "Ida, IA": {
            "county": "Ida, IA",
            "value": 0
        },
        "Iowa, IA": {
            "county": "Iowa, IA",
            "value": 0
        },
        "Jackson, IA": {
            "county": "Jackson, IA",
            "value": 0
        },
        "Jasper, IA": {
            "county": "Jasper, IA",
            "value": 0
        },
        "Jefferson, IA": {
            "county": "Jefferson, IA",
            "value": 0
        },
        "Keokuk, IA": {
            "county": "Keokuk, IA",
            "value": 0
        },
        "Kossuth, IA": {
            "county": "Kossuth, IA",
            "value": 0
        },
        "Lee, IA": {
            "county": "Lee, IA",
            "value": 0
        },
        "Lucas, IA": {
            "county": "Lucas, IA",
            "value": 0
        },
        "Madison, IA": {
            "county": "Madison, IA",
            "value": 0
        },
        "Mahaska, IA": {
            "county": "Mahaska, IA",
            "value": 0
        },
        "Marion, IA": {
            "county": "Marion, IA",
            "value": 0
        },
        "Mills, IA": {
            "county": "Mills, IA",
            "value": 0
        },
        "Monona, IA": {
            "county": "Monona, IA",
            "value": 0
        },
        "Monroe, IA": {
            "county": "Monroe, IA",
            "value": 0
        },
        "Montgomery, IA": {
            "county": "Montgomery, IA",
            "value": 0
        },
        "Muscatine, IA": {
            "county": "Muscatine, IA",
            "value": 0
        },
        "Osceola, IA": {
            "county": "Osceola, IA",
            "value": 0
        },
        "Page, IA": {
            "county": "Page, IA",
            "value": 0
        },
        "Palo Alto, IA": {
            "county": "Palo Alto, IA",
            "value": 0
        },
        "Plymouth, IA": {
            "county": "Plymouth, IA",
            "value": 0
        },
        "Pocahontas, IA": {
            "county": "Pocahontas, IA",
            "value": 0
        },
        "Polk, IA": {
            "county": "Polk, IA",
            "value": 0
        },
        "Pottawattamie, IA": {
            "county": "Pottawattamie, IA",
            "value": 0
        },
        "Poweshiek, IA": {
            "county": "Poweshiek, IA",
            "value": 0
        },
        "Ringgold, IA": {
            "county": "Ringgold, IA",
            "value": 0
        },
        "Sac, IA": {
            "county": "Sac, IA",
            "value": 0
        },
        "Shelby, IA": {
            "county": "Shelby, IA",
            "value": 0
        },
        "Tama, IA": {
            "county": "Tama, IA",
            "value": 0
        },
        "Union, IA": {
            "county": "Union, IA",
            "value": 0
        },
        "Van Buren, IA": {
            "county": "Van Buren, IA",
            "value": 0
        },
        "Wapello, IA": {
            "county": "Wapello, IA",
            "value": 0
        },
        "Washington, IA": {
            "county": "Washington, IA",
            "value": 0
        },
        "Webster, IA": {
            "county": "Webster, IA",
            "value": 0
        },
        "Winnebago, IA": {
            "county": "Winnebago, IA",
            "value": 0
        },
        "Winneshiek, IA": {
            "county": "Winneshiek, IA",
            "value": 0
        },
        "Woodbury, IA": {
            "county": "Woodbury, IA",
            "value": 0
        },
        "Worth, IA": {
            "county": "Worth, IA",
            "value": 0
        },
        "Wright, IA": {
            "county": "Wright, IA",
            "value": 0
        },
        "Anderson, KS": {
            "county": "Anderson, KS",
            "value": 0
        },
        "Atchison, KS": {
            "county": "Atchison, KS",
            "value": 0
        },
        "Barton, KS": {
            "county": "Barton, KS",
            "value": 0
        },
        "Bourbon, KS": {
            "county": "Bourbon, KS",
            "value": 0
        },
        "Brown, KS": {
            "county": "Brown, KS",
            "value": 0
        },
        "Butler, KS": {
            "county": "Butler, KS",
            "value": 0
        },
        "Chase, KS": {
            "county": "Chase, KS",
            "value": 0
        },
        "Cherokee, KS": {
            "county": "Cherokee, KS",
            "value": 0
        },
        "Cheyenne, KS": {
            "county": "Cheyenne, KS",
            "value": 0
        },
        "Clark, KS": {
            "county": "Clark, KS",
            "value": 0
        },
        "Clay, KS": {
            "county": "Clay, KS",
            "value": 0
        },
        "Coffey, KS": {
            "county": "Coffey, KS",
            "value": 0
        },
        "Comanche, KS": {
            "county": "Comanche, KS",
            "value": 0
        },
        "Cowley, KS": {
            "county": "Cowley, KS",
            "value": 0
        },
        "Crawford, KS": {
            "county": "Crawford, KS",
            "value": 0
        },
        "Decatur, KS": {
            "county": "Decatur, KS",
            "value": 0
        },
        "Dickinson, KS": {
            "county": "Dickinson, KS",
            "value": 0
        },
        "Doniphan, KS": {
            "county": "Doniphan, KS",
            "value": 0
        },
        "Edwards, KS": {
            "county": "Edwards, KS",
            "value": 0
        },
        "Ellis, KS": {
            "county": "Ellis, KS",
            "value": 0
        },
        "Ellsworth, KS": {
            "county": "Ellsworth, KS",
            "value": 0
        },
        "Franklin, KS": {
            "county": "Franklin, KS",
            "value": 0
        },
        "Geary, KS": {
            "county": "Geary, KS",
            "value": 0
        },
        "Grant, KS": {
            "county": "Grant, KS",
            "value": 0
        },
        "Gray, KS": {
            "county": "Gray, KS",
            "value": 0
        },
        "Greenwood, KS": {
            "county": "Greenwood, KS",
            "value": 0
        },
        "Hamilton, KS": {
            "county": "Hamilton, KS",
            "value": 0
        },
        "Harper, KS": {
            "county": "Harper, KS",
            "value": 0
        },
        "Harvey, KS": {
            "county": "Harvey, KS",
            "value": 0
        },
        "Haskell, KS": {
            "county": "Haskell, KS",
            "value": 0
        },
        "Hodgeman, KS": {
            "county": "Hodgeman, KS",
            "value": 0
        },
        "Jewell, KS": {
            "county": "Jewell, KS",
            "value": 0
        },
        "Johnson, KS": {
            "county": "Johnson, KS",
            "value": 0
        },
        "Kearny, KS": {
            "county": "Kearny, KS",
            "value": 0
        },
        "Kingman, KS": {
            "county": "Kingman, KS",
            "value": 0
        },
        "Kiowa, KS": {
            "county": "Kiowa, KS",
            "value": 0
        },
        "Labette, KS": {
            "county": "Labette, KS",
            "value": 0
        },
        "Lane, KS": {
            "county": "Lane, KS",
            "value": 0
        },
        "Leavenworth, KS": {
            "county": "Leavenworth, KS",
            "value": 0
        },
        "Lincoln, KS": {
            "county": "Lincoln, KS",
            "value": 0
        },
        "Linn, KS": {
            "county": "Linn, KS",
            "value": 0
        },
        "Lyon, KS": {
            "county": "Lyon, KS",
            "value": 0
        },
        "McPherson, KS": {
            "county": "McPherson, KS",
            "value": 0
        },
        "Marion, KS": {
            "county": "Marion, KS",
            "value": 0
        },
        "Meade, KS": {
            "county": "Meade, KS",
            "value": 0
        },
        "Miami, KS": {
            "county": "Miami, KS",
            "value": 0
        },
        "Mitchell, KS": {
            "county": "Mitchell, KS",
            "value": 0
        },
        "Morris, KS": {
            "county": "Morris, KS",
            "value": 0
        },
        "Morton, KS": {
            "county": "Morton, KS",
            "value": 0
        },
        "Ness, KS": {
            "county": "Ness, KS",
            "value": 0
        },
        "Osage, KS": {
            "county": "Osage, KS",
            "value": 0
        },
        "Osborne, KS": {
            "county": "Osborne, KS",
            "value": 0
        },
        "Ottawa, KS": {
            "county": "Ottawa, KS",
            "value": 0
        },
        "Pawnee, KS": {
            "county": "Pawnee, KS",
            "value": 0
        },
        "Pottawatomie, KS": {
            "county": "Pottawatomie, KS",
            "value": 0
        },
        "Pratt, KS": {
            "county": "Pratt, KS",
            "value": 0
        },
        "Rawlins, KS": {
            "county": "Rawlins, KS",
            "value": 0
        },
        "Rice, KS": {
            "county": "Rice, KS",
            "value": 0
        },
        "Saline, KS": {
            "county": "Saline, KS",
            "value": 0
        },
        "Sedgwick, KS": {
            "county": "Sedgwick, KS",
            "value": 0
        },
        "Seward, KS": {
            "county": "Seward, KS",
            "value": 0
        },
        "Smith, KS": {
            "county": "Smith, KS",
            "value": 0
        },
        "Stafford, KS": {
            "county": "Stafford, KS",
            "value": 0
        },
        "Stanton, KS": {
            "county": "Stanton, KS",
            "value": 0
        },
        "Stevens, KS": {
            "county": "Stevens, KS",
            "value": 0
        },
        "Sumner, KS": {
            "county": "Sumner, KS",
            "value": 0
        },
        "Thomas, KS": {
            "county": "Thomas, KS",
            "value": 0
        },
        "Trego, KS": {
            "county": "Trego, KS",
            "value": 0
        },
        "Wabaunsee, KS": {
            "county": "Wabaunsee, KS",
            "value": 0
        },
        "Wallace, KS": {
            "county": "Wallace, KS",
            "value": 0
        },
        "Wichita, KS": {
            "county": "Wichita, KS",
            "value": 0
        },
        "Wilson, KS": {
            "county": "Wilson, KS",
            "value": 0
        },
        "Woodson, KS": {
            "county": "Woodson, KS",
            "value": 0
        },
        "Wyandotte, KS": {
            "county": "Wyandotte, KS",
            "value": 0
        },
        "Adair, KY": {
            "county": "Adair, KY",
            "value": 0
        },
        "Allen, KY": {
            "county": "Allen, KY",
            "value": 0
        },
        "Anderson, KY": {
            "county": "Anderson, KY",
            "value": 0
        },
        "Barren, KY": {
            "county": "Barren, KY",
            "value": 0
        },
        "Bath, KY": {
            "county": "Bath, KY",
            "value": 0
        },
        "Bell, KY": {
            "county": "Bell, KY",
            "value": 0
        },
        "Boone, KY": {
            "county": "Boone, KY",
            "value": 0
        },
        "Bourbon, KY": {
            "county": "Bourbon, KY",
            "value": 0
        },
        "Boyle, KY": {
            "county": "Boyle, KY",
            "value": 0
        },
        "Bracken, KY": {
            "county": "Bracken, KY",
            "value": 0
        },
        "Breathitt, KY": {
            "county": "Breathitt, KY",
            "value": 0
        },
        "Breckinridge, KY": {
            "county": "Breckinridge, KY",
            "value": 0
        },
        "Bullitt, KY": {
            "county": "Bullitt, KY",
            "value": 0
        },
        "Butler, KY": {
            "county": "Butler, KY",
            "value": 0
        },
        "Caldwell, KY": {
            "county": "Caldwell, KY",
            "value": 0
        },
        "Calloway, KY": {
            "county": "Calloway, KY",
            "value": 0
        },
        "Campbell, KY": {
            "county": "Campbell, KY",
            "value": 0
        },
        "Carlisle, KY": {
            "county": "Carlisle, KY",
            "value": 0
        },
        "Carroll, KY": {
            "county": "Carroll, KY",
            "value": 0
        },
        "Carter, KY": {
            "county": "Carter, KY",
            "value": 0
        },
        "Casey, KY": {
            "county": "Casey, KY",
            "value": 0
        },
        "Christian, KY": {
            "county": "Christian, KY",
            "value": 0
        },
        "Clark, KY": {
            "county": "Clark, KY",
            "value": 0
        },
        "Clinton, KY": {
            "county": "Clinton, KY",
            "value": 0
        },
        "Crittenden, KY": {
            "county": "Crittenden, KY",
            "value": 0
        },
        "Cumberland, KY": {
            "county": "Cumberland, KY",
            "value": 0
        },
        "Daviess, KY": {
            "county": "Daviess, KY",
            "value": 0
        },
        "Elliott, KY": {
            "county": "Elliott, KY",
            "value": 0
        },
        "Estill, KY": {
            "county": "Estill, KY",
            "value": 0
        },
        "Fleming, KY": {
            "county": "Fleming, KY",
            "value": 0
        },
        "Floyd, KY": {
            "county": "Floyd, KY",
            "value": 0
        },
        "Franklin, KY": {
            "county": "Franklin, KY",
            "value": 0
        },
        "Fulton, KY": {
            "county": "Fulton, KY",
            "value": 0
        },
        "Gallatin, KY": {
            "county": "Gallatin, KY",
            "value": 0
        },
        "Garrard, KY": {
            "county": "Garrard, KY",
            "value": 0
        },
        "Grant, KY": {
            "county": "Grant, KY",
            "value": 0
        },
        "Graves, KY": {
            "county": "Graves, KY",
            "value": 0
        },
        "Green, KY": {
            "county": "Green, KY",
            "value": 0
        },
        "Hancock, KY": {
            "county": "Hancock, KY",
            "value": 0
        },
        "Hardin, KY": {
            "county": "Hardin, KY",
            "value": 0
        },
        "Harrison, KY": {
            "county": "Harrison, KY",
            "value": 0
        },
        "Hart, KY": {
            "county": "Hart, KY",
            "value": 0
        },
        "Henry, KY": {
            "county": "Henry, KY",
            "value": 0
        },
        "Hickman, KY": {
            "county": "Hickman, KY",
            "value": 0
        },
        "Jackson, KY": {
            "county": "Jackson, KY",
            "value": 0
        },
        "Jessamine, KY": {
            "county": "Jessamine, KY",
            "value": 0
        },
        "Johnson, KY": {
            "county": "Johnson, KY",
            "value": 0
        },
        "Kenton, KY": {
            "county": "Kenton, KY",
            "value": 0
        },
        "Knott, KY": {
            "county": "Knott, KY",
            "value": 0
        },
        "Knox, KY": {
            "county": "Knox, KY",
            "value": 0
        },
        "Larue, KY": {
            "county": "Larue, KY",
            "value": 0
        },
        "Laurel, KY": {
            "county": "Laurel, KY",
            "value": 0
        },
        "Lawrence, KY": {
            "county": "Lawrence, KY",
            "value": 0
        },
        "Lee, KY": {
            "county": "Lee, KY",
            "value": 0
        },
        "Leslie, KY": {
            "county": "Leslie, KY",
            "value": 0
        },
        "Letcher, KY": {
            "county": "Letcher, KY",
            "value": 0
        },
        "Lewis, KY": {
            "county": "Lewis, KY",
            "value": 0
        },
        "Lincoln, KY": {
            "county": "Lincoln, KY",
            "value": 0
        },
        "Livingston, KY": {
            "county": "Livingston, KY",
            "value": 0
        },
        "McCreary, KY": {
            "county": "McCreary, KY",
            "value": 0
        },
        "McLean, KY": {
            "county": "McLean, KY",
            "value": 0
        },
        "Madison, KY": {
            "county": "Madison, KY",
            "value": 0
        },
        "Magoffin, KY": {
            "county": "Magoffin, KY",
            "value": 0
        },
        "Marion, KY": {
            "county": "Marion, KY",
            "value": 0
        },
        "Marshall, KY": {
            "county": "Marshall, KY",
            "value": 0
        },
        "Martin, KY": {
            "county": "Martin, KY",
            "value": 0
        },
        "Mason, KY": {
            "county": "Mason, KY",
            "value": 0
        },
        "Meade, KY": {
            "county": "Meade, KY",
            "value": 0
        },
        "Menifee, KY": {
            "county": "Menifee, KY",
            "value": 0
        },
        "Mercer, KY": {
            "county": "Mercer, KY",
            "value": 0
        },
        "Metcalfe, KY": {
            "county": "Metcalfe, KY",
            "value": 0
        },
        "Monroe, KY": {
            "county": "Monroe, KY",
            "value": 0
        },
        "Montgomery, KY": {
            "county": "Montgomery, KY",
            "value": 0
        },
        "Morgan, KY": {
            "county": "Morgan, KY",
            "value": 0
        },
        "Muhlenberg, KY": {
            "county": "Muhlenberg, KY",
            "value": 0
        },
        "Nelson, KY": {
            "county": "Nelson, KY",
            "value": 0
        },
        "Nicholas, KY": {
            "county": "Nicholas, KY",
            "value": 0
        },
        "Oldham, KY": {
            "county": "Oldham, KY",
            "value": 0
        },
        "Owen, KY": {
            "county": "Owen, KY",
            "value": 0
        },
        "Pendleton, KY": {
            "county": "Pendleton, KY",
            "value": 0
        },
        "Perry, KY": {
            "county": "Perry, KY",
            "value": 0
        },
        "Pike, KY": {
            "county": "Pike, KY",
            "value": 0
        },
        "Powell, KY": {
            "county": "Powell, KY",
            "value": 0
        },
        "Robertson, KY": {
            "county": "Robertson, KY",
            "value": 0
        },
        "Rowan, KY": {
            "county": "Rowan, KY",
            "value": 0
        },
        "Russell, KY": {
            "county": "Russell, KY",
            "value": 0
        },
        "Scott, KY": {
            "county": "Scott, KY",
            "value": 0
        },
        "Shelby, KY": {
            "county": "Shelby, KY",
            "value": 0
        },
        "Simpson, KY": {
            "county": "Simpson, KY",
            "value": 0
        },
        "Spencer, KY": {
            "county": "Spencer, KY",
            "value": 0
        },
        "Taylor, KY": {
            "county": "Taylor, KY",
            "value": 0
        },
        "Todd, KY": {
            "county": "Todd, KY",
            "value": 0
        },
        "Trigg, KY": {
            "county": "Trigg, KY",
            "value": 0
        },
        "Trimble, KY": {
            "county": "Trimble, KY",
            "value": 0
        },
        "Union, KY": {
            "county": "Union, KY",
            "value": 0
        },
        "Washington, KY": {
            "county": "Washington, KY",
            "value": 0
        },
        "Wayne, KY": {
            "county": "Wayne, KY",
            "value": 0
        },
        "Webster, KY": {
            "county": "Webster, KY",
            "value": 0
        },
        "Whitley, KY": {
            "county": "Whitley, KY",
            "value": 0
        },
        "Woodford, KY": {
            "county": "Woodford, KY",
            "value": 0
        },
        "Acadia, LA": {
            "county": "Acadia, LA",
            "value": 0
        },
        "Allen, LA": {
            "county": "Allen, LA",
            "value": 0
        },
        "Ascension, LA": {
            "county": "Ascension, LA",
            "value": 0
        },
        "Assumption, LA": {
            "county": "Assumption, LA",
            "value": 0
        },
        "Avoyelles, LA": {
            "county": "Avoyelles, LA",
            "value": 0
        },
        "Beauregard, LA": {
            "county": "Beauregard, LA",
            "value": 0
        },
        "Bossier, LA": {
            "county": "Bossier, LA",
            "value": 0
        },
        "Caddo, LA": {
            "county": "Caddo, LA",
            "value": 0
        },
        "Calcasieu, LA": {
            "county": "Calcasieu, LA",
            "value": 0
        },
        "Caldwell, LA": {
            "county": "Caldwell, LA",
            "value": 0
        },
        "Cameron, LA": {
            "county": "Cameron, LA",
            "value": 0
        },
        "Catahoula, LA": {
            "county": "Catahoula, LA",
            "value": 0
        },
        "Claiborne, LA": {
            "county": "Claiborne, LA",
            "value": 0
        },
        "Concordia, LA": {
            "county": "Concordia, LA",
            "value": 0
        },
        "De Soto, LA": {
            "county": "De Soto, LA",
            "value": 0
        },
        "East Baton Rouge, LA": {
            "county": "East Baton Rouge, LA",
            "value": 0
        },
        "East Carroll, LA": {
            "county": "East Carroll, LA",
            "value": 0
        },
        "East Feliciana, LA": {
            "county": "East Feliciana, LA",
            "value": 0
        },
        "Evangeline, LA": {
            "county": "Evangeline, LA",
            "value": 0
        },
        "Franklin, LA": {
            "county": "Franklin, LA",
            "value": 0
        },
        "Grant, LA": {
            "county": "Grant, LA",
            "value": 0
        },
        "Iberia, LA": {
            "county": "Iberia, LA",
            "value": 0
        },
        "Iberville, LA": {
            "county": "Iberville, LA",
            "value": 0
        },
        "Jefferson, LA": {
            "county": "Jefferson, LA",
            "value": 0
        },
        "Jefferson Davis, LA": {
            "county": "Jefferson Davis, LA",
            "value": 0
        },
        "Lafayette, LA": {
            "county": "Lafayette, LA",
            "value": 0
        },
        "Lincoln, LA": {
            "county": "Lincoln, LA",
            "value": 0
        },
        "Livingston, LA": {
            "county": "Livingston, LA",
            "value": 0
        },
        "Morehouse, LA": {
            "county": "Morehouse, LA",
            "value": 0
        },
        "Natchitoches, LA": {
            "county": "Natchitoches, LA",
            "value": 0
        },
        "Orleans, LA": {
            "county": "Orleans, LA",
            "value": 0
        },
        "Ouachita, LA": {
            "county": "Ouachita, LA",
            "value": 0
        },
        "Plaquemines, LA": {
            "county": "Plaquemines, LA",
            "value": 0
        },
        "Rapides, LA": {
            "county": "Rapides, LA",
            "value": 0
        },
        "Red River, LA": {
            "county": "Red River, LA",
            "value": 0
        },
        "Richland, LA": {
            "county": "Richland, LA",
            "value": 0
        },
        "Sabine, LA": {
            "county": "Sabine, LA",
            "value": 0
        },
        "St. Bernard, LA": {
            "county": "St. Bernard, LA",
            "value": 0
        },
        "St. Charles, LA": {
            "county": "St. Charles, LA",
            "value": 0
        },
        "St. Helena, LA": {
            "county": "St. Helena, LA",
            "value": 0
        },
        "St. James, LA": {
            "county": "St. James, LA",
            "value": 0
        },
        "St. Landry, LA": {
            "county": "St. Landry, LA",
            "value": 0
        },
        "St. Martin, LA": {
            "county": "St. Martin, LA",
            "value": 0
        },
        "Tangipahoa, LA": {
            "county": "Tangipahoa, LA",
            "value": 0
        },
        "Tensas, LA": {
            "county": "Tensas, LA",
            "value": 0
        },
        "Terrebonne, LA": {
            "county": "Terrebonne, LA",
            "value": 0
        },
        "Vermilion, LA": {
            "county": "Vermilion, LA",
            "value": 0
        },
        "Washington, LA": {
            "county": "Washington, LA",
            "value": 0
        },
        "Webster, LA": {
            "county": "Webster, LA",
            "value": 0
        },
        "West Baton Rouge, LA": {
            "county": "West Baton Rouge, LA",
            "value": 0
        },
        "West Carroll, LA": {
            "county": "West Carroll, LA",
            "value": 0
        },
        "West Feliciana, LA": {
            "county": "West Feliciana, LA",
            "value": 0
        },
        "Winn, LA": {
            "county": "Winn, LA",
            "value": 0
        },
        "Androscoggin, ME": {
            "county": "Androscoggin, ME",
            "value": 0
        },
        "Cumberland, ME": {
            "county": "Cumberland, ME",
            "value": 0
        },
        "Hancock, ME": {
            "county": "Hancock, ME",
            "value": 0
        },
        "Kennebec, ME": {
            "county": "Kennebec, ME",
            "value": 0
        },
        "Knox, ME": {
            "county": "Knox, ME",
            "value": 0
        },
        "Lincoln, ME": {
            "county": "Lincoln, ME",
            "value": 0
        },
        "Oxford, ME": {
            "county": "Oxford, ME",
            "value": 0
        },
        "Penobscot, ME": {
            "county": "Penobscot, ME",
            "value": 0
        },
        "Sagadahoc, ME": {
            "county": "Sagadahoc, ME",
            "value": 0
        },
        "Waldo, ME": {
            "county": "Waldo, ME",
            "value": 0
        },
        "York, ME": {
            "county": "York, ME",
            "value": 0
        },
        "Allegany, MD": {
            "county": "Allegany, MD",
            "value": 0
        },
        "Anne Arundel, MD": {
            "county": "Anne Arundel, MD",
            "value": 0
        },
        "Baltimore County, MD": {
            "county": "Baltimore County, MD",
            "value": 0
        },
        "Caroline, MD": {
            "county": "Caroline, MD",
            "value": 0
        },
        "Cecil, MD": {
            "county": "Cecil, MD",
            "value": 0
        },
        "Charles, MD": {
            "county": "Charles, MD",
            "value": 0
        },
        "Dorchester, MD": {
            "county": "Dorchester, MD",
            "value": 0
        },
        "Garrett, MD": {
            "county": "Garrett, MD",
            "value": 0
        },
        "Harford, MD": {
            "county": "Harford, MD",
            "value": 0
        },
        "Howard, MD": {
            "county": "Howard, MD",
            "value": 0
        },
        "Montgomery, MD": {
            "county": "Montgomery, MD",
            "value": 0
        },
        "Prince George's, MD": {
            "county": "Prince George's, MD",
            "value": 0
        },
        "Queen Anne's, MD": {
            "county": "Queen Anne's, MD",
            "value": 0
        },
        "Somerset, MD": {
            "county": "Somerset, MD",
            "value": 0
        },
        "Talbot, MD": {
            "county": "Talbot, MD",
            "value": 0
        },
        "Wicomico, MD": {
            "county": "Wicomico, MD",
            "value": 0
        },
        "Worcester, MD": {
            "county": "Worcester, MD",
            "value": 0
        },
        "Berkshire, MA": {
            "county": "Berkshire, MA",
            "value": 0
        },
        "Dukes, MA": {
            "county": "Dukes, MA",
            "value": 0
        },
        "Essex, MA": {
            "county": "Essex, MA",
            "value": 0
        },
        "Franklin, MA": {
            "county": "Franklin, MA",
            "value": 0
        },
        "Hampden, MA": {
            "county": "Hampden, MA",
            "value": 0
        },
        "Hampshire, MA": {
            "county": "Hampshire, MA",
            "value": 0
        },
        "Middlesex, MA": {
            "county": "Middlesex, MA",
            "value": 0
        },
        "Suffolk, MA": {
            "county": "Suffolk, MA",
            "value": 0
        },
        "Alcona, MI": {
            "county": "Alcona, MI",
            "value": 0
        },
        "Alger, MI": {
            "county": "Alger, MI",
            "value": 0
        },
        "Antrim, MI": {
            "county": "Antrim, MI",
            "value": 0
        },
        "Baraga, MI": {
            "county": "Baraga, MI",
            "value": 0
        },
        "Bay, MI": {
            "county": "Bay, MI",
            "value": 0
        },
        "Benzie, MI": {
            "county": "Benzie, MI",
            "value": 0
        },
        "Berrien, MI": {
            "county": "Berrien, MI",
            "value": 0
        },
        "Branch, MI": {
            "county": "Branch, MI",
            "value": 0
        },
        "Cass, MI": {
            "county": "Cass, MI",
            "value": 0
        },
        "Charlevoix, MI": {
            "county": "Charlevoix, MI",
            "value": 0
        },
        "Cheboygan, MI": {
            "county": "Cheboygan, MI",
            "value": 0
        },
        "Clare, MI": {
            "county": "Clare, MI",
            "value": 0
        },
        "Clinton, MI": {
            "county": "Clinton, MI",
            "value": 0
        },
        "Crawford, MI": {
            "county": "Crawford, MI",
            "value": 0
        },
        "Delta, MI": {
            "county": "Delta, MI",
            "value": 0
        },
        "Eaton, MI": {
            "county": "Eaton, MI",
            "value": 0
        },
        "Genesee, MI": {
            "county": "Genesee, MI",
            "value": 0
        },
        "Grand Treverse, MI": {
            "county": "Grand Treverse, MI",
            "value": 0
        },
        "Gratiot, MI": {
            "county": "Gratiot, MI",
            "value": 0
        },
        "Hillsdale, MI": {
            "county": "Hillsdale, MI",
            "value": 0
        },
        "Houghton, MI": {
            "county": "Houghton, MI",
            "value": 0
        },
        "Ingham, MI": {
            "county": "Ingham, MI",
            "value": 0
        },
        "Isabella, MI": {
            "county": "Isabella, MI",
            "value": 0
        },
        "Jackson, MI": {
            "county": "Jackson, MI",
            "value": 0
        },
        "Kalkaska, MI": {
            "county": "Kalkaska, MI",
            "value": 0
        },
        "Kent, MI": {
            "county": "Kent, MI",
            "value": 0
        },
        "Keweenaw, MI": {
            "county": "Keweenaw, MI",
            "value": 0
        },
        "Leelanau, MI": {
            "county": "Leelanau, MI",
            "value": 0
        },
        "Livingston, MI": {
            "county": "Livingston, MI",
            "value": 0
        },
        "Luce, MI": {
            "county": "Luce, MI",
            "value": 0
        },
        "Mackinac, MI": {
            "county": "Mackinac, MI",
            "value": 0
        },
        "Manistee, MI": {
            "county": "Manistee, MI",
            "value": 0
        },
        "Mason, MI": {
            "county": "Mason, MI",
            "value": 0
        },
        "Menominee, MI": {
            "county": "Menominee, MI",
            "value": 0
        },
        "Midland, MI": {
            "county": "Midland, MI",
            "value": 0
        },
        "Montcalm, MI": {
            "county": "Montcalm, MI",
            "value": 0
        },
        "Muskegon, MI": {
            "county": "Muskegon, MI",
            "value": 0
        },
        "Newaygo, MI": {
            "county": "Newaygo, MI",
            "value": 0
        },
        "Oceana, MI": {
            "county": "Oceana, MI",
            "value": 0
        },
        "Ogemaw, MI": {
            "county": "Ogemaw, MI",
            "value": 0
        },
        "Ontonagon, MI": {
            "county": "Ontonagon, MI",
            "value": 0
        },
        "Osceola, MI": {
            "county": "Osceola, MI",
            "value": 0
        },
        "Oscoda, MI": {
            "county": "Oscoda, MI",
            "value": 0
        },
        "Ottawa, MI": {
            "county": "Ottawa, MI",
            "value": 0
        },
        "Presque Isle, MI": {
            "county": "Presque Isle, MI",
            "value": 0
        },
        "Saginaw, MI": {
            "county": "Saginaw, MI",
            "value": 0
        },
        "St. Joseph, MI": {
            "county": "St. Joseph, MI",
            "value": 0
        },
        "Sanilac, MI": {
            "county": "Sanilac, MI",
            "value": 0
        },
        "Schoolcraft, MI": {
            "county": "Schoolcraft, MI",
            "value": 0
        },
        "Shiawassee, MI": {
            "county": "Shiawassee, MI",
            "value": 0
        },
        "Van Buren, MI": {
            "county": "Van Buren, MI",
            "value": 0
        },
        "Washtenaw, MI": {
            "county": "Washtenaw, MI",
            "value": 0
        },
        "Wexford, MI": {
            "county": "Wexford, MI",
            "value": 0
        },
        "Aitkin, MN": {
            "county": "Aitkin, MN",
            "value": 0
        },
        "Becker, MN": {
            "county": "Becker, MN",
            "value": 0
        },
        "Beltrami, MN": {
            "county": "Beltrami, MN",
            "value": 0
        },
        "Benton, MN": {
            "county": "Benton, MN",
            "value": 0
        },
        "Big Stone, MN": {
            "county": "Big Stone, MN",
            "value": 0
        },
        "Blue Earth, MN": {
            "county": "Blue Earth, MN",
            "value": 0
        },
        "Brown, MN": {
            "county": "Brown, MN",
            "value": 0
        },
        "Chippewa, MN": {
            "county": "Chippewa, MN",
            "value": 0
        },
        "Chisago, MN": {
            "county": "Chisago, MN",
            "value": 0
        },
        "Clearwater, MN": {
            "county": "Clearwater, MN",
            "value": 0
        },
        "Cook, MN": {
            "county": "Cook, MN",
            "value": 0
        },
        "Cottonwood, MN": {
            "county": "Cottonwood, MN",
            "value": 0
        },
        "Crow Wing, MN": {
            "county": "Crow Wing, MN",
            "value": 0
        },
        "Dakota, MN": {
            "county": "Dakota, MN",
            "value": 0
        },
        "Dodge, MN": {
            "county": "Dodge, MN",
            "value": 0
        },
        "Douglas, MN": {
            "county": "Douglas, MN",
            "value": 0
        },
        "Faribault, MN": {
            "county": "Faribault, MN",
            "value": 0
        },
        "Freeborn, MN": {
            "county": "Freeborn, MN",
            "value": 0
        },
        "Goodhue, MN": {
            "county": "Goodhue, MN",
            "value": 0
        },
        "Grant, MN": {
            "county": "Grant, MN",
            "value": 0
        },
        "Houston, MN": {
            "county": "Houston, MN",
            "value": 0
        },
        "Hubbard, MN": {
            "county": "Hubbard, MN",
            "value": 0
        },
        "Isanti, MN": {
            "county": "Isanti, MN",
            "value": 0
        },
        "Kanabec, MN": {
            "county": "Kanabec, MN",
            "value": 0
        },
        "Kandiyohi, MN": {
            "county": "Kandiyohi, MN",
            "value": 0
        },
        "Kittson, MN": {
            "county": "Kittson, MN",
            "value": 0
        },
        "Lac qui Parle, MN": {
            "county": "Lac qui Parle, MN",
            "value": 0
        },
        "Lake Of The Woods, MN": {
            "county": "Lake Of The Woods, MN",
            "value": 0
        },
        "Le Sueur, MN": {
            "county": "Le Sueur, MN",
            "value": 0
        },
        "Lincoln, MN": {
            "county": "Lincoln, MN",
            "value": 0
        },
        "Mahnomen, MN": {
            "county": "Mahnomen, MN",
            "value": 0
        },
        "Marshall, MN": {
            "county": "Marshall, MN",
            "value": 0
        },
        "Mille Lacs, MN": {
            "county": "Mille Lacs, MN",
            "value": 0
        },
        "Morrison, MN": {
            "county": "Morrison, MN",
            "value": 0
        },
        "Mower, MN": {
            "county": "Mower, MN",
            "value": 0
        },
        "Murray, MN": {
            "county": "Murray, MN",
            "value": 0
        },
        "Nicollet, MN": {
            "county": "Nicollet, MN",
            "value": 0
        },
        "Nobles, MN": {
            "county": "Nobles, MN",
            "value": 0
        },
        "Olmsted, MN": {
            "county": "Olmsted, MN",
            "value": 0
        },
        "Otter Tail, MN": {
            "county": "Otter Tail, MN",
            "value": 0
        },
        "Pennington, MN": {
            "county": "Pennington, MN",
            "value": 0
        },
        "Pine, MN": {
            "county": "Pine, MN",
            "value": 0
        },
        "Pipestone, MN": {
            "county": "Pipestone, MN",
            "value": 0
        },
        "Pope, MN": {
            "county": "Pope, MN",
            "value": 0
        },
        "Red Lake, MN": {
            "county": "Red Lake, MN",
            "value": 0
        },
        "Redwood, MN": {
            "county": "Redwood, MN",
            "value": 0
        },
        "Renville, MN": {
            "county": "Renville, MN",
            "value": 0
        },
        "Rice, MN": {
            "county": "Rice, MN",
            "value": 0
        },
        "Rock, MN": {
            "county": "Rock, MN",
            "value": 0
        },
        "Scott, MN": {
            "county": "Scott, MN",
            "value": 0
        },
        "Sherburne, MN": {
            "county": "Sherburne, MN",
            "value": 0
        },
        "Stearns, MN": {
            "county": "Stearns, MN",
            "value": 0
        },
        "Swift, MN": {
            "county": "Swift, MN",
            "value": 0
        },
        "Traverse, MN": {
            "county": "Traverse, MN",
            "value": 0
        },
        "Wabasha, MN": {
            "county": "Wabasha, MN",
            "value": 0
        },
        "Wadena, MN": {
            "county": "Wadena, MN",
            "value": 0
        },
        "Washington, MN": {
            "county": "Washington, MN",
            "value": 0
        },
        "Watonwan, MN": {
            "county": "Watonwan, MN",
            "value": 0
        },
        "Winona, MN": {
            "county": "Winona, MN",
            "value": 0
        },
        "Adams, MS": {
            "county": "Adams, MS",
            "value": 0
        },
        "Alcorn, MS": {
            "county": "Alcorn, MS",
            "value": 0
        },
        "Amite, MS": {
            "county": "Amite, MS",
            "value": 0
        },
        "Attala, MS": {
            "county": "Attala, MS",
            "value": 0
        },
        "Benton, MS": {
            "county": "Benton, MS",
            "value": 0
        },
        "Bolivar, MS": {
            "county": "Bolivar, MS",
            "value": 0
        },
        "Calhoun, MS": {
            "county": "Calhoun, MS",
            "value": 0
        },
        "Chickasaw, MS": {
            "county": "Chickasaw, MS",
            "value": 0
        },
        "Choctaw, MS": {
            "county": "Choctaw, MS",
            "value": 0
        },
        "Claiborne, MS": {
            "county": "Claiborne, MS",
            "value": 0
        },
        "Clarke, MS": {
            "county": "Clarke, MS",
            "value": 0
        },
        "Clay, MS": {
            "county": "Clay, MS",
            "value": 0
        },
        "Coahoma, MS": {
            "county": "Coahoma, MS",
            "value": 0
        },
        "Covington, MS": {
            "county": "Covington, MS",
            "value": 0
        },
        "DeSoto, MS": {
            "county": "DeSoto, MS",
            "value": 0
        },
        "George, MS": {
            "county": "George, MS",
            "value": 0
        },
        "Greene, MS": {
            "county": "Greene, MS",
            "value": 0
        },
        "Grenada, MS": {
            "county": "Grenada, MS",
            "value": 0
        },
        "Harrison, MS": {
            "county": "Harrison, MS",
            "value": 0
        },
        "Hinds, MS": {
            "county": "Hinds, MS",
            "value": 0
        },
        "Humphreys, MS": {
            "county": "Humphreys, MS",
            "value": 0
        },
        "Issaquena, MS": {
            "county": "Issaquena, MS",
            "value": 0
        },
        "Itawamba, MS": {
            "county": "Itawamba, MS",
            "value": 0
        },
        "Jackson, MS": {
            "county": "Jackson, MS",
            "value": 0
        },
        "Jasper, MS": {
            "county": "Jasper, MS",
            "value": 0
        },
        "Jefferson, MS": {
            "county": "Jefferson, MS",
            "value": 0
        },
        "Jefferson Davis, MS": {
            "county": "Jefferson Davis, MS",
            "value": 0
        },
        "Jones, MS": {
            "county": "Jones, MS",
            "value": 0
        },
        "Lafayette, MS": {
            "county": "Lafayette, MS",
            "value": 0
        },
        "Lamar, MS": {
            "county": "Lamar, MS",
            "value": 0
        },
        "Lauderdale, MS": {
            "county": "Lauderdale, MS",
            "value": 0
        },
        "Lawrence, MS": {
            "county": "Lawrence, MS",
            "value": 0
        },
        "Leake, MS": {
            "county": "Leake, MS",
            "value": 0
        },
        "Lee, MS": {
            "county": "Lee, MS",
            "value": 0
        },
        "Leflore, MS": {
            "county": "Leflore, MS",
            "value": 0
        },
        "Lincoln, MS": {
            "county": "Lincoln, MS",
            "value": 0
        },
        "Lowndes, MS": {
            "county": "Lowndes, MS",
            "value": 0
        },
        "Madison, MS": {
            "county": "Madison, MS",
            "value": 0
        },
        "Marion, MS": {
            "county": "Marion, MS",
            "value": 0
        },
        "Marshall, MS": {
            "county": "Marshall, MS",
            "value": 0
        },
        "Monroe, MS": {
            "county": "Monroe, MS",
            "value": 0
        },
        "Montgomery, MS": {
            "county": "Montgomery, MS",
            "value": 0
        },
        "Neshoba, MS": {
            "county": "Neshoba, MS",
            "value": 0
        },
        "Newton, MS": {
            "county": "Newton, MS",
            "value": 0
        },
        "Noxubee, MS": {
            "county": "Noxubee, MS",
            "value": 0
        },
        "Oktibbeha, MS": {
            "county": "Oktibbeha, MS",
            "value": 0
        },
        "Panola, MS": {
            "county": "Panola, MS",
            "value": 0
        },
        "Perry, MS": {
            "county": "Perry, MS",
            "value": 0
        },
        "Pike, MS": {
            "county": "Pike, MS",
            "value": 0
        },
        "Prentiss, MS": {
            "county": "Prentiss, MS",
            "value": 0
        },
        "Quitman, MS": {
            "county": "Quitman, MS",
            "value": 0
        },
        "Scott, MS": {
            "county": "Scott, MS",
            "value": 0
        },
        "Sharkey, MS": {
            "county": "Sharkey, MS",
            "value": 0
        },
        "Simpson, MS": {
            "county": "Simpson, MS",
            "value": 0
        },
        "Stone, MS": {
            "county": "Stone, MS",
            "value": 0
        },
        "Sunflower, MS": {
            "county": "Sunflower, MS",
            "value": 0
        },
        "Tippah, MS": {
            "county": "Tippah, MS",
            "value": 0
        },
        "Tishomingo, MS": {
            "county": "Tishomingo, MS",
            "value": 0
        },
        "Tunica, MS": {
            "county": "Tunica, MS",
            "value": 0
        },
        "Union, MS": {
            "county": "Union, MS",
            "value": 0
        },
        "Warren, MS": {
            "county": "Warren, MS",
            "value": 0
        },
        "Webster, MS": {
            "county": "Webster, MS",
            "value": 0
        },
        "Wilkinson, MS": {
            "county": "Wilkinson, MS",
            "value": 0
        },
        "Winston, MS": {
            "county": "Winston, MS",
            "value": 0
        },
        "Yalobusha, MS": {
            "county": "Yalobusha, MS",
            "value": 0
        },
        "Yazoo, MS": {
            "county": "Yazoo, MS",
            "value": 0
        },
        "Andrew, MO": {
            "county": "Andrew, MO",
            "value": 0
        },
        "Atchison, MO": {
            "county": "Atchison, MO",
            "value": 0
        },
        "Audrain, MO": {
            "county": "Audrain, MO",
            "value": 0
        },
        "Barry, MO": {
            "county": "Barry, MO",
            "value": 0
        },
        "Barton, MO": {
            "county": "Barton, MO",
            "value": 0
        },
        "Bates, MO": {
            "county": "Bates, MO",
            "value": 0
        },
        "Benton, MO": {
            "county": "Benton, MO",
            "value": 0
        },
        "Bollinger, MO": {
            "county": "Bollinger, MO",
            "value": 0
        },
        "Boone, MO": {
            "county": "Boone, MO",
            "value": 0
        },
        "Caldwell, MO": {
            "county": "Caldwell, MO",
            "value": 0
        },
        "Callaway, MO": {
            "county": "Callaway, MO",
            "value": 0
        },
        "Camden, MO": {
            "county": "Camden, MO",
            "value": 0
        },
        "Carroll, MO": {
            "county": "Carroll, MO",
            "value": 0
        },
        "Cass, MO": {
            "county": "Cass, MO",
            "value": 0
        },
        "Cedar, MO": {
            "county": "Cedar, MO",
            "value": 0
        },
        "Chariton, MO": {
            "county": "Chariton, MO",
            "value": 0
        },
        "Christian, MO": {
            "county": "Christian, MO",
            "value": 0
        },
        "Clark, MO": {
            "county": "Clark, MO",
            "value": 0
        },
        "Clay, MO": {
            "county": "Clay, MO",
            "value": 0
        },
        "Clinton, MO": {
            "county": "Clinton, MO",
            "value": 0
        },
        "Cole, MO": {
            "county": "Cole, MO",
            "value": 0
        },
        "Cooper, MO": {
            "county": "Cooper, MO",
            "value": 0
        },
        "Crawford, MO": {
            "county": "Crawford, MO",
            "value": 0
        },
        "Dade, MO": {
            "county": "Dade, MO",
            "value": 0
        },
        "Dallas, MO": {
            "county": "Dallas, MO",
            "value": 0
        },
        "Daviess, MO": {
            "county": "Daviess, MO",
            "value": 0
        },
        "DeKalb, MO": {
            "county": "DeKalb, MO",
            "value": 0
        },
        "Dunklin, MO": {
            "county": "Dunklin, MO",
            "value": 0
        },
        "Gentry, MO": {
            "county": "Gentry, MO",
            "value": 0
        },
        "Greene, MO": {
            "county": "Greene, MO",
            "value": 0
        },
        "Grundy, MO": {
            "county": "Grundy, MO",
            "value": 0
        },
        "Harrison, MO": {
            "county": "Harrison, MO",
            "value": 0
        },
        "Henry, MO": {
            "county": "Henry, MO",
            "value": 0
        },
        "Hickory, MO": {
            "county": "Hickory, MO",
            "value": 0
        },
        "Howard, MO": {
            "county": "Howard, MO",
            "value": 0
        },
        "Howell, MO": {
            "county": "Howell, MO",
            "value": 0
        },
        "Iron, MO": {
            "county": "Iron, MO",
            "value": 0
        },
        "Jackson, MO": {
            "county": "Jackson, MO",
            "value": 0
        },
        "Jefferson, MO": {
            "county": "Jefferson, MO",
            "value": 0
        },
        "Johnson, MO": {
            "county": "Johnson, MO",
            "value": 0
        },
        "Knox, MO": {
            "county": "Knox, MO",
            "value": 0
        },
        "Laclede, MO": {
            "county": "Laclede, MO",
            "value": 0
        },
        "Lafayette, MO": {
            "county": "Lafayette, MO",
            "value": 0
        },
        "Lawrence, MO": {
            "county": "Lawrence, MO",
            "value": 0
        },
        "Lewis, MO": {
            "county": "Lewis, MO",
            "value": 0
        },
        "Lincoln, MO": {
            "county": "Lincoln, MO",
            "value": 0
        },
        "Linn, MO": {
            "county": "Linn, MO",
            "value": 0
        },
        "Livingston, MO": {
            "county": "Livingston, MO",
            "value": 0
        },
        "McDonald, MO": {
            "county": "McDonald, MO",
            "value": 0
        },
        "Macon, MO": {
            "county": "Macon, MO",
            "value": 0
        },
        "Mercer, MO": {
            "county": "Mercer, MO",
            "value": 0
        },
        "Miller, MO": {
            "county": "Miller, MO",
            "value": 0
        },
        "Mississippi, MO": {
            "county": "Mississippi, MO",
            "value": 0
        },
        "Monroe, MO": {
            "county": "Monroe, MO",
            "value": 0
        },
        "Montgomery, MO": {
            "county": "Montgomery, MO",
            "value": 0
        },
        "Morgan, MO": {
            "county": "Morgan, MO",
            "value": 0
        },
        "New Madrid, MO": {
            "county": "New Madrid, MO",
            "value": 0
        },
        "Newton, MO": {
            "county": "Newton, MO",
            "value": 0
        },
        "Oregon, MO": {
            "county": "Oregon, MO",
            "value": 0
        },
        "Osage, MO": {
            "county": "Osage, MO",
            "value": 0
        },
        "Ozark, MO": {
            "county": "Ozark, MO",
            "value": 0
        },
        "Perry, MO": {
            "county": "Perry, MO",
            "value": 0
        },
        "Pike, MO": {
            "county": "Pike, MO",
            "value": 0
        },
        "Platte, MO": {
            "county": "Platte, MO",
            "value": 0
        },
        "Polk, MO": {
            "county": "Polk, MO",
            "value": 0
        },
        "Pulaski, MO": {
            "county": "Pulaski, MO",
            "value": 0
        },
        "Putnam, MO": {
            "county": "Putnam, MO",
            "value": 0
        },
        "Ralls, MO": {
            "county": "Ralls, MO",
            "value": 0
        },
        "Randolph, MO": {
            "county": "Randolph, MO",
            "value": 0
        },
        "Reynolds, MO": {
            "county": "Reynolds, MO",
            "value": 0
        },
        "Ripley, MO": {
            "county": "Ripley, MO",
            "value": 0
        },
        "St. Clair, MO": {
            "county": "St. Clair, MO",
            "value": 0
        },
        "Ste. Genevieve, MO": {
            "county": "Ste. Genevieve, MO",
            "value": 0
        },
        "St. Francois, MO": {
            "county": "St. Francois, MO",
            "value": 0
        },
        "St. Louis County, MO": {
            "county": "St. Louis County, MO",
            "value": 0
        },
        "Scotland, MO": {
            "county": "Scotland, MO",
            "value": 0
        },
        "Scott, MO": {
            "county": "Scott, MO",
            "value": 0
        },
        "Shelby, MO": {
            "county": "Shelby, MO",
            "value": 0
        },
        "Stoddard, MO": {
            "county": "Stoddard, MO",
            "value": 0
        },
        "Stone, MO": {
            "county": "Stone, MO",
            "value": 0
        },
        "Sullivan, MO": {
            "county": "Sullivan, MO",
            "value": 0
        },
        "Taney, MO": {
            "county": "Taney, MO",
            "value": 0
        },
        "Vernon, MO": {
            "county": "Vernon, MO",
            "value": 0
        },
        "Warren, MO": {
            "county": "Warren, MO",
            "value": 0
        },
        "Washington, MO": {
            "county": "Washington, MO",
            "value": 0
        },
        "Wayne, MO": {
            "county": "Wayne, MO",
            "value": 0
        },
        "Worth, MO": {
            "county": "Worth, MO",
            "value": 0
        },
        "Wright, MO": {
            "county": "Wright, MO",
            "value": 0
        },
        "St. Louis City, MO": {
            "county": "St. Louis City, MO",
            "value": 0
        },
        "Broadwater, MT": {
            "county": "Broadwater, MT",
            "value": 0
        },
        "Carbon, MT": {
            "county": "Carbon, MT",
            "value": 0
        },
        "Cascade, MT": {
            "county": "Cascade, MT",
            "value": 0
        },
        "Dawson, MT": {
            "county": "Dawson, MT",
            "value": 0
        },
        "Deer Lodge, MT": {
            "county": "Deer Lodge, MT",
            "value": 0
        },
        "Glacier, MT": {
            "county": "Glacier, MT",
            "value": 0
        },
        "Golden Valley, MT": {
            "county": "Golden Valley, MT",
            "value": 0
        },
        "Hill, MT": {
            "county": "Hill, MT",
            "value": 0
        },
        "Lake, MT": {
            "county": "Lake, MT",
            "value": 0
        },
        "McCone, MT": {
            "county": "McCone, MT",
            "value": 0
        },
        "Powder River, MT": {
            "county": "Powder River, MT",
            "value": 0
        },
        "Powell, MT": {
            "county": "Powell, MT",
            "value": 0
        },
        "Prairie, MT": {
            "county": "Prairie, MT",
            "value": 0
        },
        "Roosevelt, MT": {
            "county": "Roosevelt, MT",
            "value": 0
        },
        "Teton, MT": {
            "county": "Teton, MT",
            "value": 0
        },
        "Toole, MT": {
            "county": "Toole, MT",
            "value": 0
        },
        "Treasure, MT": {
            "county": "Treasure, MT",
            "value": 0
        },
        "Yellowstone, MT": {
            "county": "Yellowstone, MT",
            "value": 0
        },
        "Antelope, NE": {
            "county": "Antelope, NE",
            "value": 0
        },
        "Banner, NE": {
            "county": "Banner, NE",
            "value": 0
        },
        "Boone, NE": {
            "county": "Boone, NE",
            "value": 0
        },
        "Boyd, NE": {
            "county": "Boyd, NE",
            "value": 0
        },
        "Brown, NE": {
            "county": "Brown, NE",
            "value": 0
        },
        "Buffalo, NE": {
            "county": "Buffalo, NE",
            "value": 0
        },
        "Burt, NE": {
            "county": "Burt, NE",
            "value": 0
        },
        "Cass, NE": {
            "county": "Cass, NE",
            "value": 0
        },
        "Cedar, NE": {
            "county": "Cedar, NE",
            "value": 0
        },
        "Cheyenne, NE": {
            "county": "Cheyenne, NE",
            "value": 0
        },
        "Colfax, NE": {
            "county": "Colfax, NE",
            "value": 0
        },
        "Cuming, NE": {
            "county": "Cuming, NE",
            "value": 0
        },
        "Custer, NE": {
            "county": "Custer, NE",
            "value": 0
        },
        "Dakota, NE": {
            "county": "Dakota, NE",
            "value": 0
        },
        "Dawes, NE": {
            "county": "Dawes, NE",
            "value": 0
        },
        "Dawson, NE": {
            "county": "Dawson, NE",
            "value": 0
        },
        "Deuel, NE": {
            "county": "Deuel, NE",
            "value": 0
        },
        "Douglas, NE": {
            "county": "Douglas, NE",
            "value": 0
        },
        "Fillmore, NE": {
            "county": "Fillmore, NE",
            "value": 0
        },
        "Franklin, NE": {
            "county": "Franklin, NE",
            "value": 0
        },
        "Frontier, NE": {
            "county": "Frontier, NE",
            "value": 0
        },
        "Furnas, NE": {
            "county": "Furnas, NE",
            "value": 0
        },
        "Gage, NE": {
            "county": "Gage, NE",
            "value": 0
        },
        "Gosper, NE": {
            "county": "Gosper, NE",
            "value": 0
        },
        "Greeley, NE": {
            "county": "Greeley, NE",
            "value": 0
        },
        "Hall, NE": {
            "county": "Hall, NE",
            "value": 0
        },
        "Hamilton, NE": {
            "county": "Hamilton, NE",
            "value": 0
        },
        "Harlan, NE": {
            "county": "Harlan, NE",
            "value": 0
        },
        "Hayes, NE": {
            "county": "Hayes, NE",
            "value": 0
        },
        "Hitchcock, NE": {
            "county": "Hitchcock, NE",
            "value": 0
        },
        "Holt, NE": {
            "county": "Holt, NE",
            "value": 0
        },
        "Hooker, NE": {
            "county": "Hooker, NE",
            "value": 0
        },
        "Howard, NE": {
            "county": "Howard, NE",
            "value": 0
        },
        "Kearney, NE": {
            "county": "Kearney, NE",
            "value": 0
        },
        "Keith, NE": {
            "county": "Keith, NE",
            "value": 0
        },
        "Keya Paha, NE": {
            "county": "Keya Paha, NE",
            "value": 0
        },
        "Kimball, NE": {
            "county": "Kimball, NE",
            "value": 0
        },
        "Logan, NE": {
            "county": "Logan, NE",
            "value": 0
        },
        "Loup, NE": {
            "county": "Loup, NE",
            "value": 0
        },
        "McPherson, NE": {
            "county": "McPherson, NE",
            "value": 0
        },
        "Madison, NE": {
            "county": "Madison, NE",
            "value": 0
        },
        "Merrick, NE": {
            "county": "Merrick, NE",
            "value": 0
        },
        "Morrill, NE": {
            "county": "Morrill, NE",
            "value": 0
        },
        "Nance, NE": {
            "county": "Nance, NE",
            "value": 0
        },
        "Nemaha, NE": {
            "county": "Nemaha, NE",
            "value": 0
        },
        "Nuckolls, NE": {
            "county": "Nuckolls, NE",
            "value": 0
        },
        "Perkins, NE": {
            "county": "Perkins, NE",
            "value": 0
        },
        "Phelps, NE": {
            "county": "Phelps, NE",
            "value": 0
        },
        "Pierce, NE": {
            "county": "Pierce, NE",
            "value": 0
        },
        "Platte, NE": {
            "county": "Platte, NE",
            "value": 0
        },
        "Polk, NE": {
            "county": "Polk, NE",
            "value": 0
        },
        "Red Willow, NE": {
            "county": "Red Willow, NE",
            "value": 0
        },
        "Richardson, NE": {
            "county": "Richardson, NE",
            "value": 0
        },
        "Rock, NE": {
            "county": "Rock, NE",
            "value": 0
        },
        "Saline, NE": {
            "county": "Saline, NE",
            "value": 0
        },
        "Sarpy, NE": {
            "county": "Sarpy, NE",
            "value": 0
        },
        "Saunders, NE": {
            "county": "Saunders, NE",
            "value": 0
        },
        "Seward, NE": {
            "county": "Seward, NE",
            "value": 0
        },
        "Sheridan, NE": {
            "county": "Sheridan, NE",
            "value": 0
        },
        "Sherman, NE": {
            "county": "Sherman, NE",
            "value": 0
        },
        "Sioux, NE": {
            "county": "Sioux, NE",
            "value": 0
        },
        "Thayer, NE": {
            "county": "Thayer, NE",
            "value": 0
        },
        "Thomas, NE": {
            "county": "Thomas, NE",
            "value": 0
        },
        "Valley, NE": {
            "county": "Valley, NE",
            "value": 0
        },
        "Washington, NE": {
            "county": "Washington, NE",
            "value": 0
        },
        "Wayne, NE": {
            "county": "Wayne, NE",
            "value": 0
        },
        "Webster, NE": {
            "county": "Webster, NE",
            "value": 0
        },
        "Wheeler, NE": {
            "county": "Wheeler, NE",
            "value": 0
        },
        "York, NE": {
            "county": "York, NE",
            "value": 0
        },
        "Churchill, NV": {
            "county": "Churchill, NV",
            "value": 0
        },
        "Esmeralda, NV": {
            "county": "Esmeralda, NV",
            "value": 0
        },
        "Storey, NV": {
            "county": "Storey, NV",
            "value": 0
        },
        "Carson City, NV": {
            "county": "Carson City, NV",
            "value": 0
        },
        "Belknap, NH": {
            "county": "Belknap, NH",
            "value": 0
        },
        "Hillsborough, NH": {
            "county": "Hillsborough, NH",
            "value": 0
        },
        "Merrimack, NH": {
            "county": "Merrimack, NH",
            "value": 0
        },
        "Strafford, NH": {
            "county": "Strafford, NH",
            "value": 0
        },
        "Sullivan, NH": {
            "county": "Sullivan, NH",
            "value": 0
        },
        "Atlantic, NJ": {
            "county": "Atlantic, NJ",
            "value": 0
        },
        "Burlington, NJ": {
            "county": "Burlington, NJ",
            "value": 0
        },
        "Cape May, NJ": {
            "county": "Cape May, NJ",
            "value": 0
        },
        "Cumberland, NJ": {
            "county": "Cumberland, NJ",
            "value": 0
        },
        "Essex, NJ": {
            "county": "Essex, NJ",
            "value": 0
        },
        "Gloucester, NJ": {
            "county": "Gloucester, NJ",
            "value": 0
        },
        "Mercer, NJ": {
            "county": "Mercer, NJ",
            "value": 0
        },
        "Middlesex, NJ": {
            "county": "Middlesex, NJ",
            "value": 0
        },
        "Monmouth, NJ": {
            "county": "Monmouth, NJ",
            "value": 0
        },
        "Ocean, NJ": {
            "county": "Ocean, NJ",
            "value": 0
        },
        "Passaic, NJ": {
            "county": "Passaic, NJ",
            "value": 0
        },
        "Salem, NJ": {
            "county": "Salem, NJ",
            "value": 0
        },
        "Somerset, NJ": {
            "county": "Somerset, NJ",
            "value": 0
        },
        "Warren, NJ": {
            "county": "Warren, NJ",
            "value": 0
        },
        "Catron, NM": {
            "county": "Catron, NM",
            "value": 0
        },
        "Cibola, NM": {
            "county": "Cibola, NM",
            "value": 0
        },
        "Colfax, NM": {
            "county": "Colfax, NM",
            "value": 0
        },
        "De Baca, NM": {
            "county": "De Baca, NM",
            "value": 0
        },
        "Eddy, NM": {
            "county": "Eddy, NM",
            "value": 0
        },
        "Guadalupe, NM": {
            "county": "Guadalupe, NM",
            "value": 0
        },
        "Lincoln, NM": {
            "county": "Lincoln, NM",
            "value": 0
        },
        "Los Alamos, NM": {
            "county": "Los Alamos, NM",
            "value": 0
        },
        "Roosevelt, NM": {
            "county": "Roosevelt, NM",
            "value": 0
        },
        "Sandoval, NM": {
            "county": "Sandoval, NM",
            "value": 0
        },
        "Sierra, NM": {
            "county": "Sierra, NM",
            "value": 0
        },
        "Valencia, NM": {
            "county": "Valencia, NM",
            "value": 0
        },
        "Albany, NY": {
            "county": "Albany, NY",
            "value": 0
        },
        "Broome, NY": {
            "county": "Broome, NY",
            "value": 0
        },
        "Cattaraugus, NY": {
            "county": "Cattaraugus, NY",
            "value": 0
        },
        "Cayuga, NY": {
            "county": "Cayuga, NY",
            "value": 0
        },
        "Chemung, NY": {
            "county": "Chemung, NY",
            "value": 0
        },
        "Clinton, NY": {
            "county": "Clinton, NY",
            "value": 0
        },
        "Columbia, NY": {
            "county": "Columbia, NY",
            "value": 0
        },
        "Cortland, NY": {
            "county": "Cortland, NY",
            "value": 0
        },
        "Delaware, NY": {
            "county": "Delaware, NY",
            "value": 0
        },
        "Essex, NY": {
            "county": "Essex, NY",
            "value": 0
        },
        "Franklin, NY": {
            "county": "Franklin, NY",
            "value": 0
        },
        "Fulton, NY": {
            "county": "Fulton, NY",
            "value": 0
        },
        "Genesee, NY": {
            "county": "Genesee, NY",
            "value": 0
        },
        "Greene, NY": {
            "county": "Greene, NY",
            "value": 0
        },
        "Hamilton, NY": {
            "county": "Hamilton, NY",
            "value": 0
        },
        "Herkimer, NY": {
            "county": "Herkimer, NY",
            "value": 0
        },
        "Kings, NY": {
            "county": "Kings, NY",
            "value": 0
        },
        "Madison, NY": {
            "county": "Madison, NY",
            "value": 0
        },
        "Montgomery, NY": {
            "county": "Montgomery, NY",
            "value": 0
        },
        "Nassau, NY": {
            "county": "Nassau, NY",
            "value": 0
        },
        "New York, NY": {
            "county": "New York, NY",
            "value": 0
        },
        "Niagara, NY": {
            "county": "Niagara, NY",
            "value": 0
        },
        "Onondaga, NY": {
            "county": "Onondaga, NY",
            "value": 0
        },
        "Ontario, NY": {
            "county": "Ontario, NY",
            "value": 0
        },
        "Orleans, NY": {
            "county": "Orleans, NY",
            "value": 0
        },
        "Otsego, NY": {
            "county": "Otsego, NY",
            "value": 0
        },
        "Putnam, NY": {
            "county": "Putnam, NY",
            "value": 0
        },
        "Queens, NY": {
            "county": "Queens, NY",
            "value": 0
        },
        "Rensselaer, NY": {
            "county": "Rensselaer, NY",
            "value": 0
        },
        "Richmond, NY": {
            "county": "Richmond, NY",
            "value": 0
        },
        "Rockland, NY": {
            "county": "Rockland, NY",
            "value": 0
        },
        "Saratoga, NY": {
            "county": "Saratoga, NY",
            "value": 0
        },
        "Schoharie, NY": {
            "county": "Schoharie, NY",
            "value": 0
        },
        "Schuyler, NY": {
            "county": "Schuyler, NY",
            "value": 0
        },
        "Seneca, NY": {
            "county": "Seneca, NY",
            "value": 0
        },
        "Suffolk, NY": {
            "county": "Suffolk, NY",
            "value": 0
        },
        "Sullivan, NY": {
            "county": "Sullivan, NY",
            "value": 0
        },
        "Tompkins, NY": {
            "county": "Tompkins, NY",
            "value": 0
        },
        "Warren, NY": {
            "county": "Warren, NY",
            "value": 0
        },
        "Wayne, NY": {
            "county": "Wayne, NY",
            "value": 0
        },
        "Wyoming, NY": {
            "county": "Wyoming, NY",
            "value": 0
        },
        "Yates, NY": {
            "county": "Yates, NY",
            "value": 0
        },
        "Alamance, NC": {
            "county": "Alamance, NC",
            "value": 0
        },
        "Alleghany, NC": {
            "county": "Alleghany, NC",
            "value": 0
        },
        "Anson, NC": {
            "county": "Anson, NC",
            "value": 0
        },
        "Avery, NC": {
            "county": "Avery, NC",
            "value": 0
        },
        "Bertie, NC": {
            "county": "Bertie, NC",
            "value": 0
        },
        "Bladen, NC": {
            "county": "Bladen, NC",
            "value": 0
        },
        "Brunswick, NC": {
            "county": "Brunswick, NC",
            "value": 0
        },
        "Buncombe, NC": {
            "county": "Buncombe, NC",
            "value": 0
        },
        "Burke, NC": {
            "county": "Burke, NC",
            "value": 0
        },
        "Cabarrus, NC": {
            "county": "Cabarrus, NC",
            "value": 0
        },
        "Camden, NC": {
            "county": "Camden, NC",
            "value": 0
        },
        "Caswell, NC": {
            "county": "Caswell, NC",
            "value": 0
        },
        "Catawba, NC": {
            "county": "Catawba, NC",
            "value": 0
        },
        "Chatham, NC": {
            "county": "Chatham, NC",
            "value": 0
        },
        "Chowan, NC": {
            "county": "Chowan, NC",
            "value": 0
        },
        "Cleveland, NC": {
            "county": "Cleveland, NC",
            "value": 0
        },
        "Columbus, NC": {
            "county": "Columbus, NC",
            "value": 0
        },
        "Craven, NC": {
            "county": "Craven, NC",
            "value": 0
        },
        "Cumberland, NC": {
            "county": "Cumberland, NC",
            "value": 0
        },
        "Currituck, NC": {
            "county": "Currituck, NC",
            "value": 0
        },
        "Davidson, NC": {
            "county": "Davidson, NC",
            "value": 0
        },
        "Davie, NC": {
            "county": "Davie, NC",
            "value": 0
        },
        "Duplin, NC": {
            "county": "Duplin, NC",
            "value": 0
        },
        "Durham, NC": {
            "county": "Durham, NC",
            "value": 0
        },
        "Edgecombe, NC": {
            "county": "Edgecombe, NC",
            "value": 0
        },
        "Forsyth, NC": {
            "county": "Forsyth, NC",
            "value": 0
        },
        "Franklin, NC": {
            "county": "Franklin, NC",
            "value": 0
        },
        "Gaston, NC": {
            "county": "Gaston, NC",
            "value": 0
        },
        "Gates, NC": {
            "county": "Gates, NC",
            "value": 0
        },
        "Granville, NC": {
            "county": "Granville, NC",
            "value": 0
        },
        "Greene, NC": {
            "county": "Greene, NC",
            "value": 0
        },
        "Guilford, NC": {
            "county": "Guilford, NC",
            "value": 0
        },
        "Halifax, NC": {
            "county": "Halifax, NC",
            "value": 0
        },
        "Harnett, NC": {
            "county": "Harnett, NC",
            "value": 0
        },
        "Haywood, NC": {
            "county": "Haywood, NC",
            "value": 0
        },
        "Henderson, NC": {
            "county": "Henderson, NC",
            "value": 0
        },
        "Hoke, NC": {
            "county": "Hoke, NC",
            "value": 0
        },
        "Hyde, NC": {
            "county": "Hyde, NC",
            "value": 0
        },
        "Iredell, NC": {
            "county": "Iredell, NC",
            "value": 0
        },
        "Jackson, NC": {
            "county": "Jackson, NC",
            "value": 0
        },
        "Johnston, NC": {
            "county": "Johnston, NC",
            "value": 0
        },
        "Jones, NC": {
            "county": "Jones, NC",
            "value": 0
        },
        "Lee, NC": {
            "county": "Lee, NC",
            "value": 0
        },
        "Lincoln, NC": {
            "county": "Lincoln, NC",
            "value": 0
        },
        "McDowell, NC": {
            "county": "McDowell, NC",
            "value": 0
        },
        "Madison, NC": {
            "county": "Madison, NC",
            "value": 0
        },
        "Martin, NC": {
            "county": "Martin, NC",
            "value": 0
        },
        "Mecklenburg, NC": {
            "county": "Mecklenburg, NC",
            "value": 0
        },
        "Mitchell, NC": {
            "county": "Mitchell, NC",
            "value": 0
        },
        "Montgomery, NC": {
            "county": "Montgomery, NC",
            "value": 0
        },
        "Moore, NC": {
            "county": "Moore, NC",
            "value": 0
        },
        "Northampton, NC": {
            "county": "Northampton, NC",
            "value": 0
        },
        "Onslow, NC": {
            "county": "Onslow, NC",
            "value": 0
        },
        "Pasquotank, NC": {
            "county": "Pasquotank, NC",
            "value": 0
        },
        "Pender, NC": {
            "county": "Pender, NC",
            "value": 0
        },
        "Perquimans, NC": {
            "county": "Perquimans, NC",
            "value": 0
        },
        "Person, NC": {
            "county": "Person, NC",
            "value": 0
        },
        "Pitt, NC": {
            "county": "Pitt, NC",
            "value": 0
        },
        "Polk, NC": {
            "county": "Polk, NC",
            "value": 0
        },
        "Randolph, NC": {
            "county": "Randolph, NC",
            "value": 0
        },
        "Richmond, NC": {
            "county": "Richmond, NC",
            "value": 0
        },
        "Rockingham, NC": {
            "county": "Rockingham, NC",
            "value": 0
        },
        "Rutherford, NC": {
            "county": "Rutherford, NC",
            "value": 0
        },
        "Sampson, NC": {
            "county": "Sampson, NC",
            "value": 0
        },
        "Stanly, NC": {
            "county": "Stanly, NC",
            "value": 0
        },
        "Stokes, NC": {
            "county": "Stokes, NC",
            "value": 0
        },
        "Surry, NC": {
            "county": "Surry, NC",
            "value": 0
        },
        "Swain, NC": {
            "county": "Swain, NC",
            "value": 0
        },
        "Transylvania, NC": {
            "county": "Transylvania, NC",
            "value": 0
        },
        "Tyrrell, NC": {
            "county": "Tyrrell, NC",
            "value": 0
        },
        "Vance, NC": {
            "county": "Vance, NC",
            "value": 0
        },
        "Wake, NC": {
            "county": "Wake, NC",
            "value": 0
        },
        "Warren, NC": {
            "county": "Warren, NC",
            "value": 0
        },
        "Watauga, NC": {
            "county": "Watauga, NC",
            "value": 0
        },
        "Wayne, NC": {
            "county": "Wayne, NC",
            "value": 0
        },
        "Wilson, NC": {
            "county": "Wilson, NC",
            "value": 0
        },
        "Yadkin, NC": {
            "county": "Yadkin, NC",
            "value": 0
        },
        "Yancey, NC": {
            "county": "Yancey, NC",
            "value": 0
        },
        "Barnes, ND": {
            "county": "Barnes, ND",
            "value": 0
        },
        "Benson, ND": {
            "county": "Benson, ND",
            "value": 0
        },
        "Billings, ND": {
            "county": "Billings, ND",
            "value": 0
        },
        "Bottineau, ND": {
            "county": "Bottineau, ND",
            "value": 0
        },
        "Bowman, ND": {
            "county": "Bowman, ND",
            "value": 0
        },
        "Burke, ND": {
            "county": "Burke, ND",
            "value": 0
        },
        "Burleigh, ND": {
            "county": "Burleigh, ND",
            "value": 0
        },
        "Cass, ND": {
            "county": "Cass, ND",
            "value": 0
        },
        "Divide, ND": {
            "county": "Divide, ND",
            "value": 0
        },
        "Dunn, ND": {
            "county": "Dunn, ND",
            "value": 0
        },
        "Eddy, ND": {
            "county": "Eddy, ND",
            "value": 0
        },
        "Emmons, ND": {
            "county": "Emmons, ND",
            "value": 0
        },
        "Foster, ND": {
            "county": "Foster, ND",
            "value": 0
        },
        "Golden Valley, ND": {
            "county": "Golden Valley, ND",
            "value": 0
        },
        "Grand Forks, ND": {
            "county": "Grand Forks, ND",
            "value": 0
        },
        "Griggs, ND": {
            "county": "Griggs, ND",
            "value": 0
        },
        "Hettinger, ND": {
            "county": "Hettinger, ND",
            "value": 0
        },
        "Kidder, ND": {
            "county": "Kidder, ND",
            "value": 0
        },
        "LaMoure, ND": {
            "county": "LaMoure, ND",
            "value": 0
        },
        "Logan, ND": {
            "county": "Logan, ND",
            "value": 0
        },
        "McKenzie, ND": {
            "county": "McKenzie, ND",
            "value": 0
        },
        "Mercer, ND": {
            "county": "Mercer, ND",
            "value": 0
        },
        "Mountrail, ND": {
            "county": "Mountrail, ND",
            "value": 0
        },
        "Nelson, ND": {
            "county": "Nelson, ND",
            "value": 0
        },
        "Oliver, ND": {
            "county": "Oliver, ND",
            "value": 0
        },
        "Pembina, ND": {
            "county": "Pembina, ND",
            "value": 0
        },
        "Pierce, ND": {
            "county": "Pierce, ND",
            "value": 0
        },
        "Rolette, ND": {
            "county": "Rolette, ND",
            "value": 0
        },
        "Sargent, ND": {
            "county": "Sargent, ND",
            "value": 0
        },
        "Sioux, ND": {
            "county": "Sioux, ND",
            "value": 0
        },
        "Slope, ND": {
            "county": "Slope, ND",
            "value": 0
        },
        "Stark, ND": {
            "county": "Stark, ND",
            "value": 0
        },
        "Steele, ND": {
            "county": "Steele, ND",
            "value": 0
        },
        "Towner, ND": {
            "county": "Towner, ND",
            "value": 0
        },
        "Traill, ND": {
            "county": "Traill, ND",
            "value": 0
        },
        "Walsh, ND": {
            "county": "Walsh, ND",
            "value": 0
        },
        "Ward, ND": {
            "county": "Ward, ND",
            "value": 0
        },
        "Williams, ND": {
            "county": "Williams, ND",
            "value": 0
        },
        "Adams, OH": {
            "county": "Adams, OH",
            "value": 0
        },
        "Allen, OH": {
            "county": "Allen, OH",
            "value": 0
        },
        "Athens, OH": {
            "county": "Athens, OH",
            "value": 0
        },
        "Auglaize, OH": {
            "county": "Auglaize, OH",
            "value": 0
        },
        "Belmont, OH": {
            "county": "Belmont, OH",
            "value": 0
        },
        "Brown, OH": {
            "county": "Brown, OH",
            "value": 0
        },
        "Butler, OH": {
            "county": "Butler, OH",
            "value": 0
        },
        "Carroll, OH": {
            "county": "Carroll, OH",
            "value": 0
        },
        "Champaign, OH": {
            "county": "Champaign, OH",
            "value": 0
        },
        "Clermont, OH": {
            "county": "Clermont, OH",
            "value": 0
        },
        "Clinton, OH": {
            "county": "Clinton, OH",
            "value": 0
        },
        "Columbiana, OH": {
            "county": "Columbiana, OH",
            "value": 0
        },
        "Coshocton, OH": {
            "county": "Coshocton, OH",
            "value": 0
        },
        "Crawford, OH": {
            "county": "Crawford, OH",
            "value": 0
        },
        "Cuyahoga, OH": {
            "county": "Cuyahoga, OH",
            "value": 0
        },
        "Darke, OH": {
            "county": "Darke, OH",
            "value": 0
        },
        "Defiance, OH": {
            "county": "Defiance, OH",
            "value": 0
        },
        "Delaware, OH": {
            "county": "Delaware, OH",
            "value": 0
        },
        "Erie, OH": {
            "county": "Erie, OH",
            "value": 0
        },
        "Fairfield, OH": {
            "county": "Fairfield, OH",
            "value": 0
        },
        "Franklin, OH": {
            "county": "Franklin, OH",
            "value": 0
        },
        "Fulton, OH": {
            "county": "Fulton, OH",
            "value": 0
        },
        "Geauga, OH": {
            "county": "Geauga, OH",
            "value": 0
        },
        "Greene, OH": {
            "county": "Greene, OH",
            "value": 0
        },
        "Guernsey, OH": {
            "county": "Guernsey, OH",
            "value": 0
        },
        "Hamilton, OH": {
            "county": "Hamilton, OH",
            "value": 0
        },
        "Hancock, OH": {
            "county": "Hancock, OH",
            "value": 0
        },
        "Hardin, OH": {
            "county": "Hardin, OH",
            "value": 0
        },
        "Harrison, OH": {
            "county": "Harrison, OH",
            "value": 0
        },
        "Henry, OH": {
            "county": "Henry, OH",
            "value": 0
        },
        "Highland, OH": {
            "county": "Highland, OH",
            "value": 0
        },
        "Hocking, OH": {
            "county": "Hocking, OH",
            "value": 0
        },
        "Holmes, OH": {
            "county": "Holmes, OH",
            "value": 0
        },
        "Huron, OH": {
            "county": "Huron, OH",
            "value": 0
        },
        "Jackson, OH": {
            "county": "Jackson, OH",
            "value": 0
        },
        "Lake, OH": {
            "county": "Lake, OH",
            "value": 0
        },
        "Licking, OH": {
            "county": "Licking, OH",
            "value": 0
        },
        "Logan, OH": {
            "county": "Logan, OH",
            "value": 0
        },
        "Lucas, OH": {
            "county": "Lucas, OH",
            "value": 0
        },
        "Madison, OH": {
            "county": "Madison, OH",
            "value": 0
        },
        "Mahoning, OH": {
            "county": "Mahoning, OH",
            "value": 0
        },
        "Marion, OH": {
            "county": "Marion, OH",
            "value": 0
        },
        "Medina, OH": {
            "county": "Medina, OH",
            "value": 0
        },
        "Meigs, OH": {
            "county": "Meigs, OH",
            "value": 0
        },
        "Mercer, OH": {
            "county": "Mercer, OH",
            "value": 0
        },
        "Miami, OH": {
            "county": "Miami, OH",
            "value": 0
        },
        "Monroe, OH": {
            "county": "Monroe, OH",
            "value": 0
        },
        "Montgomery, OH": {
            "county": "Montgomery, OH",
            "value": 0
        },
        "Morgan, OH": {
            "county": "Morgan, OH",
            "value": 0
        },
        "Morrow, OH": {
            "county": "Morrow, OH",
            "value": 0
        },
        "Muskingum, OH": {
            "county": "Muskingum, OH",
            "value": 0
        },
        "Ottawa, OH": {
            "county": "Ottawa, OH",
            "value": 0
        },
        "Paulding, OH": {
            "county": "Paulding, OH",
            "value": 0
        },
        "Perry, OH": {
            "county": "Perry, OH",
            "value": 0
        },
        "Pickaway, OH": {
            "county": "Pickaway, OH",
            "value": 0
        },
        "Pike, OH": {
            "county": "Pike, OH",
            "value": 0
        },
        "Portage, OH": {
            "county": "Portage, OH",
            "value": 0
        },
        "Preble, OH": {
            "county": "Preble, OH",
            "value": 0
        },
        "Putnam, OH": {
            "county": "Putnam, OH",
            "value": 0
        },
        "Richland, OH": {
            "county": "Richland, OH",
            "value": 0
        },
        "Ross, OH": {
            "county": "Ross, OH",
            "value": 0
        },
        "Sandusky, OH": {
            "county": "Sandusky, OH",
            "value": 0
        },
        "Scioto, OH": {
            "county": "Scioto, OH",
            "value": 0
        },
        "Seneca, OH": {
            "county": "Seneca, OH",
            "value": 0
        },
        "Shelby, OH": {
            "county": "Shelby, OH",
            "value": 0
        },
        "Stark, OH": {
            "county": "Stark, OH",
            "value": 0
        },
        "Tuscarawas, OH": {
            "county": "Tuscarawas, OH",
            "value": 0
        },
        "Vinton, OH": {
            "county": "Vinton, OH",
            "value": 0
        },
        "Warren, OH": {
            "county": "Warren, OH",
            "value": 0
        },
        "Washington, OH": {
            "county": "Washington, OH",
            "value": 0
        },
        "Williams, OH": {
            "county": "Williams, OH",
            "value": 0
        },
        "Wyandot, OH": {
            "county": "Wyandot, OH",
            "value": 0
        },
        "Adair, OK": {
            "county": "Adair, OK",
            "value": 0
        },
        "Beaver, OK": {
            "county": "Beaver, OK",
            "value": 0
        },
        "Bryan, OK": {
            "county": "Bryan, OK",
            "value": 0
        },
        "Caddo, OK": {
            "county": "Caddo, OK",
            "value": 0
        },
        "Carter, OK": {
            "county": "Carter, OK",
            "value": 0
        },
        "Cimarron, OK": {
            "county": "Cimarron, OK",
            "value": 0
        },
        "Cleveland, OK": {
            "county": "Cleveland, OK",
            "value": 0
        },
        "Comanche, OK": {
            "county": "Comanche, OK",
            "value": 0
        },
        "Cotton, OK": {
            "county": "Cotton, OK",
            "value": 0
        },
        "Craig, OK": {
            "county": "Craig, OK",
            "value": 0
        },
        "Creek, OK": {
            "county": "Creek, OK",
            "value": 0
        },
        "Delaware, OK": {
            "county": "Delaware, OK",
            "value": 0
        },
        "Ellis, OK": {
            "county": "Ellis, OK",
            "value": 0
        },
        "Grant, OK": {
            "county": "Grant, OK",
            "value": 0
        },
        "Greer, OK": {
            "county": "Greer, OK",
            "value": 0
        },
        "Harmon, OK": {
            "county": "Harmon, OK",
            "value": 0
        },
        "Haskell, OK": {
            "county": "Haskell, OK",
            "value": 0
        },
        "Hughes, OK": {
            "county": "Hughes, OK",
            "value": 0
        },
        "Jackson, OK": {
            "county": "Jackson, OK",
            "value": 0
        },
        "Kingfisher, OK": {
            "county": "Kingfisher, OK",
            "value": 0
        },
        "Kiowa, OK": {
            "county": "Kiowa, OK",
            "value": 0
        },
        "Le Flore, OK": {
            "county": "Le Flore, OK",
            "value": 0
        },
        "Logan, OK": {
            "county": "Logan, OK",
            "value": 0
        },
        "Love, OK": {
            "county": "Love, OK",
            "value": 0
        },
        "McIntosh, OK": {
            "county": "McIntosh, OK",
            "value": 0
        },
        "Major, OK": {
            "county": "Major, OK",
            "value": 0
        },
        "Marshall, OK": {
            "county": "Marshall, OK",
            "value": 0
        },
        "Mayes, OK": {
            "county": "Mayes, OK",
            "value": 0
        },
        "Murray, OK": {
            "county": "Murray, OK",
            "value": 0
        },
        "Noble, OK": {
            "county": "Noble, OK",
            "value": 0
        },
        "Nowata, OK": {
            "county": "Nowata, OK",
            "value": 0
        },
        "Okfuskee, OK": {
            "county": "Okfuskee, OK",
            "value": 0
        },
        "Ottawa, OK": {
            "county": "Ottawa, OK",
            "value": 0
        },
        "Pawnee, OK": {
            "county": "Pawnee, OK",
            "value": 0
        },
        "Pottawatomie, OK": {
            "county": "Pottawatomie, OK",
            "value": 0
        },
        "Pushmataha, OK": {
            "county": "Pushmataha, OK",
            "value": 0
        },
        "Rogers, OK": {
            "county": "Rogers, OK",
            "value": 0
        },
        "Seminole, OK": {
            "county": "Seminole, OK",
            "value": 0
        },
        "Sequoyah, OK": {
            "county": "Sequoyah, OK",
            "value": 0
        },
        "Stephens, OK": {
            "county": "Stephens, OK",
            "value": 0
        },
        "Tulsa, OK": {
            "county": "Tulsa, OK",
            "value": 0
        },
        "Wagoner, OK": {
            "county": "Wagoner, OK",
            "value": 0
        },
        "Washington, OK": {
            "county": "Washington, OK",
            "value": 0
        },
        "Washita, OK": {
            "county": "Washita, OK",
            "value": 0
        },
        "Woods, OK": {
            "county": "Woods, OK",
            "value": 0
        },
        "Clatsop, OR": {
            "county": "Clatsop, OR",
            "value": 0
        },
        "Deschutes, OR": {
            "county": "Deschutes, OR",
            "value": 0
        },
        "Gilliam, OR": {
            "county": "Gilliam, OR",
            "value": 0
        },
        "Hood River, OR": {
            "county": "Hood River, OR",
            "value": 0
        },
        "Morrow, OR": {
            "county": "Morrow, OR",
            "value": 0
        },
        "Multnomah, OR": {
            "county": "Multnomah, OR",
            "value": 0
        },
        "Polk, OR": {
            "county": "Polk, OR",
            "value": 0
        },
        "Sherman, OR": {
            "county": "Sherman, OR",
            "value": 0
        },
        "Adams, PA": {
            "county": "Adams, PA",
            "value": 0
        },
        "Allegheny, PA": {
            "county": "Allegheny, PA",
            "value": 0
        },
        "Armstrong, PA": {
            "county": "Armstrong, PA",
            "value": 0
        },
        "Blair, PA": {
            "county": "Blair, PA",
            "value": 0
        },
        "Bradford, PA": {
            "county": "Bradford, PA",
            "value": 0
        },
        "Butler, PA": {
            "county": "Butler, PA",
            "value": 0
        },
        "Cameron, PA": {
            "county": "Cameron, PA",
            "value": 0
        },
        "Carbon, PA": {
            "county": "Carbon, PA",
            "value": 0
        },
        "Centre, PA": {
            "county": "Centre, PA",
            "value": 0
        },
        "Chester, PA": {
            "county": "Chester, PA",
            "value": 0
        },
        "Clarion, PA": {
            "county": "Clarion, PA",
            "value": 0
        },
        "Clearfield, PA": {
            "county": "Clearfield, PA",
            "value": 0
        },
        "Columbia, PA": {
            "county": "Columbia, PA",
            "value": 0
        },
        "Cumberland, PA": {
            "county": "Cumberland, PA",
            "value": 0
        },
        "Dauphin, PA": {
            "county": "Dauphin, PA",
            "value": 0
        },
        "Delaware, PA": {
            "county": "Delaware, PA",
            "value": 0
        },
        "Elk, PA": {
            "county": "Elk, PA",
            "value": 0
        },
        "Forest, PA": {
            "county": "Forest, PA",
            "value": 0
        },
        "Franklin, PA": {
            "county": "Franklin, PA",
            "value": 0
        },
        "Fulton, PA": {
            "county": "Fulton, PA",
            "value": 0
        },
        "Greene, PA": {
            "county": "Greene, PA",
            "value": 0
        },
        "Juniata, PA": {
            "county": "Juniata, PA",
            "value": 0
        },
        "Lackawanna, PA": {
            "county": "Lackawanna, PA",
            "value": 0
        },
        "Lawrence, PA": {
            "county": "Lawrence, PA",
            "value": 0
        },
        "Lebanon, PA": {
            "county": "Lebanon, PA",
            "value": 0
        },
        "Lehigh, PA": {
            "county": "Lehigh, PA",
            "value": 0
        },
        "Lycoming, PA": {
            "county": "Lycoming, PA",
            "value": 0
        },
        "Mifflin, PA": {
            "county": "Mifflin, PA",
            "value": 0
        },
        "Montgomery, PA": {
            "county": "Montgomery, PA",
            "value": 0
        },
        "Montour, PA": {
            "county": "Montour, PA",
            "value": 0
        },
        "Northampton, PA": {
            "county": "Northampton, PA",
            "value": 0
        },
        "Northumberland, PA": {
            "county": "Northumberland, PA",
            "value": 0
        },
        "Perry, PA": {
            "county": "Perry, PA",
            "value": 0
        },
        "Philadelphia, PA": {
            "county": "Philadelphia, PA",
            "value": 0
        },
        "Pike, PA": {
            "county": "Pike, PA",
            "value": 0
        },
        "Schuylkill, PA": {
            "county": "Schuylkill, PA",
            "value": 0
        },
        "Snyder, PA": {
            "county": "Snyder, PA",
            "value": 0
        },
        "Sullivan, PA": {
            "county": "Sullivan, PA",
            "value": 0
        },
        "Union, PA": {
            "county": "Union, PA",
            "value": 0
        },
        "Venango, PA": {
            "county": "Venango, PA",
            "value": 0
        },
        "Warren, PA": {
            "county": "Warren, PA",
            "value": 0
        },
        "Washington, PA": {
            "county": "Washington, PA",
            "value": 0
        },
        "Wayne, PA": {
            "county": "Wayne, PA",
            "value": 0
        },
        "Westmoreland, PA": {
            "county": "Westmoreland, PA",
            "value": 0
        },
        "Wyoming, PA": {
            "county": "Wyoming, PA",
            "value": 0
        },
        "York, PA": {
            "county": "York, PA",
            "value": 0
        },
        "Bristol, RI": {
            "county": "Bristol, RI",
            "value": 0
        },
        "Kent, RI": {
            "county": "Kent, RI",
            "value": 0
        },
        "Newport, RI": {
            "county": "Newport, RI",
            "value": 0
        },
        "Providence, RI": {
            "county": "Providence, RI",
            "value": 0
        },
        "Abbeville, SC": {
            "county": "Abbeville, SC",
            "value": 0
        },
        "Aiken, SC": {
            "county": "Aiken, SC",
            "value": 0
        },
        "Allendale, SC": {
            "county": "Allendale, SC",
            "value": 0
        },
        "Anderson, SC": {
            "county": "Anderson, SC",
            "value": 0
        },
        "Bamberg, SC": {
            "county": "Bamberg, SC",
            "value": 0
        },
        "Barnwell, SC": {
            "county": "Barnwell, SC",
            "value": 0
        },
        "Beaufort, SC": {
            "county": "Beaufort, SC",
            "value": 0
        },
        "Berkeley, SC": {
            "county": "Berkeley, SC",
            "value": 0
        },
        "Calhoun, SC": {
            "county": "Calhoun, SC",
            "value": 0
        },
        "Chester, SC": {
            "county": "Chester, SC",
            "value": 0
        },
        "Chesterfield, SC": {
            "county": "Chesterfield, SC",
            "value": 0
        },
        "Clarendon, SC": {
            "county": "Clarendon, SC",
            "value": 0
        },
        "Colleton, SC": {
            "county": "Colleton, SC",
            "value": 0
        },
        "Darlington, SC": {
            "county": "Darlington, SC",
            "value": 0
        },
        "Dorchester, SC": {
            "county": "Dorchester, SC",
            "value": 0
        },
        "Edgefield, SC": {
            "county": "Edgefield, SC",
            "value": 0
        },
        "Fairfield, SC": {
            "county": "Fairfield, SC",
            "value": 0
        },
        "Florence, SC": {
            "county": "Florence, SC",
            "value": 0
        },
        "Greenville, SC": {
            "county": "Greenville, SC",
            "value": 0
        },
        "Greenwood, SC": {
            "county": "Greenwood, SC",
            "value": 0
        },
        "Hampton, SC": {
            "county": "Hampton, SC",
            "value": 0
        },
        "Horry, SC": {
            "county": "Horry, SC",
            "value": 0
        },
        "Jasper, SC": {
            "county": "Jasper, SC",
            "value": 0
        },
        "Lee, SC": {
            "county": "Lee, SC",
            "value": 0
        },
        "Lexington, SC": {
            "county": "Lexington, SC",
            "value": 0
        },
        "McCormick, SC": {
            "county": "McCormick, SC",
            "value": 0
        },
        "Marion, SC": {
            "county": "Marion, SC",
            "value": 0
        },
        "Marlboro, SC": {
            "county": "Marlboro, SC",
            "value": 0
        },
        "Orangeburg, SC": {
            "county": "Orangeburg, SC",
            "value": 0
        },
        "Pickens, SC": {
            "county": "Pickens, SC",
            "value": 0
        },
        "Richland, SC": {
            "county": "Richland, SC",
            "value": 0
        },
        "Saluda, SC": {
            "county": "Saluda, SC",
            "value": 0
        },
        "Spartanburg, SC": {
            "county": "Spartanburg, SC",
            "value": 0
        },
        "Sumter, SC": {
            "county": "Sumter, SC",
            "value": 0
        },
        "Union, SC": {
            "county": "Union, SC",
            "value": 0
        },
        "Williamsburg, SC": {
            "county": "Williamsburg, SC",
            "value": 0
        },
        "York, SC": {
            "county": "York, SC",
            "value": 0
        },
        "Aurora, SD": {
            "county": "Aurora, SD",
            "value": 0
        },
        "Beadle, SD": {
            "county": "Beadle, SD",
            "value": 0
        },
        "Bennett, SD": {
            "county": "Bennett, SD",
            "value": 0
        },
        "Bon Homme, SD": {
            "county": "Bon Homme, SD",
            "value": 0
        },
        "Brown, SD": {
            "county": "Brown, SD",
            "value": 0
        },
        "Brule, SD": {
            "county": "Brule, SD",
            "value": 0
        },
        "Buffalo, SD": {
            "county": "Buffalo, SD",
            "value": 0
        },
        "Campbell, SD": {
            "county": "Campbell, SD",
            "value": 0
        },
        "Charles Mix, SD": {
            "county": "Charles Mix, SD",
            "value": 0
        },
        "Clay, SD": {
            "county": "Clay, SD",
            "value": 0
        },
        "Codington, SD": {
            "county": "Codington, SD",
            "value": 0
        },
        "Corson, SD": {
            "county": "Corson, SD",
            "value": 0
        },
        "Davison, SD": {
            "county": "Davison, SD",
            "value": 0
        },
        "Deuel, SD": {
            "county": "Deuel, SD",
            "value": 0
        },
        "Dewey, SD": {
            "county": "Dewey, SD",
            "value": 0
        },
        "Douglas, SD": {
            "county": "Douglas, SD",
            "value": 0
        },
        "Edmunds, SD": {
            "county": "Edmunds, SD",
            "value": 0
        },
        "Faulk, SD": {
            "county": "Faulk, SD",
            "value": 0
        },
        "Grant, SD": {
            "county": "Grant, SD",
            "value": 0
        },
        "Hamlin, SD": {
            "county": "Hamlin, SD",
            "value": 0
        },
        "Hanson, SD": {
            "county": "Hanson, SD",
            "value": 0
        },
        "Hughes, SD": {
            "county": "Hughes, SD",
            "value": 0
        },
        "Hutchinson, SD": {
            "county": "Hutchinson, SD",
            "value": 0
        },
        "Hyde, SD": {
            "county": "Hyde, SD",
            "value": 0
        },
        "Jackson, SD": {
            "county": "Jackson, SD",
            "value": 0
        },
        "Jerauld, SD": {
            "county": "Jerauld, SD",
            "value": 0
        },
        "Jones, SD": {
            "county": "Jones, SD",
            "value": 0
        },
        "Kingsbury, SD": {
            "county": "Kingsbury, SD",
            "value": 0
        },
        "Lake, SD": {
            "county": "Lake, SD",
            "value": 0
        },
        "Lincoln, SD": {
            "county": "Lincoln, SD",
            "value": 0
        },
        "Lyman, SD": {
            "county": "Lyman, SD",
            "value": 0
        },
        "McCook, SD": {
            "county": "McCook, SD",
            "value": 0
        },
        "Meade, SD": {
            "county": "Meade, SD",
            "value": 0
        },
        "Miner, SD": {
            "county": "Miner, SD",
            "value": 0
        },
        "Minnehaha, SD": {
            "county": "Minnehaha, SD",
            "value": 0
        },
        "Moody, SD": {
            "county": "Moody, SD",
            "value": 0
        },
        "Potter, SD": {
            "county": "Potter, SD",
            "value": 0
        },
        "Roberts, SD": {
            "county": "Roberts, SD",
            "value": 0
        },
        "Sanborn, SD": {
            "county": "Sanborn, SD",
            "value": 0
        },
        "Shannon, SD": {
            "county": "Shannon, SD",
            "value": 0
        },
        "Spink, SD": {
            "county": "Spink, SD",
            "value": 0
        },
        "Sully, SD": {
            "county": "Sully, SD",
            "value": 0
        },
        "Todd, SD": {
            "county": "Todd, SD",
            "value": 0
        },
        "Turner, SD": {
            "county": "Turner, SD",
            "value": 0
        },
        "Union, SD": {
            "county": "Union, SD",
            "value": 0
        },
        "Walworth, SD": {
            "county": "Walworth, SD",
            "value": 0
        },
        "Yankton, SD": {
            "county": "Yankton, SD",
            "value": 0
        },
        "Ziebach, SD": {
            "county": "Ziebach, SD",
            "value": 0
        },
        "Anderson, TN": {
            "county": "Anderson, TN",
            "value": 0
        },
        "Bedford, TN": {
            "county": "Bedford, TN",
            "value": 0
        },
        "Benton, TN": {
            "county": "Benton, TN",
            "value": 0
        },
        "Bledsoe, TN": {
            "county": "Bledsoe, TN",
            "value": 0
        },
        "Bradley, TN": {
            "county": "Bradley, TN",
            "value": 0
        },
        "Campbell, TN": {
            "county": "Campbell, TN",
            "value": 0
        },
        "Cannon, TN": {
            "county": "Cannon, TN",
            "value": 0
        },
        "Carroll, TN": {
            "county": "Carroll, TN",
            "value": 0
        },
        "Carter, TN": {
            "county": "Carter, TN",
            "value": 0
        },
        "Cheatham, TN": {
            "county": "Cheatham, TN",
            "value": 0
        },
        "Chester, TN": {
            "county": "Chester, TN",
            "value": 0
        },
        "Claiborne, TN": {
            "county": "Claiborne, TN",
            "value": 0
        },
        "Cocke, TN": {
            "county": "Cocke, TN",
            "value": 0
        },
        "Coffee, TN": {
            "county": "Coffee, TN",
            "value": 0
        },
        "Crockett, TN": {
            "county": "Crockett, TN",
            "value": 0
        },
        "Cumberland, TN": {
            "county": "Cumberland, TN",
            "value": 0
        },
        "Davidson, TN": {
            "county": "Davidson, TN",
            "value": 0
        },
        "Decatur, TN": {
            "county": "Decatur, TN",
            "value": 0
        },
        "DeKalb, TN": {
            "county": "DeKalb, TN",
            "value": 0
        },
        "Dickson, TN": {
            "county": "Dickson, TN",
            "value": 0
        },
        "Dyer, TN": {
            "county": "Dyer, TN",
            "value": 0
        },
        "Fayette, TN": {
            "county": "Fayette, TN",
            "value": 0
        },
        "Franklin, TN": {
            "county": "Franklin, TN",
            "value": 0
        },
        "Gibson, TN": {
            "county": "Gibson, TN",
            "value": 0
        },
        "Giles, TN": {
            "county": "Giles, TN",
            "value": 0
        },
        "Grainger, TN": {
            "county": "Grainger, TN",
            "value": 0
        },
        "Greene, TN": {
            "county": "Greene, TN",
            "value": 0
        },
        "Grundy, TN": {
            "county": "Grundy, TN",
            "value": 0
        },
        "Hamblen, TN": {
            "county": "Hamblen, TN",
            "value": 0
        },
        "Hamilton, TN": {
            "county": "Hamilton, TN",
            "value": 0
        },
        "Hancock, TN": {
            "county": "Hancock, TN",
            "value": 0
        },
        "Hardeman, TN": {
            "county": "Hardeman, TN",
            "value": 0
        },
        "Hardin, TN": {
            "county": "Hardin, TN",
            "value": 0
        },
        "Haywood, TN": {
            "county": "Haywood, TN",
            "value": 0
        },
        "Henderson, TN": {
            "county": "Henderson, TN",
            "value": 0
        },
        "Henry, TN": {
            "county": "Henry, TN",
            "value": 0
        },
        "Hickman, TN": {
            "county": "Hickman, TN",
            "value": 0
        },
        "Houston, TN": {
            "county": "Houston, TN",
            "value": 0
        },
        "Humphreys, TN": {
            "county": "Humphreys, TN",
            "value": 0
        },
        "Jackson, TN": {
            "county": "Jackson, TN",
            "value": 0
        },
        "Jefferson, TN": {
            "county": "Jefferson, TN",
            "value": 0
        },
        "Johnson, TN": {
            "county": "Johnson, TN",
            "value": 0
        },
        "Knox, TN": {
            "county": "Knox, TN",
            "value": 0
        },
        "Lake, TN": {
            "county": "Lake, TN",
            "value": 0
        },
        "Lauderdale, TN": {
            "county": "Lauderdale, TN",
            "value": 0
        },
        "Lincoln, TN": {
            "county": "Lincoln, TN",
            "value": 0
        },
        "McNairy, TN": {
            "county": "McNairy, TN",
            "value": 0
        },
        "Macon, TN": {
            "county": "Macon, TN",
            "value": 0
        },
        "Madison, TN": {
            "county": "Madison, TN",
            "value": 0
        },
        "Marion, TN": {
            "county": "Marion, TN",
            "value": 0
        },
        "Meigs, TN": {
            "county": "Meigs, TN",
            "value": 0
        },
        "Monroe, TN": {
            "county": "Monroe, TN",
            "value": 0
        },
        "Montgomery, TN": {
            "county": "Montgomery, TN",
            "value": 0
        },
        "Moore, TN": {
            "county": "Moore, TN",
            "value": 0
        },
        "Morgan, TN": {
            "county": "Morgan, TN",
            "value": 0
        },
        "Obion, TN": {
            "county": "Obion, TN",
            "value": 0
        },
        "Overton, TN": {
            "county": "Overton, TN",
            "value": 0
        },
        "Perry, TN": {
            "county": "Perry, TN",
            "value": 0
        },
        "Pickett, TN": {
            "county": "Pickett, TN",
            "value": 0
        },
        "Polk, TN": {
            "county": "Polk, TN",
            "value": 0
        },
        "Putnam, TN": {
            "county": "Putnam, TN",
            "value": 0
        },
        "Rhea, TN": {
            "county": "Rhea, TN",
            "value": 0
        },
        "Roane, TN": {
            "county": "Roane, TN",
            "value": 0
        },
        "Robertson, TN": {
            "county": "Robertson, TN",
            "value": 0
        },
        "Rutherford, TN": {
            "county": "Rutherford, TN",
            "value": 0
        },
        "Scott, TN": {
            "county": "Scott, TN",
            "value": 0
        },
        "Sequatchie, TN": {
            "county": "Sequatchie, TN",
            "value": 0
        },
        "Sevier, TN": {
            "county": "Sevier, TN",
            "value": 0
        },
        "Smith, TN": {
            "county": "Smith, TN",
            "value": 0
        },
        "Stewart, TN": {
            "county": "Stewart, TN",
            "value": 0
        },
        "Sullivan, TN": {
            "county": "Sullivan, TN",
            "value": 0
        },
        "Trousdale, TN": {
            "county": "Trousdale, TN",
            "value": 0
        },
        "Unicoi, TN": {
            "county": "Unicoi, TN",
            "value": 0
        },
        "Union, TN": {
            "county": "Union, TN",
            "value": 0
        },
        "Van Buren, TN": {
            "county": "Van Buren, TN",
            "value": 0
        },
        "Washington, TN": {
            "county": "Washington, TN",
            "value": 0
        },
        "Wayne, TN": {
            "county": "Wayne, TN",
            "value": 0
        },
        "Weakley, TN": {
            "county": "Weakley, TN",
            "value": 0
        },
        "White, TN": {
            "county": "White, TN",
            "value": 0
        },
        "Andrews, TX": {
            "county": "Andrews, TX",
            "value": 0
        },
        "Angelina, TX": {
            "county": "Angelina, TX",
            "value": 0
        },
        "Archer, TX": {
            "county": "Archer, TX",
            "value": 0
        },
        "Armstrong, TX": {
            "county": "Armstrong, TX",
            "value": 0
        },
        "Austin, TX": {
            "county": "Austin, TX",
            "value": 0
        },
        "Bailey, TX": {
            "county": "Bailey, TX",
            "value": 0
        },
        "Bandera, TX": {
            "county": "Bandera, TX",
            "value": 0
        },
        "Bastrop, TX": {
            "county": "Bastrop, TX",
            "value": 0
        },
        "Baylor, TX": {
            "county": "Baylor, TX",
            "value": 0
        },
        "Bee, TX": {
            "county": "Bee, TX",
            "value": 0
        },
        "Bell, TX": {
            "county": "Bell, TX",
            "value": 0
        },
        "Bexar, TX": {
            "county": "Bexar, TX",
            "value": 0
        },
        "Blanco, TX": {
            "county": "Blanco, TX",
            "value": 0
        },
        "Bowie, TX": {
            "county": "Bowie, TX",
            "value": 0
        },
        "Brazos, TX": {
            "county": "Brazos, TX",
            "value": 0
        },
        "Briscoe, TX": {
            "county": "Briscoe, TX",
            "value": 0
        },
        "Brooks, TX": {
            "county": "Brooks, TX",
            "value": 0
        },
        "Brown, TX": {
            "county": "Brown, TX",
            "value": 0
        },
        "Burleson, TX": {
            "county": "Burleson, TX",
            "value": 0
        },
        "Burnet, TX": {
            "county": "Burnet, TX",
            "value": 0
        },
        "Caldwell, TX": {
            "county": "Caldwell, TX",
            "value": 0
        },
        "Calhoun, TX": {
            "county": "Calhoun, TX",
            "value": 0
        },
        "Callahan, TX": {
            "county": "Callahan, TX",
            "value": 0
        },
        "Camp, TX": {
            "county": "Camp, TX",
            "value": 0
        },
        "Cass, TX": {
            "county": "Cass, TX",
            "value": 0
        },
        "Castro, TX": {
            "county": "Castro, TX",
            "value": 0
        },
        "Chambers, TX": {
            "county": "Chambers, TX",
            "value": 0
        },
        "Childress, TX": {
            "county": "Childress, TX",
            "value": 0
        },
        "Clay, TX": {
            "county": "Clay, TX",
            "value": 0
        },
        "Cochran, TX": {
            "county": "Cochran, TX",
            "value": 0
        },
        "Coke, TX": {
            "county": "Coke, TX",
            "value": 0
        },
        "Coleman, TX": {
            "county": "Coleman, TX",
            "value": 0
        },
        "Collin, TX": {
            "county": "Collin, TX",
            "value": 0
        },
        "Collingsworth, TX": {
            "county": "Collingsworth, TX",
            "value": 0
        },
        "Colorado, TX": {
            "county": "Colorado, TX",
            "value": 0
        },
        "Comal, TX": {
            "county": "Comal, TX",
            "value": 0
        },
        "Cooke, TX": {
            "county": "Cooke, TX",
            "value": 0
        },
        "Cottle, TX": {
            "county": "Cottle, TX",
            "value": 0
        },
        "Crockett, TX": {
            "county": "Crockett, TX",
            "value": 0
        },
        "Crosby, TX": {
            "county": "Crosby, TX",
            "value": 0
        },
        "Culberson, TX": {
            "county": "Culberson, TX",
            "value": 0
        },
        "Dallam, TX": {
            "county": "Dallam, TX",
            "value": 0
        },
        "Dallas, TX": {
            "county": "Dallas, TX",
            "value": 0
        },
        "Dawson, TX": {
            "county": "Dawson, TX",
            "value": 0
        },
        "Deaf Smith, TX": {
            "county": "Deaf Smith, TX",
            "value": 0
        },
        "Delta, TX": {
            "county": "Delta, TX",
            "value": 0
        },
        "Denton, TX": {
            "county": "Denton, TX",
            "value": 0
        },
        "DeWitt, TX": {
            "county": "DeWitt, TX",
            "value": 0
        },
        "Dickens, TX": {
            "county": "Dickens, TX",
            "value": 0
        },
        "Dimmit, TX": {
            "county": "Dimmit, TX",
            "value": 0
        },
        "Eastland, TX": {
            "county": "Eastland, TX",
            "value": 0
        },
        "Ector, TX": {
            "county": "Ector, TX",
            "value": 0
        },
        "El Paso, TX": {
            "county": "El Paso, TX",
            "value": 0
        },
        "Falls, TX": {
            "county": "Falls, TX",
            "value": 0
        },
        "Fannin, TX": {
            "county": "Fannin, TX",
            "value": 0
        },
        "Fayette, TX": {
            "county": "Fayette, TX",
            "value": 0
        },
        "Fisher, TX": {
            "county": "Fisher, TX",
            "value": 0
        },
        "Floyd, TX": {
            "county": "Floyd, TX",
            "value": 0
        },
        "Foard, TX": {
            "county": "Foard, TX",
            "value": 0
        },
        "Fort Bend, TX": {
            "county": "Fort Bend, TX",
            "value": 0
        },
        "Franklin, TX": {
            "county": "Franklin, TX",
            "value": 0
        },
        "Freestone, TX": {
            "county": "Freestone, TX",
            "value": 0
        },
        "Frio, TX": {
            "county": "Frio, TX",
            "value": 0
        },
        "Gaines, TX": {
            "county": "Gaines, TX",
            "value": 0
        },
        "Galveston, TX": {
            "county": "Galveston, TX",
            "value": 0
        },
        "Gillespie, TX": {
            "county": "Gillespie, TX",
            "value": 0
        },
        "Glasscock, TX": {
            "county": "Glasscock, TX",
            "value": 0
        },
        "Goliad, TX": {
            "county": "Goliad, TX",
            "value": 0
        },
        "Gray, TX": {
            "county": "Gray, TX",
            "value": 0
        },
        "Grayson, TX": {
            "county": "Grayson, TX",
            "value": 0
        },
        "Gregg, TX": {
            "county": "Gregg, TX",
            "value": 0
        },
        "Grimes, TX": {
            "county": "Grimes, TX",
            "value": 0
        },
        "Guadalupe, TX": {
            "county": "Guadalupe, TX",
            "value": 0
        },
        "Hale, TX": {
            "county": "Hale, TX",
            "value": 0
        },
        "Hall, TX": {
            "county": "Hall, TX",
            "value": 0
        },
        "Hamilton, TX": {
            "county": "Hamilton, TX",
            "value": 0
        },
        "Hansford, TX": {
            "county": "Hansford, TX",
            "value": 0
        },
        "Hardin, TX": {
            "county": "Hardin, TX",
            "value": 0
        },
        "Harris, TX": {
            "county": "Harris, TX",
            "value": 0
        },
        "Hays, TX": {
            "county": "Hays, TX",
            "value": 0
        },
        "Henderson, TX": {
            "county": "Henderson, TX",
            "value": 0
        },
        "Hill, TX": {
            "county": "Hill, TX",
            "value": 0
        },
        "Hockley, TX": {
            "county": "Hockley, TX",
            "value": 0
        },
        "Hood, TX": {
            "county": "Hood, TX",
            "value": 0
        },
        "Houston, TX": {
            "county": "Houston, TX",
            "value": 0
        },
        "Howard, TX": {
            "county": "Howard, TX",
            "value": 0
        },
        "Hudspeth, TX": {
            "county": "Hudspeth, TX",
            "value": 0
        },
        "Hunt, TX": {
            "county": "Hunt, TX",
            "value": 0
        },
        "Hutchinson, TX": {
            "county": "Hutchinson, TX",
            "value": 0
        },
        "Irion, TX": {
            "county": "Irion, TX",
            "value": 0
        },
        "Jack, TX": {
            "county": "Jack, TX",
            "value": 0
        },
        "Jackson, TX": {
            "county": "Jackson, TX",
            "value": 0
        },
        "Jefferson, TX": {
            "county": "Jefferson, TX",
            "value": 0
        },
        "Jim Hogg, TX": {
            "county": "Jim Hogg, TX",
            "value": 0
        },
        "Johnson, TX": {
            "county": "Johnson, TX",
            "value": 0
        },
        "Karnes, TX": {
            "county": "Karnes, TX",
            "value": 0
        },
        "Kaufman, TX": {
            "county": "Kaufman, TX",
            "value": 0
        },
        "Kenedy, TX": {
            "county": "Kenedy, TX",
            "value": 0
        },
        "Kent, TX": {
            "county": "Kent, TX",
            "value": 0
        },
        "Kimble, TX": {
            "county": "Kimble, TX",
            "value": 0
        },
        "King, TX": {
            "county": "King, TX",
            "value": 0
        },
        "Kinney, TX": {
            "county": "Kinney, TX",
            "value": 0
        },
        "Knox, TX": {
            "county": "Knox, TX",
            "value": 0
        },
        "Lampasas, TX": {
            "county": "Lampasas, TX",
            "value": 0
        },
        "La Salle, TX": {
            "county": "La Salle, TX",
            "value": 0
        },
        "Lavaca, TX": {
            "county": "Lavaca, TX",
            "value": 0
        },
        "Lee, TX": {
            "county": "Lee, TX",
            "value": 0
        },
        "Leon, TX": {
            "county": "Leon, TX",
            "value": 0
        },
        "Limestone, TX": {
            "county": "Limestone, TX",
            "value": 0
        },
        "Lipscomb, TX": {
            "county": "Lipscomb, TX",
            "value": 0
        },
        "Live Oak, TX": {
            "county": "Live Oak, TX",
            "value": 0
        },
        "Loving, TX": {
            "county": "Loving, TX",
            "value": 0
        },
        "Lubbock, TX": {
            "county": "Lubbock, TX",
            "value": 0
        },
        "Lynn, TX": {
            "county": "Lynn, TX",
            "value": 0
        },
        "McLennan, TX": {
            "county": "McLennan, TX",
            "value": 0
        },
        "Marion, TX": {
            "county": "Marion, TX",
            "value": 0
        },
        "Martin, TX": {
            "county": "Martin, TX",
            "value": 0
        },
        "Mason, TX": {
            "county": "Mason, TX",
            "value": 0
        },
        "Matagorda, TX": {
            "county": "Matagorda, TX",
            "value": 0
        },
        "Maverick, TX": {
            "county": "Maverick, TX",
            "value": 0
        },
        "Medina, TX": {
            "county": "Medina, TX",
            "value": 0
        },
        "Menard, TX": {
            "county": "Menard, TX",
            "value": 0
        },
        "Mitchell, TX": {
            "county": "Mitchell, TX",
            "value": 0
        },
        "Montague, TX": {
            "county": "Montague, TX",
            "value": 0
        },
        "Moore, TX": {
            "county": "Moore, TX",
            "value": 0
        },
        "Morris, TX": {
            "county": "Morris, TX",
            "value": 0
        },
        "Motley, TX": {
            "county": "Motley, TX",
            "value": 0
        },
        "Nacogdoches, TX": {
            "county": "Nacogdoches, TX",
            "value": 0
        },
        "Navarro, TX": {
            "county": "Navarro, TX",
            "value": 0
        },
        "Newton, TX": {
            "county": "Newton, TX",
            "value": 0
        },
        "Nolan, TX": {
            "county": "Nolan, TX",
            "value": 0
        },
        "Ochiltree, TX": {
            "county": "Ochiltree, TX",
            "value": 0
        },
        "Orange, TX": {
            "county": "Orange, TX",
            "value": 0
        },
        "Panola, TX": {
            "county": "Panola, TX",
            "value": 0
        },
        "Parker, TX": {
            "county": "Parker, TX",
            "value": 0
        },
        "Parmer, TX": {
            "county": "Parmer, TX",
            "value": 0
        },
        "Pecos, TX": {
            "county": "Pecos, TX",
            "value": 0
        },
        "Potter, TX": {
            "county": "Potter, TX",
            "value": 0
        },
        "Rains, TX": {
            "county": "Rains, TX",
            "value": 0
        },
        "Randall, TX": {
            "county": "Randall, TX",
            "value": 0
        },
        "Reagan, TX": {
            "county": "Reagan, TX",
            "value": 0
        },
        "Real, TX": {
            "county": "Real, TX",
            "value": 0
        },
        "Reeves, TX": {
            "county": "Reeves, TX",
            "value": 0
        },
        "Refugio, TX": {
            "county": "Refugio, TX",
            "value": 0
        },
        "Roberts, TX": {
            "county": "Roberts, TX",
            "value": 0
        },
        "Robertson, TX": {
            "county": "Robertson, TX",
            "value": 0
        },
        "Rockwall, TX": {
            "county": "Rockwall, TX",
            "value": 0
        },
        "Rusk, TX": {
            "county": "Rusk, TX",
            "value": 0
        },
        "Sabine, TX": {
            "county": "Sabine, TX",
            "value": 0
        },
        "San Augustine, TX": {
            "county": "San Augustine, TX",
            "value": 0
        },
        "San Jacinto, TX": {
            "county": "San Jacinto, TX",
            "value": 0
        },
        "San Patricio, TX": {
            "county": "San Patricio, TX",
            "value": 0
        },
        "Schleicher, TX": {
            "county": "Schleicher, TX",
            "value": 0
        },
        "Scurry, TX": {
            "county": "Scurry, TX",
            "value": 0
        },
        "Shelby, TX": {
            "county": "Shelby, TX",
            "value": 0
        },
        "Sherman, TX": {
            "county": "Sherman, TX",
            "value": 0
        },
        "Smith, TX": {
            "county": "Smith, TX",
            "value": 0
        },
        "Somervell, TX": {
            "county": "Somervell, TX",
            "value": 0
        },
        "Starr, TX": {
            "county": "Starr, TX",
            "value": 0
        },
        "Stephens, TX": {
            "county": "Stephens, TX",
            "value": 0
        },
        "Sterling, TX": {
            "county": "Sterling, TX",
            "value": 0
        },
        "Stonewall, TX": {
            "county": "Stonewall, TX",
            "value": 0
        },
        "Sutton, TX": {
            "county": "Sutton, TX",
            "value": 0
        },
        "Swisher, TX": {
            "county": "Swisher, TX",
            "value": 0
        },
        "Terrell, TX": {
            "county": "Terrell, TX",
            "value": 0
        },
        "Terry, TX": {
            "county": "Terry, TX",
            "value": 0
        },
        "Throckmorton, TX": {
            "county": "Throckmorton, TX",
            "value": 0
        },
        "Titus, TX": {
            "county": "Titus, TX",
            "value": 0
        },
        "Tom Green, TX": {
            "county": "Tom Green, TX",
            "value": 0
        },
        "Trinity, TX": {
            "county": "Trinity, TX",
            "value": 0
        },
        "Tyler, TX": {
            "county": "Tyler, TX",
            "value": 0
        },
        "Upshur, TX": {
            "county": "Upshur, TX",
            "value": 0
        },
        "Upton, TX": {
            "county": "Upton, TX",
            "value": 0
        },
        "Uvalde, TX": {
            "county": "Uvalde, TX",
            "value": 0
        },
        "Van Zandt, TX": {
            "county": "Van Zandt, TX",
            "value": 0
        },
        "Walker, TX": {
            "county": "Walker, TX",
            "value": 0
        },
        "Waller, TX": {
            "county": "Waller, TX",
            "value": 0
        },
        "Ward, TX": {
            "county": "Ward, TX",
            "value": 0
        },
        "Washington, TX": {
            "county": "Washington, TX",
            "value": 0
        },
        "Wharton, TX": {
            "county": "Wharton, TX",
            "value": 0
        },
        "Wichita, TX": {
            "county": "Wichita, TX",
            "value": 0
        },
        "Wilbarger, TX": {
            "county": "Wilbarger, TX",
            "value": 0
        },
        "Williamson, TX": {
            "county": "Williamson, TX",
            "value": 0
        },
        "Wilson, TX": {
            "county": "Wilson, TX",
            "value": 0
        },
        "Winkler, TX": {
            "county": "Winkler, TX",
            "value": 0
        },
        "Wise, TX": {
            "county": "Wise, TX",
            "value": 0
        },
        "Wood, TX": {
            "county": "Wood, TX",
            "value": 0
        },
        "Young, TX": {
            "county": "Young, TX",
            "value": 0
        },
        "Zapata, TX": {
            "county": "Zapata, TX",
            "value": 0
        },
        "Zavala, TX": {
            "county": "Zavala, TX",
            "value": 0
        },
        "Cache, UT": {
            "county": "Cache, UT",
            "value": 0
        },
        "Daggett, UT": {
            "county": "Daggett, UT",
            "value": 0
        },
        "Emery, UT": {
            "county": "Emery, UT",
            "value": 0
        },
        "San Juan, UT": {
            "county": "San Juan, UT",
            "value": 0
        },
        "Bennington, VT": {
            "county": "Bennington, VT",
            "value": 0
        },
        "Franklin, VT": {
            "county": "Franklin, VT",
            "value": 0
        },
        "Grand Isle, VT": {
            "county": "Grand Isle, VT",
            "value": 0
        },
        "Lamoille, VT": {
            "county": "Lamoille, VT",
            "value": 0
        },
        "Orange, VT": {
            "county": "Orange, VT",
            "value": 0
        },
        "Orleans, VT": {
            "county": "Orleans, VT",
            "value": 0
        },
        "Rutland, VT": {
            "county": "Rutland, VT",
            "value": 0
        },
        "Washington, VT": {
            "county": "Washington, VT",
            "value": 0
        },
        "Windham, VT": {
            "county": "Windham, VT",
            "value": 0
        },
        "Accomack, VA": {
            "county": "Accomack, VA",
            "value": 0
        },
        "Alleghany, VA": {
            "county": "Alleghany, VA",
            "value": 0
        },
        "Amelia, VA": {
            "county": "Amelia, VA",
            "value": 0
        },
        "Amherst, VA": {
            "county": "Amherst, VA",
            "value": 0
        },
        "Appomattox, VA": {
            "county": "Appomattox, VA",
            "value": 0
        },
        "Arlington, VA": {
            "county": "Arlington, VA",
            "value": 0
        },
        "Augusta, VA": {
            "county": "Augusta, VA",
            "value": 0
        },
        "Bath, VA": {
            "county": "Bath, VA",
            "value": 0
        },
        "Bedford County, VA": {
            "county": "Bedford County, VA",
            "value": 0
        },
        "Bland, VA": {
            "county": "Bland, VA",
            "value": 0
        },
        "Botetourt, VA": {
            "county": "Botetourt, VA",
            "value": 0
        },
        "Brunswick, VA": {
            "county": "Brunswick, VA",
            "value": 0
        },
        "Buchanan, VA": {
            "county": "Buchanan, VA",
            "value": 0
        },
        "Buckingham, VA": {
            "county": "Buckingham, VA",
            "value": 0
        },
        "Campbell, VA": {
            "county": "Campbell, VA",
            "value": 0
        },
        "Caroline, VA": {
            "county": "Caroline, VA",
            "value": 0
        },
        "Carroll, VA": {
            "county": "Carroll, VA",
            "value": 0
        },
        "Charles City, VA": {
            "county": "Charles City, VA",
            "value": 0
        },
        "Charlotte, VA": {
            "county": "Charlotte, VA",
            "value": 0
        },
        "Chesterfield, VA": {
            "county": "Chesterfield, VA",
            "value": 0
        },
        "Clarke, VA": {
            "county": "Clarke, VA",
            "value": 0
        },
        "Craig, VA": {
            "county": "Craig, VA",
            "value": 0
        },
        "Culpeper, VA": {
            "county": "Culpeper, VA",
            "value": 0
        },
        "Cumberland, VA": {
            "county": "Cumberland, VA",
            "value": 0
        },
        "Dickenson, VA": {
            "county": "Dickenson, VA",
            "value": 0
        },
        "Dinwiddie, VA": {
            "county": "Dinwiddie, VA",
            "value": 0
        },
        "Essex, VA": {
            "county": "Essex, VA",
            "value": 0
        },
        "Fairfax County, VA": {
            "county": "Fairfax County, VA",
            "value": 0
        },
        "Fauquier, VA": {
            "county": "Fauquier, VA",
            "value": 0
        },
        "Floyd, VA": {
            "county": "Floyd, VA",
            "value": 0
        },
        "Fluvanna, VA": {
            "county": "Fluvanna, VA",
            "value": 0
        },
        "Franklin County, VA": {
            "county": "Franklin County, VA",
            "value": 0
        },
        "Frederick, VA": {
            "county": "Frederick, VA",
            "value": 0
        },
        "Giles, VA": {
            "county": "Giles, VA",
            "value": 0
        },
        "Gloucester, VA": {
            "county": "Gloucester, VA",
            "value": 0
        },
        "Goochland, VA": {
            "county": "Goochland, VA",
            "value": 0
        },
        "Grayson, VA": {
            "county": "Grayson, VA",
            "value": 0
        },
        "Greene, VA": {
            "county": "Greene, VA",
            "value": 0
        },
        "Greensville, VA": {
            "county": "Greensville, VA",
            "value": 0
        },
        "Halifax, VA": {
            "county": "Halifax, VA",
            "value": 0
        },
        "Hanover, VA": {
            "county": "Hanover, VA",
            "value": 0
        },
        "Henrico, VA": {
            "county": "Henrico, VA",
            "value": 0
        },
        "Henry, VA": {
            "county": "Henry, VA",
            "value": 0
        },
        "Highland, VA": {
            "county": "Highland, VA",
            "value": 0
        },
        "Isle of Wight, VA": {
            "county": "Isle of Wight, VA",
            "value": 0
        },
        "James City, VA": {
            "county": "James City, VA",
            "value": 0
        },
        "King and Queen, VA": {
            "county": "King and Queen, VA",
            "value": 0
        },
        "King George, VA": {
            "county": "King George, VA",
            "value": 0
        },
        "King William, VA": {
            "county": "King William, VA",
            "value": 0
        },
        "Lancaster, VA": {
            "county": "Lancaster, VA",
            "value": 0
        },
        "Lee, VA": {
            "county": "Lee, VA",
            "value": 0
        },
        "Loudoun, VA": {
            "county": "Loudoun, VA",
            "value": 0
        },
        "Louisa, VA": {
            "county": "Louisa, VA",
            "value": 0
        },
        "Lunenburg, VA": {
            "county": "Lunenburg, VA",
            "value": 0
        },
        "Madison, VA": {
            "county": "Madison, VA",
            "value": 0
        },
        "Mathews, VA": {
            "county": "Mathews, VA",
            "value": 0
        },
        "Mecklenburg, VA": {
            "county": "Mecklenburg, VA",
            "value": 0
        },
        "Middlesex, VA": {
            "county": "Middlesex, VA",
            "value": 0
        },
        "Montgomery, VA": {
            "county": "Montgomery, VA",
            "value": 0
        },
        "Nelson, VA": {
            "county": "Nelson, VA",
            "value": 0
        },
        "New Kent, VA": {
            "county": "New Kent, VA",
            "value": 0
        },
        "Northampton, VA": {
            "county": "Northampton, VA",
            "value": 0
        },
        "Northumberland, VA": {
            "county": "Northumberland, VA",
            "value": 0
        },
        "Nottoway, VA": {
            "county": "Nottoway, VA",
            "value": 0
        },
        "Patrick, VA": {
            "county": "Patrick, VA",
            "value": 0
        },
        "Pittsylvania, VA": {
            "county": "Pittsylvania, VA",
            "value": 0
        },
        "Powhatan, VA": {
            "county": "Powhatan, VA",
            "value": 0
        },
        "Prince Edward, VA": {
            "county": "Prince Edward, VA",
            "value": 0
        },
        "Prince George, VA": {
            "county": "Prince George, VA",
            "value": 0
        },
        "Prince William, VA": {
            "county": "Prince William, VA",
            "value": 0
        },
        "Rappahannock, VA": {
            "county": "Rappahannock, VA",
            "value": 0
        },
        "Richmond County, VA": {
            "county": "Richmond County, VA",
            "value": 0
        },
        "Roanoke County, VA": {
            "county": "Roanoke County, VA",
            "value": 0
        },
        "Rockbridge, VA": {
            "county": "Rockbridge, VA",
            "value": 0
        },
        "Rockingham, VA": {
            "county": "Rockingham, VA",
            "value": 0
        },
        "Scott, VA": {
            "county": "Scott, VA",
            "value": 0
        },
        "Shenandoah, VA": {
            "county": "Shenandoah, VA",
            "value": 0
        },
        "Smyth, VA": {
            "county": "Smyth, VA",
            "value": 0
        },
        "Southampton, VA": {
            "county": "Southampton, VA",
            "value": 0
        },
        "Spotsylvania, VA": {
            "county": "Spotsylvania, VA",
            "value": 0
        },
        "Stafford, VA": {
            "county": "Stafford, VA",
            "value": 0
        },
        "Surry, VA": {
            "county": "Surry, VA",
            "value": 0
        },
        "Tazewell, VA": {
            "county": "Tazewell, VA",
            "value": 0
        },
        "Warren, VA": {
            "county": "Warren, VA",
            "value": 0
        },
        "Washington, VA": {
            "county": "Washington, VA",
            "value": 0
        },
        "Westmoreland, VA": {
            "county": "Westmoreland, VA",
            "value": 0
        },
        "Wythe, VA": {
            "county": "Wythe, VA",
            "value": 0
        },
        "York, VA": {
            "county": "York, VA",
            "value": 0
        },
        "Alexandria, VA": {
            "county": "Alexandria, VA",
            "value": 0
        },
        "Bedford, VA": {
            "county": "Bedford, VA",
            "value": 0
        },
        "Bristol, VA": {
            "county": "Bristol, VA",
            "value": 0
        },
        "Buena Vista, VA": {
            "county": "Buena Vista, VA",
            "value": 0
        },
        "Charlottesville, VA": {
            "county": "Charlottesville, VA",
            "value": 0
        },
        "Chesapeake, VA": {
            "county": "Chesapeake, VA",
            "value": 0
        },
        "Colonial Heights, VA": {
            "county": "Colonial Heights, VA",
            "value": 0
        },
        "Covington, VA": {
            "county": "Covington, VA",
            "value": 0
        },
        "Danville, VA": {
            "county": "Danville, VA",
            "value": 0
        },
        "Emporia, VA": {
            "county": "Emporia, VA",
            "value": 0
        },
        "Fairfax, VA": {
            "county": "Fairfax, VA",
            "value": 0
        },
        "Falls Church, VA": {
            "county": "Falls Church, VA",
            "value": 0
        },
        "Franklin, VA": {
            "county": "Franklin, VA",
            "value": 0
        },
        "Fredericksburg, VA": {
            "county": "Fredericksburg, VA",
            "value": 0
        },
        "Galax, VA": {
            "county": "Galax, VA",
            "value": 0
        },
        "Hampton, VA": {
            "county": "Hampton, VA",
            "value": 0
        },
        "Harrisonburg, VA": {
            "county": "Harrisonburg, VA",
            "value": 0
        },
        "Hopewell, VA": {
            "county": "Hopewell, VA",
            "value": 0
        },
        "Lexington, VA": {
            "county": "Lexington, VA",
            "value": 0
        },
        "Lynchburg, VA": {
            "county": "Lynchburg, VA",
            "value": 0
        },
        "Manassas, VA": {
            "county": "Manassas, VA",
            "value": 0
        },
        "Manassas Park, VA": {
            "county": "Manassas Park, VA",
            "value": 0
        },
        "Martinsville, VA": {
            "county": "Martinsville, VA",
            "value": 0
        },
        "Newport News, VA": {
            "county": "Newport News, VA",
            "value": 0
        },
        "Norfolk, VA": {
            "county": "Norfolk, VA",
            "value": 0
        },
        "Norton, VA": {
            "county": "Norton, VA",
            "value": 0
        },
        "Petersburg, VA": {
            "county": "Petersburg, VA",
            "value": 0
        },
        "Poquoson, VA": {
            "county": "Poquoson, VA",
            "value": 0
        },
        "Portsmouth, VA": {
            "county": "Portsmouth, VA",
            "value": 0
        },
        "Radford, VA": {
            "county": "Radford, VA",
            "value": 0
        },
        "Richmond, VA": {
            "county": "Richmond, VA",
            "value": 0
        },
        "Salem, VA": {
            "county": "Salem, VA",
            "value": 0
        },
        "Staunton, VA": {
            "county": "Staunton, VA",
            "value": 0
        },
        "Suffolk, VA": {
            "county": "Suffolk, VA",
            "value": 0
        },
        "Virginia Beach, VA": {
            "county": "Virginia Beach, VA",
            "value": 0
        },
        "Waynesboro, VA": {
            "county": "Waynesboro, VA",
            "value": 0
        },
        "Williamsburg, VA": {
            "county": "Williamsburg, VA",
            "value": 0
        },
        "Winchester, VA": {
            "county": "Winchester, VA",
            "value": 0
        },
        "Adams, WA": {
            "county": "Adams, WA",
            "value": 0
        },
        "Asotin, WA": {
            "county": "Asotin, WA",
            "value": 0
        },
        "Benton, WA": {
            "county": "Benton, WA",
            "value": 0
        },
        "Clallam, WA": {
            "county": "Clallam, WA",
            "value": 0
        },
        "Clark, WA": {
            "county": "Clark, WA",
            "value": 0
        },
        "Columbia, WA": {
            "county": "Columbia, WA",
            "value": 0
        },
        "Cowlitz, WA": {
            "county": "Cowlitz, WA",
            "value": 0
        },
        "Douglas, WA": {
            "county": "Douglas, WA",
            "value": 0
        },
        "Ferry, WA": {
            "county": "Ferry, WA",
            "value": 0
        },
        "Garfield, WA": {
            "county": "Garfield, WA",
            "value": 0
        },
        "Island, WA": {
            "county": "Island, WA",
            "value": 0
        },
        "King, WA": {
            "county": "King, WA",
            "value": 0
        },
        "Kitsap, WA": {
            "county": "Kitsap, WA",
            "value": 0
        },
        "Kittitas, WA": {
            "county": "Kittitas, WA",
            "value": 0
        },
        "Lewis, WA": {
            "county": "Lewis, WA",
            "value": 0
        },
        "Lincoln, WA": {
            "county": "Lincoln, WA",
            "value": 0
        },
        "Mason, WA": {
            "county": "Mason, WA",
            "value": 0
        },
        "Pend Oreille, WA": {
            "county": "Pend Oreille, WA",
            "value": 0
        },
        "San Juan, WA": {
            "county": "San Juan, WA",
            "value": 0
        },
        "Skamania, WA": {
            "county": "Skamania, WA",
            "value": 0
        },
        "Spokane, WA": {
            "county": "Spokane, WA",
            "value": 0
        },
        "Thurston, WA": {
            "county": "Thurston, WA",
            "value": 0
        },
        "Wahkiakum, WA": {
            "county": "Wahkiakum, WA",
            "value": 0
        },
        "Walla Walla, WA": {
            "county": "Walla Walla, WA",
            "value": 0
        },
        "Whitman, WA": {
            "county": "Whitman, WA",
            "value": 0
        },
        "Barbour, WV": {
            "county": "Barbour, WV",
            "value": 0
        },
        "Berkeley, WV": {
            "county": "Berkeley, WV",
            "value": 0
        },
        "Boone, WV": {
            "county": "Boone, WV",
            "value": 0
        },
        "Brooke, WV": {
            "county": "Brooke, WV",
            "value": 0
        },
        "Cabell, WV": {
            "county": "Cabell, WV",
            "value": 0
        },
        "Calhoun, WV": {
            "county": "Calhoun, WV",
            "value": 0
        },
        "Clay, WV": {
            "county": "Clay, WV",
            "value": 0
        },
        "Doddridge, WV": {
            "county": "Doddridge, WV",
            "value": 0
        },
        "Gilmer, WV": {
            "county": "Gilmer, WV",
            "value": 0
        },
        "Grant, WV": {
            "county": "Grant, WV",
            "value": 0
        },
        "Hampshire, WV": {
            "county": "Hampshire, WV",
            "value": 0
        },
        "Hancock, WV": {
            "county": "Hancock, WV",
            "value": 0
        },
        "Hardy, WV": {
            "county": "Hardy, WV",
            "value": 0
        },
        "Harrison, WV": {
            "county": "Harrison, WV",
            "value": 0
        },
        "Kanawha, WV": {
            "county": "Kanawha, WV",
            "value": 0
        },
        "Lincoln, WV": {
            "county": "Lincoln, WV",
            "value": 0
        },
        "McDowell, WV": {
            "county": "McDowell, WV",
            "value": 0
        },
        "Mason, WV": {
            "county": "Mason, WV",
            "value": 0
        },
        "Mercer, WV": {
            "county": "Mercer, WV",
            "value": 0
        },
        "Mineral, WV": {
            "county": "Mineral, WV",
            "value": 0
        },
        "Mingo, WV": {
            "county": "Mingo, WV",
            "value": 0
        },
        "Monongalia, WV": {
            "county": "Monongalia, WV",
            "value": 0
        },
        "Monroe, WV": {
            "county": "Monroe, WV",
            "value": 0
        },
        "Morgan, WV": {
            "county": "Morgan, WV",
            "value": 0
        },
        "Nicholas, WV": {
            "county": "Nicholas, WV",
            "value": 0
        },
        "Ohio, WV": {
            "county": "Ohio, WV",
            "value": 0
        },
        "Pendleton, WV": {
            "county": "Pendleton, WV",
            "value": 0
        },
        "Pleasants, WV": {
            "county": "Pleasants, WV",
            "value": 0
        },
        "Putnam, WV": {
            "county": "Putnam, WV",
            "value": 0
        },
        "Raleigh, WV": {
            "county": "Raleigh, WV",
            "value": 0
        },
        "Randolph, WV": {
            "county": "Randolph, WV",
            "value": 0
        },
        "Ritchie, WV": {
            "county": "Ritchie, WV",
            "value": 0
        },
        "Roane, WV": {
            "county": "Roane, WV",
            "value": 0
        },
        "Summers, WV": {
            "county": "Summers, WV",
            "value": 0
        },
        "Taylor, WV": {
            "county": "Taylor, WV",
            "value": 0
        },
        "Tyler, WV": {
            "county": "Tyler, WV",
            "value": 0
        },
        "Upshur, WV": {
            "county": "Upshur, WV",
            "value": 0
        },
        "Wayne, WV": {
            "county": "Wayne, WV",
            "value": 0
        },
        "Webster, WV": {
            "county": "Webster, WV",
            "value": 0
        },
        "Wetzel, WV": {
            "county": "Wetzel, WV",
            "value": 0
        },
        "Wirt, WV": {
            "county": "Wirt, WV",
            "value": 0
        },
        "Wood, WV": {
            "county": "Wood, WV",
            "value": 0
        },
        "Wyoming, WV": {
            "county": "Wyoming, WV",
            "value": 0
        },
        "Adams, WI": {
            "county": "Adams, WI",
            "value": 0
        },
        "Ashland, WI": {
            "county": "Ashland, WI",
            "value": 0
        },
        "Barron, WI": {
            "county": "Barron, WI",
            "value": 0
        },
        "Buffalo, WI": {
            "county": "Buffalo, WI",
            "value": 0
        },
        "Burnett, WI": {
            "county": "Burnett, WI",
            "value": 0
        },
        "Calumet, WI": {
            "county": "Calumet, WI",
            "value": 0
        },
        "Chippewa, WI": {
            "county": "Chippewa, WI",
            "value": 0
        },
        "Clark, WI": {
            "county": "Clark, WI",
            "value": 0
        },
        "Columbia, WI": {
            "county": "Columbia, WI",
            "value": 0
        },
        "Dane, WI": {
            "county": "Dane, WI",
            "value": 0
        },
        "Dodge, WI": {
            "county": "Dodge, WI",
            "value": 0
        },
        "Dunn, WI": {
            "county": "Dunn, WI",
            "value": 0
        },
        "Eau Claire, WI": {
            "county": "Eau Claire, WI",
            "value": 0
        },
        "Florence, WI": {
            "county": "Florence, WI",
            "value": 0
        },
        "Fond du Lac, WI": {
            "county": "Fond du Lac, WI",
            "value": 0
        },
        "Forest, WI": {
            "county": "Forest, WI",
            "value": 0
        },
        "Grant, WI": {
            "county": "Grant, WI",
            "value": 0
        },
        "Green Lake, WI": {
            "county": "Green Lake, WI",
            "value": 0
        },
        "Iowa, WI": {
            "county": "Iowa, WI",
            "value": 0
        },
        "Jackson, WI": {
            "county": "Jackson, WI",
            "value": 0
        },
        "Kenosha, WI": {
            "county": "Kenosha, WI",
            "value": 0
        },
        "Kewaunee, WI": {
            "county": "Kewaunee, WI",
            "value": 0
        },
        "La Crosse, WI": {
            "county": "La Crosse, WI",
            "value": 0
        },
        "Lincoln, WI": {
            "county": "Lincoln, WI",
            "value": 0
        },
        "Marathon, WI": {
            "county": "Marathon, WI",
            "value": 0
        },
        "Marquette, WI": {
            "county": "Marquette, WI",
            "value": 0
        },
        "Menominee, WI": {
            "county": "Menominee, WI",
            "value": 0
        },
        "Outagamie, WI": {
            "county": "Outagamie, WI",
            "value": 0
        },
        "Pepin, WI": {
            "county": "Pepin, WI",
            "value": 0
        },
        "Portage, WI": {
            "county": "Portage, WI",
            "value": 0
        },
        "Price, WI": {
            "county": "Price, WI",
            "value": 0
        },
        "Racine, WI": {
            "county": "Racine, WI",
            "value": 0
        },
        "Richland, WI": {
            "county": "Richland, WI",
            "value": 0
        },
        "Rock, WI": {
            "county": "Rock, WI",
            "value": 0
        },
        "Rusk, WI": {
            "county": "Rusk, WI",
            "value": 0
        },
        "St. Croix, WI": {
            "county": "St. Croix, WI",
            "value": 0
        },
        "Sauk, WI": {
            "county": "Sauk, WI",
            "value": 0
        },
        "Sawyer, WI": {
            "county": "Sawyer, WI",
            "value": 0
        },
        "Shawano, WI": {
            "county": "Shawano, WI",
            "value": 0
        },
        "Sheboygan, WI": {
            "county": "Sheboygan, WI",
            "value": 0
        },
        "Taylor, WI": {
            "county": "Taylor, WI",
            "value": 0
        },
        "Trempealeau, WI": {
            "county": "Trempealeau, WI",
            "value": 0
        },
        "Vernon, WI": {
            "county": "Vernon, WI",
            "value": 0
        },
        "Vilas, WI": {
            "county": "Vilas, WI",
            "value": 0
        },
        "Washburn, WI": {
            "county": "Washburn, WI",
            "value": 0
        },
        "Waupaca, WI": {
            "county": "Waupaca, WI",
            "value": 0
        },
        "Waushara, WI": {
            "county": "Waushara, WI",
            "value": 0
        },
        "Winnebago, WI": {
            "county": "Winnebago, WI",
            "value": 0
        },
        "Wood, WI": {
            "county": "Wood, WI",
            "value": 0
        },
        "Big Horn, WY": {
            "county": "Big Horn, WY",
            "value": 0
        },
        "Crook, WY": {
            "county": "Crook, WY",
            "value": 0
        },
        "Hot Springs, WY": {
            "county": "Hot Springs, WY",
            "value": 0
        },
        "Johnson, WY": {
            "county": "Johnson, WY",
            "value": 0
        },
        "Niobrara, WY": {
            "county": "Niobrara, WY",
            "value": 0
        },
        "Platte, WY": {
            "county": "Platte, WY",
            "value": 0
        },
        "Sheridan, WY": {
            "county": "Sheridan, WY",
            "value": 0
        },
        "Uinta, WY": {
            "county": "Uinta, WY",
            "value": 0
        },
        "Weston, WY": {
            "county": "Weston, WY",
            "value": 0
        }
    }
}